require 'builder'
# Use kramdown for md rendering
set :markdown_engine, :kramdown

# Set time zone
set :time_zone, 'Europe/Berlin'

# Per-page layout changes
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

###
## Asset settings
###
#redirect "https://technik-und-welt.de/", to: "https://www.technik-podcast.de/"
#redirect "https://www.technik-und-welt.de/", to: "https://www.technik-podcast.de/"

# Sprockets asset compilation
activate :sprockets
set :fonts_dir, 'fonts'
set :css_dir, 'stylesheets'
set :js_dir, 'javascripts'
set :images_dir, 'images'

# Autoprefixer extension
activate :autoprefixer do |prefix|
  prefix.browsers = 'last 2 versions'
end

###
## Blog settings
###

activate :blog do |blog|
  blog.name = "podcast"
  blog.prefix = 'podcast'
  blog.sources = '{year}-{month}-{day}-{title}.html'
  blog.permalink = '{year}-{month}-{day}-{title}'
  blog.taglink = 'podcast/{tag}.html'
  blog.layout = 'article'
  blog.summary_separator = /(READMORE)/
  blog.summary_length = 600
  blog.year_link = '{year}.html'
  blog.month_link = '{year}/{month}.html'
  blog.day_link = '{year}/{month}/{day}.html'
  blog.default_extension = '.md'

  blog.tag_template = '/blog_common/tag.html'
  blog.calendar_template = '/blog_common/calendar.html'
  blog.new_article_template = File.expand_path('source/blog_common/podcast_template.erb', __dir__)

  blog.paginate = true
  blog.per_page = 10
  blog.page_link = 'page/{num}'
  
end

activate :livereload
activate :dragonfly_thumbnailer

activate :directory_indexes

# Setup blog feed
page "/podcast/atom.xml", layout: false
page "/feed.xml", layout: false

activate :syntax

# Build-specific configuration
# https://middlemanapp.com/advanced/configuration/#environment-specific-settings
configure :build do
  activate :minify_css
  activate :minify_javascript
  activate :favicon_maker, :icons => {
    "_favicon_template.png" =>  [
    { icon: "apple-touch-icon-180x180-precomposed.png" },             # Same as apple-touch-icon-57x57.png, for iPhone 6 Plus with @3× display
    { icon: "apple-touch-icon-152x152-precomposed.png" },             # Same as apple-touch-icon-57x57.png, for retina iPad with iOS7.
    { icon: "apple-touch-icon-144x144-precomposed.png" },             # Same as apple-touch-icon-57x57.png, for retina iPad with iOS6 or prior.
    { icon: "apple-touch-icon-120x120-precomposed.png" },             # Same as apple-touch-icon-57x57.png, for retina iPhone with iOS7.
    { icon: "apple-touch-icon-114x114-precomposed.png" },             # Same as apple-touch-icon-57x57.png, for retina iPhone with iOS6 or prior.
    { icon: "apple-touch-icon-76x76-precomposed.png" },               # Same as apple-touch-icon-57x57.png, for non-retina iPad with iOS7.
    { icon: "apple-touch-icon-72x72-precomposed.png" },               # Same as apple-touch-icon-57x57.png, for non-retina iPad with iOS6 or prior.
    { icon: "apple-touch-icon-60x60-precomposed.png" },               # Same as apple-touch-icon-57x57.png, for non-retina iPhone with iOS7.
    { icon: "apple-touch-icon-57x57-precomposed.png" },               # iPhone and iPad users can turn web pages into icons on their home screen. Such link appears as a regular iOS native application. When this happens, the device looks for a specific picture. The 57x57 resolution is convenient for non-retina iPhone with iOS6 or prior. Learn more in Apple docs.
    { icon: "apple-touch-icon-precomposed.png", size: "57x57" },      # Same as apple-touch-icon.png, expect that is already have rounded corners (but neither drop shadow nor gloss effect).
    { icon: "apple-touch-icon.png", size: "57x57" },                  # Same as apple-touch-icon-57x57.png, for "default" requests, as some devices may look for this specific file. This picture may save some 404 errors in your HTTP logs. See Apple docs
    { icon: "favicon-196x196.png" },                                  # For Android Chrome M31+.
    { icon: "favicon-160x160.png" },                                  # For Opera Speed Dial (up to Opera 12; this icon is deprecated starting from Opera 15), although the optimal icon is not square but rather 256x160. If Opera is a major platform for you, you should create this icon yourself.
    { icon: "favicon-96x96.png" },                                    # For Google TV.
    { icon: "favicon-32x32.png" },                                    # For Safari on Mac OS.
    { icon: "favicon-16x16.png" },                                    # The classic favicon, displayed in the tabs.
    { icon: "favicon.png", size: "16x16" },                           # The classic favicon, displayed in the tabs.
    { icon: "favicon.ico", size: "64x64,32x32,24x24,16x16" },         # Used by IE, and also by some other browsers if we are not careful.
    { icon: "mstile-70x70.png", size: "70x70" },                      # For Windows 8 / IE11.
    { icon: "mstile-144x144.png", size: "144x144" },
    { icon: "mstile-150x150.png", size: "150x150" },
    { icon: "mstile-310x310.png", size: "310x310" },
    { icon: "mstile-310x150.png", size: "310x150" }
   ]
  }
end
