FROM ruby

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        nodejs

WORKDIR /app

RUN gem install middleman
RUN gem install bundler:1.17.2

COPY . /app/.

RUN bundle install

EXPOSE 4567

CMD ["middleman", "server"]
