xml.instruct!
xml.feed "xmlns" => "http://www.w3.org/2005/Atom" do
  site_url = 'https://technik-podcast.de/' # TODO: Replace with absolute URL of site
  xml.title "Technik Podcast auf deutsch"
  xml.subtitle "Innovationen und neue Technologien"
  xml.id URI.join(site_url, blog('podcast').options.prefix.to_s)
  xml.link "href" => URI.join(site_url, blog('podcast').options.prefix.to_s)
  xml.link "href" => URI.join(site_url, current_page.path), "rel" => "self"
  xml.updated(blog('podcast').articles.first.date.to_time.iso8601) unless blog('podcast').articles.empty?


  blog('podcast').articles.each do |article|
    xml.entry do
      xml.title article.title
      xml.link "rel" => "alternate", "href" => URI.join(site_url, article.url)
      xml.id URI.join(site_url, article.url)
      xml.published article.date.to_time.iso8601
      xml.updated File.mtime(article.source_file).iso8601
      xml.author { xml.name article.data[:author], "type" => "html" }
      xml.summary article.data[:introartikel], "type" => "html"
      # xml.content article.body, "type" => "html"
    end
  end
end