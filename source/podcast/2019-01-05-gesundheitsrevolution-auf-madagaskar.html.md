---
date: 2019-01-05 04:08 PST
permalink: gesundheitsrevolution-auf-madagaskar
blog: podcast
title: Gesundheitsrevolution auf Madagaskar
short_title: Handy-Sparbuch statt Kranken-Versicherung
tags: 
  - E-Health
  - Gesundheit
  - Madagaskar
  - Informationstechnologie
  - Entwicklungshilfe
image: Ejeda_11-2018.JPG
image_alt_title: Handysparbuch ermöglicht Geburten in einer Klinik in Ejeda, Madagaskar
soundcloud_link: 554057832
published: true
author: Jutta Schwengsbier
introartikel: Medizin Innovation auf Madagaskar - Ein Handy-Mikrosparbuch ersetzt die fehlende Krankenversicherung. 
---

"Was sind die Schwierigkeiten auf die man da stößt? Dass Krankenhäuser sehr schwer zu erreichen sind. Die sind weit weg zum Teil. Müssen Patienten über Stunden oder Tage dort anreisen. Werden zum Teil von den Angehörigen getragen. Oder wenn sie etwas mehr Geld haben, können sie sich einen Ochsenwagen leisten um dann in die Klinik zu kommen. Es gibt Schwierigkeiten, dass Krankenhäuser keine Medikamente haben oder dass die Logistik Kette hakt und dass man keine Verbrauchsmaterialien zur Verfügung stehen. Viele von den Krankenhäusern sind auch ganz verfallen. Die Infrastruktur ist schlecht. Was wir immer mehr gemerkt haben, das ist eine der Schwierigkeiten, das nicht bezahlen können von jeder Art von Behandlung." Julius Emmrich

"Wir wollen mit unserer Lösung das umgehen, indem wir diese drei Hauptplayer Ärzte, Patienten und letztendlich die, die Behandlung bezahlen, besser verbinden. Das machen wir darüber, dass wir jetzt ein Zugang schaffen direkt zu einem mobilen Gesundheitssparbuch für Patienten wo das Geld, das auf dem Sparbuch liegt, eben nur noch für Gesundheit ausgegeben werden kann." Samuel Knauss

## mTOMADY - Ein Handy Sparbuch zur Gesundheitsvorsorge

Entwickelt als Innovationsprojekt von zwei Ärzten des Berliner Charité Krankenhauses und des Berliner Instituts für Gesundheitsforschung könnte die Technologielösung von mTOMADY auch für andere Länder Afrikas interessant sein. Nach dem Start im Oktober haben auf Madagaskar bis Ende Dezember 2018 mehr als  500 Patientinnen angefangen per Handygeldbörse für Ihre Gesundheit zu sparen.  So konnte über das neue mobile Gesundheitskonto in wen   igen  Wochen die Geburt von 20 Babies finanziert werden. Tendenz stark steigend. Julius Emmrich ist mehrmals pro Jahr zu medizinischen Hilfseinsätzen auf Madagaskar unterwegs. Der Berliner Neurologe und Vorsitzende des Vereins Ärzte für Madagaskar kennt die Insel und ihre medizinische Versorgung gut. 

"Wenn Patienten oder die Familie nicht in der Lage sind zu bezahlen, wird in vielen Fällen auch nicht behandelt. Da gibt es auch ganz absurde Situationen. Beispielsweise gibt es Räume oder Gebäude, die auf solch einem Krankenhausgelände dann gebaut werden, wo Patienten wie Geiseln festgehalten werden. Zum Teil über mehrere Wochen. Bis dann die Familie die Behandlungskosten begleichen kann. Dann kommen zum Teil Angehörige von vielen hundert Kilometern Entfernung angereist mit einem Beutel in dem sich das Bargeld befindet. Und dann kann der Angehörige ausgelöst werden. Das hat uns motiviert in der Richtung weiter zu denken." Julius Emmrich 

Samuel Knauss ist ebenfalls im Hauptberuf Neurologe am Berliner Charité Krankenhaus. Beide Ärzte fragten sich, warum so wenig Geld für Gesundheitsversorgung in Madagaskar verfügbar ist, obwohl supranationale Organisationen jedes Jahr hohe Millionenbeträge dafür bereitstellen. 

"80 Prozent der Gesundheitsdienstleistungen in Madagaskar sind extern finanziert. Das ist aber so, dass es Studien dazu gibt, dass auf dem Weg, bis es tatsächlich dieses Geld in Form von Behandlung beim Patienten ankommt, gehen ungefähr 40 bis 60 Prozent verloren." Samuel Knauss

## Abrechnung per Gesundheitscloud

Korruption. Mißwirtschaft. Fehlende Infrastruktur. Gründe für die schlechte Gesundheitsversorgung auf Madagaskar gibt es genug. 

"Die Patienten haben nur das Bargeld, um für ihre medizinischen Leistungen zu bezahlen und die Ärzte wissen aber nicht am Anfang, wenn sie nicht den gesamten Betrag vorher einziehen, ob sie die Bezahlung für die Behandlung bekommen." Samuel Knauss

Eine Krankenversicherung gibt es auf Madagaskar nicht. Bankkonten haben die wenigsten. Und dadurch auch keine Möglichkeit, für ihre Gesundeheitsvorsorge Geld zurück zu legen. Im Rahmen eines von der Charité und dem Berliner Institut für Gesundheitsforschung geförderten Innovationsprogramms begannen die beiden Mediziner nach Lösungen zu suchen. 

"Da sind wir jetzt dabei, eine Lösung zu finden wo wir Telefone und Mobiltelefone nutzen und auch noch eine ganz andere spannende Entwicklung nämlich Geld übers H   andy. Und das wird extrem viel genutzt nicht nur in Madagaskar sondern auch in anderen Ländern. Es ist fast einfacher hier in Madagaskar mit Mobil Money auf dem Markt sich eine Flasche Wasser zu kaufen als sich hier in Berlin eine Currywurst zu besorgen. Da braucht man immer Bargeld. Und das muss man wirklich sagen, dass in vielen afrikanischen Ländern ist man da schon einen Schritt voraus." Julius Emmrich

Mit einem internationalen Team von Ärzten, Softwareentwicklern, Wirtschaftsexperten und vielen ehrenamtlichen Helfern begannen Emmrich und Knauss eine neue IT-Infrastruktur für Madagaskar aufzubauen, durch die Patientinnen und Patienten, Gesundheitszentren und Geldgeber direkt miteinander verbunden werden. 

"Wenn man in ein System Geld rein gibt, um eine bestimmte Intervention zu machen, zum Beispiel um 50 000 Kinder zu impfen, dann geht es im Moment nur darüber, dass man auf die Pyramide oben an das Gesundheitsministerium zum Beispiel einen gewissen Betrag überweist und die es dann umsetzen." Samuel Knauss

Das Problem dabei? Bis das Geld vom Gesundheitsministerium in Form von Behandung bei Patientinnen und Patienten ankommt, ist rund die Hälfte der Mittel auf dem Weg in undurchsichtigen Kanälen verschwunden. 

"Wir wollen mit unserer Lösung das umgehen, indem wir diese drei Hauptplayer Ärzte, Patienten und letztendlich die, die Behandlung bezahlen, besser verbinden. Das machen wir darüber, dass wir jetzt ein Zugang schaffen direkt zu einem mobilen Gesundheitssparbuch für Patienten wo das Geld das auf dem Sparbuch liegt eben nur noch für Gesundheit ausgegeben werd en kann. / Dazu nutzen wir die bestehende Infrastruktur. Und haben einen Zugang zum Patienten geschaffen den jeder nutzen kann mit jedem einfachen Telefon. Man braucht dafür kein Smartphone und braucht kein Internet. Man kann sein altes Nokia Telefon rausholen und kann direkt sparen und direkt nur für Gesundheit sparen." Samuel Knauss

Bankkonten haben die wenigsten auf Madagaskar. Geld zuhause in einem Strumpf zurück zu legen, wäre viel zu unsicher, weil Bargeld schnell gestohlen werden kann. Durch die mobile Gesundheitsgeldbörse für's Handy sollen alle eine sichere Möglichkeit erhalten, Geld für die eigene Gesundheit zu sparen. 

"Es gibt Menschen die sitzen unter Schirmen oder in kleinen Hütten und nehmen Bargeld entgegen und wandeln das dann um in diese Währung die auf den Handys läuft. Man kauft sich kleine Rubbel Karten. Gibt einen Code ein. Und das wird dann eben auf dieses Gesundheitssparbuch aufgeladen." Samuel Knauss 

mTOMADY bedeutet übersetzt so viel wie kräftig oder gesund. Der Projektname ist Programm. Durch Ihre Low-Tech Lösung per Mobilfunk hat praktisch jeder auf Madagaskar Zugang zum neuen Gesundheitskonto, erläutert Samuel Knauss.

"Immer wo es Korruption gibt, gibt es auch jemand der diese Korruption betreibt. Aber das Interessante ist, dass sie di  e Spitze der Pyramide,  die wirkliches Interesse haben, dass es ein Gesundheitssystem gibt, das funktioniert und die ganz unten an der Pyramide, die die Behandlungen bekommen wollen. Dass die beiden haben großes Interesse dass auf dem Weg in der Mitte kein Geld verloren geht. Und das ist ein großer Vorteil für uns, weil letztendlich die entscheiden und die die es nutzen, die wollen das." Samuel Knauss

Weil viele Patientinnen vorab nie wissen, wie viel sie für eine Behandlung zahlen müssen, gehen sie lieber gar nicht zum Arzt, hat Samuel Knauss festgestellt. Die Gefahr, die ganze Familie durch zu hohe Gesundheitskosten zu ruinieren, ist ihnen einfach zu groß.

"Ein Grund dafür ist, dass es keinen Katalog gibt, wie viel eine Behandlung kosten darf. Beziehungsweise diesen Katalog gibt es schon, aber der wird nicht eingehalten und ist nicht kontrollierbar. Und das ist ein weiterer großer Punkt den wir damit angehen, dass wir die ganze Behandlung, die auch über unser System abgerechnet wird, kontrollieren können, wieviel hat die Behandlung gekostet und wieviel wurde tatsächlich dafür ausgegeben." Samuel Knauss

## Kliniken profitieren vom Qualitätsmanagement

mTOMADY hat deshalb neben dem mobilen Gesundheitskonto auch noch Qualitätsmanagement für die beteiligten Kliniken eingeführt. Dazu wurde neben mediz inischen Qualitätskriterien auch ein Kostenrahmen für jede Behandlung eingeführt. Und ein internet basiertes Abrechnungssystem zur Kontrolle der beteiligten Kliniken, erläutert Niklas Riekenbrauck.

"Die Handy Technologie funktioniert über GSM.  SMS und Technologien die nennt sich USSD. Das ist ein Echtzeit Kommunikationsmöglichkeiten, die parallel zum Internet funktioniert und die Mobilfunkanbieter erlauben es uns, diese Technologie aus dem Internet anzufassen. Das heißt, es gibt Schnittstellen bei den Mobilfunkanbietern, die wir über das Internet anfragen können oder auch in umgekehrter Richtung, wenn etwas auf dem Handy passiert, bekommen wir eine Nachricht, dass etwas auf dem Handy passiert ist und das wird dann direkt an unsere Infrastruktur, wo die Software läuft, geschickt." Niklas Riekenbrauck

Über ein eigenes Gateway senden die Mobilfunkanbieter in Madagaskar also die Daten der Gesundheitskonten direkt an das Abrechnungssystem von mTOMADY im Internet. So können über die Gesundheitscloud alle Zahlungen direkt verwaltet und die Behandlungkosten jederzeit eingesehen werden. Der Softwareingenieur musste dazu eine eigene Schnittstelle entwickeln, die Daten zwischen den Mobilfunksystemen und dem internet austauschen kann. Vorher gab es das auf Madagaskar nicht.

"Es gibt diese zentralisierten Schnittstellen in anderen Ländern in Afrika in Asien.  Madagaskar ist eins der Länder, wo es keine einzige Integration gibt. Und wir waren die Ersten in Madagaskar, die diese Integration bereitgestellt bekommen haben. Wir speichern dann, welcher Patient wie viel Geld und so weiter hat. Und wenn dann eine Behandlung ansteht wird das eingetragen in das System des Gesundheitsdienstleister, des Krankenhauses. Und wir speichern internen bei uns, wieviel Geld sozusagen übertragen werden soll." Niklas Riekenbrauck

Am Ende des Monats überträgt mTOMADY dann die Gelder der Gesundheitssparbücher direkt auf die Bankkonten der beteiligten Gesundheitszentren. Die Kliniken können also sicher sein, die Behandlungskosten auch wirklich erstattet zu bekommen. Und die Patientinnen und Patienten wissen vorab, was eine Behandlung kosten darf. Die neue Gesundheitsgeldbörse schafft also zugleich Transparenz im Gesundheitssystem.

"Teil unseres Systems ist eine Abrechnungssoftware, die auf von uns bereit gestellten Android-Tablets funktioniert. Man kann Beweise für eine Behandlung in Form von Bildern hochladen und über diese Tablets findet auch eine Kommunikation mit unseren Mitarbeitern statt, die auf medizinischer Seite überprüfen, dass die Behandlung wirklich richtig abgerechnet wurde." Niklas Riekenbrauck

Für die Kliniken, die jetzt schon ein Qualitätssiegel von mTOMADY erhalten haben, ist der Effekt deutlich spürbar. In einigen Spitälern ist die Zahl der Geburten pro Monat von 5 auf 60 gestiegen, - noch bevor das mobile Gesundheitskonto freigeschaltet war. Allein die Gewissheit, eine gute Behandlung zu erhalten und dafür nicht zu viel bezahlen zu müssen, hat viele überzeugt mit weniger Risiko als bei einer Hausgeburt in einer Klinik zu entbinden. Der positive Effekt des mTOMADY Systems hat sich in Madagaskar inzwischen herum gesprochen. Nicht nur bei Patientinnen sondern auch bei Kliniken. Auch eine Krankenhausketten hat sich beworben, künftig bei mTOMADY mitmachen zu dürfen. 

## Podcast Service 

Die Vorstellung von mTOMADY ist der Beginn einer Serie von audio-Beiträgen über technische Veränderungen, die unsere Welt bewegen. 
Abbonieren sie unseren newsletter, wenn Sie über weitere Folgen von Technik und Welt informiert werden wollen. Falls Sie selbst eine Podcast-Serie für Ihr Unternehmen produzieren lassen möchten oder als Sponsor unserer Beiträge genannt werden wollen, kontaktieren Sie uns gerne direkt.









