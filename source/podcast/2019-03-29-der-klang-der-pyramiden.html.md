---
date: 2019-03-29 09:50 PDT
permalink: der-klang-der-pyramiden
blog: podcast
title: Der Klang der Pyramiden
short_title: Philosoph entschlüsselt archäologisches Rätsel
tags:
  - Musik
  - Mathematik
  - Archäologie
  - Philosophie
image: pyramide.jpg
image_alt_title: Pyramiden sind in Stein gemeisselte Harmonie
soundcloud_link: 597775320
published: true
author: Jutta Schwengsbier
introartikel: Weil in der Antike Philosophie, Mathematik und Musiktheorie noch eine Einheit waren, glichen Bauwerke in Stein gemeißelten musikalischen Harmonien.
---

## Die Nay-Flöte wird Maßeinheit

Die Nay-Flöte wird in Ägypten schon seit Jahrtausenden aus Schilf-Rohren gebaut. Doch wie war es unseren Vorfahren gelungen, den reinen, schönen Klang der Flöte über die Jahrtausende zu bewahren? Und ihn immer wieder aufs neue hervorzubringen? Erste Bauanleitungen der Nay sind bis heute auf Papyrus-Platten erhalten, dem wichtigsten Schreibmaterial der Antike, erläutert Friedrich Wilhelm Korff. Der emeritierte Professor für Philosophie an der Universität Hannover hat anhand alter Dokumente untersucht, wie die musikalischen Harmonien, wie der reine, schöne Klang der Flöte, weiter gegeben werden konnte.

> Die Nay Flöte gibt es heute noch in Ägypten. Die wird heute auch noch gespielt. Und dann gibt es dazu den Spruch eines Meisters aus dieser Zeit, der zu seinem Gesellen sagt: Geh an den Nil und schneide mir ein Rohr. Das Rohr muss neun Knoten haben. Und der Daumen muss rein passen. Damit war die Rohrlänge vorgegeben. Damit war auch der Grundton vorgegeben. (***Friedrich Wilhelm Korff***)

Um die Flöte mit dem gleichen Klang nachbauen zu können, wurden also die Bau-Maße von Generation zu Generation überliefert. Korff stellte sich die Frage, ob der harmonische Klang so einer Nay - Flöte vielleicht Ausgangspunkt war für die Definition der ägyptischen Elle als Maßeinheit, mit 7 Handbreit und 28 Fingern als Unterteilung? Korff beauftragte einen bekannten Flötenbauer mit einem Experiment. Marwan Hassan sollte eine Schilfrohr -Flöte bauen, die dem alt-ägyptischen Ellen-Maß entspricht.

>Jetzt machen wir eine typische Korff Größe. Er wollte die Nay ja als 105 Zentimeter haben. Was nicht geht, weil kein Mensch das spielen kann. Daher haben wir uns auf 52,5 Zentimeter geeinigt. Weil diese Größe kann man spielen. Sie ist ja die Hälfte und dementsprechend auch dann gleich gestimmt, halt nur eine Oktave höher als seine Vorstellung von einer Nay. Das heißt, ich muss dieses Rohr, was ich hier habe, das ist 56 Zentimeter, irgendwie kriegen auf 52 Zentimeter. Dann mach ich das mal (***Marwan Hassan***)


## Orientalische Musik als harmonische Vorlage

Das Ergebnis: Das auf die Länge der altägyptischen Elle gekürzte Schilfrohr hat, noch ohne Löcher, genau den Grundton D. Ist das also der Klang, dem schon die Ägypter vor Tausenden von Jahren lauschten? Und wo genau gehören die Löcher hin?

>Also. Es gibt in der orientalischen Musik, in der arabischen Musik 24 Noten. Und eine Nay wird dementsprechend in 24 Stücke geteilt. Man kann sich vorstellen, ich teile jetzt die Länge der Flöte in 24 Stücke, allerdings mit Verschiebungen. Und an diesen Punkten setzt man die Löcher. Natürlich muss man wissen wo. (***Marwan Hassan***)

Damit die Töne harmonisch klingen, mißt Hassan die Gesamtlänge des Rohres und bohrt die Löcher dann an Positionen, die die Länge proportional teilen. Die Bohrlöcher müssen zunächst noch etwas kleiner sein, damit der Ton noch verändert werden kann, erläutert Hassan. Denn der Ton wird nicht nur von der Länge der Flöte bestimmt, sondern auch vom Durchmesser und der Dicke der Rohrwände. Und das ist bei jedem Rohr anders. So ganz genau kann also niemand vorher berechnen, wohin die Löcher gehören und wie groß sie sein müssen. Die Feinabstimmung macht Hassan deshalb immer mit dem Gehör.

Wie Flötenbauer in Ägypten es schon seit Jahrhunderten getan haben, brennt auch Hassan die kleinen, vorgebohrten Löcher noch mal mit einem glühenden Eisenstab weiter aus. Dabei entscheidet er intuitiv, ob ein Loch etwas weiter nach unten oder nach oben gehört. Ob es etwas größer oder kleiner sein muss. Am Ende entsteht der richtige Klang durch jahrelange Erfahrung eines Meisters im Flötenbau.

>Ich spiele es jetzt an. Eigentlich stimmen die ganz. Ich muss da nicht viel ändern. Also,   das mache ich jetzt genau in der Mitte und kratze den Mist hier weg und weis dann, ob ich nach unten oder nach oben drücke.  (***Marwan Hassan***)

Das Experiment scheint gelungen. Eine nach dem altägyptischen Ellen Maß gefertigte Schilfrohr-Flöte ist sehr, sehr gut gestimmt. Alle Töne dieser Nay-Flöte klingen für menschliche Ohren wie der Inbegriff für musikalische Harmonie.

Hier könnte die Geschichte eigentlich zu Ende sein. Doch wie bei allen guten Geschichten ist die Lösung der ersten Frage nicht das Ende, - sondern der Anfang einer noch viel spannenderen Geschichte. Nein, es geht nicht um ein Märchen aus Tausend und einer Nacht. Dem Philosophen Friedrich Wilhelm Korff ist es tatsächlich gelungen, im harmonischen Klang der Nay und dem ägyptischen Ellenmaß den Schlüssel zur Lösung eines archäologischen Mysteriums zu finden.

Seit Jahrhunderten rätselten Wissenschaftler der Archäologie, der Ägyptologie und auch einiger weniger seriösen Fachbereiche, von der Esoterik bis zur Ufo-Forschung, warum die Pyramiden in Ägypten trotz ihrer imposanten Größe eine immer gleichbleibende geometrische Schönheit ausstrahlen. Wie gelang es den alten Ägyptern derart große Gebäude zu bauen, die in sich harmonisch wirken? Das Rätsel entschlüsselte nun mit Hilfe der Musiktheorie kein Altertumswissenschaftler sondern, - der Philosoph Friedrich Wilhelm Korff. Der 77 jährige lehrte lange als Professor an der hannoverschen Leibnitz Universität, interessiert sich gleichermaßen für Technik und Mathematik wie für die Musik. Als Philosoph ist er zudem versiert in Latein und Altgriechisch. Korff gehört also zur heute seltenen Spezies des Universalgelehrten. Weil es Korff gelang, wieder zusammen zu führen, was in der Antike untrennbar verbunden war, kam ihm die entscheidende Idee: Philosophie, Mathematik und Musiktheorie waren bis in die Renaissance die Fundamente einer Architektur, die den schönen Gleichklang von Melodien zu in Stein gemeißelten Harmonien verwandelten. Und hier beginnt die eigentliche Geschichte: Es ist die Geschichte vom Klang der Pyramiden.

## Das entschlüsselte Rätsel: Pyramiden sind harmonisch wie Musik

Die meisten Menschen empfinden die Klänge von Musik als harmonisch, ohne groß darüber nachzudenken. Aber was genau erscheint uns als harmonisch und was als unharmonisch? Was Harmonie tatsächlich bedeutet, ist vor allem eine philosophische Frage. Und so war es auch ein deutscher Philosoph, Gottfried Wilhelm Leibnitz, der Harmonie definierte als “Einheit in der Vielfalt”. Auf die Musik übertragen bedeutet das, dass aus der Vielzahl aller Töne nur diejenigen für unser Empfinden eine harmonische Einheit bilden, die sich in einem bestimmten proportionalen Schwingungsverhältnis überlagern. Denn nur diese schwingen gewissermaßen in einem harmonischen Gleichklang. So wie weißes Licht eigentlich aus einem Gemisch bunter Spektralfarben besteht, die bei Regenwetter gelegentlich als Regenbogenfarben sichtbar werden, so ist ein Ton eigentlich auch ein Gemisch miteinander schwingender Teiltöne: Der sogenannten Obertöne.

Georg Faust, der früher 1. Solocellist der Berliner Philharmoniker war, ist nicht nur selbst ein herausragender Musiker. Wie Friedrich Wilhelm Korff interessiert sich der 60 jährige auch für Musiktheorie. Wann klingen Töne und ihre Teiltöne, die sogenannten Obertöne harmonisch? Und ab wann tun sie das nach unserem Empfinden nicht mehr, obwohl sie es eigentlich theoretisch sein müßten?

>Das ist ein D. Das ist der erste Oberton. Das ist der zweite Oberton. Der dritte Oberton. Vierte Oberton. Fünfte Oberton. (***Georg Faust***)
>Die Septime. (***Friedrich Wilhelm Korff***)
>Da geht’s schon los. (***Georg Faust***)
>Da klingt's schräg. (***Jutta Schwengsbier***)
>Da sin mer wieder in der Oktave.(***Georg Faust***)

Das erste Ergebnis des Tonexperiments: Je höher die Schwingungsfrequenz eines Obertons wird, um so schwerer fällt es unseren Ohren und damit unserem Gehirn, diese noch als harmonisch zu erkennen. 

>Die Frequenzen Stimmen ganz genau. 440 Hz zu 330 Hz. Dann wissen wir, aha, das ist eine Quarte. Polizei Signal, nicht. Polizeihorn Signal. Das hört sich banal an, aber die Töne in unserem Handy. Bei der Straßenbahn und über all. Das sind einfache Signale. Das sind einfache Intervalle aus einfachen Zahlen. Und meistens nur von 1-10. Drüber nicht. Ab zehn, man hat noch den Ganzton, neun geteilt durch acht. Aber wenn schon bei Zehn Neuntl, das ist der kleine Ganzton. Das geht auch noch. Aber wenn sie Elf-Zehntel haben, empfinden Sie das schon nicht mehr als harmonisch. Obwohl es harmonisch ist.(***Friedrich Wilhelm Korff***)

Bei schrillen Tönen versagt also irgendwann unser Empfinden für Harmonie. Das zweite Ergebnis des Tonexperiments:

>Je höher wir in der Oktave kommen, um so mehr Obertöne gibt es. In der ersten Oktave haben sie keinen Oberton. (Tonbeispiel) In der zweiten Oktave haben sie einen Oberton. Das ist die Quinte. (Tonbeispiel) In der dritten Oktave haben sie schon vier, glaub ich. (Töne anhören) Habe sie drei Obertöne dazwischen. Dann haben sie sechs oder sieben Obertöne und dann haben sie 25. Also das potenziert sich. Mit jeder Oktave. Das hören wir nicht mehr. Aber die sind immer noch da. Die Obertöne. Das ist eine Struktur, die ist dem Ton sozusagen eingegeben. (***Georg Faust***)

So wie sich heute der Star-Cellist Georg Faust für harmonische Klänge und die Frequenzresonanzen zwischen dem Grundton und seinen Obertönen interessiert, genau so hat auch schon der griechische Philosoph Platon in der Antike Tonexperimente gemacht. Über Abbildungen von alten Tontafeln hat Friedrich Wilhelm Korff Platons Musiktheorie nun wieder entdeckt. Und darüber ist Korff dann auch auf den in der Antike bekannten Zusammenhang zwischen Mathematik und harmonisch klingenden Ton-Intervallen gekommen. Ein Ton-Intervall, also zwei gleichzeitig oder nacheinander erklingende Töne verschiedener Höhe, erscheint uns demnach immer dann als harmonisch, wenn die Frequenzen in einem bestimmten Verhältnis geteilt werden. Platon konnte natürlich noch keine Schwingungsfrequenzen messen, wie das Musiker heute tun, um Töne verschiedener Instrumente aufeinander abzugleichen. Doch inzwischen ist bekannt, dass die Längen-Verhältnisse von Instrumenten-Saiten exakt den messbaren Schwingungsverhältnissen von Tönen und ihren Obertönen entsprechen. Die geometrischen Abmessungen von Instrumenten und die harmonischen Klänge, die damit produziert werden, stehen also immer in einem proportionalen Verhältnis zueinander.

## Harmonische Seitenverhältnisse

Wenn zum Beispiel eine Gitarrensaite genau in der Mitte heruntergedrückt wird, erklingt noch einmal der gleiche Ton, - nur eine Oktave höher. Schon Platon hatte herausgefunden, dass uns diejenigen Töne als zueinander harmonisch erscheinen, deren Klang proportionalen Zahlenverhältnissen entsprechen. Die Proportion zwei zu eins, also das Tonintervall einer halben Saitenlänge zur ganzen Gitarrensaite, entspricht einer Oktave. Es klingt in unseren Ohren wie der gleiche Ton, - nur höher. Wenn die Saite im Verhältnis drei zu zwei heruntergedrückt wird, erklingt eine Quinte, und bei vier zu drei teilt sich der Ton im Frequenzverhältnis einer Quarte. Und wenn diese Intervall-Töne gemeinsam erklingen, verschmelzen sie zu einer Einheit, die uns als sehr harmonisch erscheint.

Und genau über diese harmonische Aufteilung von Tönen, die proportional geteilten Seitenverhältnissen entsprechen, kamen schon die griechischen Philosophen der Antike auf den engen Zusammenhang von Musik, Arithmetik und Geometrie. Demnach erscheinen uns besonders die geometrischen Formen als harmonisch, die genau im Verhältnis musikalischer Ton-Intervalle geteilt sind. Über die Wiederentdeckung der platonischen Musiktheorie gelang es dem zeitgenössischen Philosophen Friedrich Wilhelm Korff das Rätsel der Pyramiden zu lösen, das Ägyptologen bislang verborgen geblieben war.

So wie Musik-Akkorde in unseren Ohren harmonisch erscheinen, so sind die Proportionen der Pyramiden in unseren Augen seit Jahrhunderten ein Inbegriff von Schönheit. Warum? Die gigantischen Pyramiden in Ägypten erscheinen uns deshalb so harmonisch, sagt Korff, weil ihre Seitenverhältnisse harmonischen musikalischen Intervallen entsprechen. Doch wie wurde die Musiktheorie mit den harmonischen Teilungsverhältnissen der Töne auf Bauwerke übertragen?

Hier sind wir wieder bei unserem Experiment mit der Nay-Flöte. Damit aus der Vielzahl aller möglichen Töne harmonische Ton-Intervalle werden, müssen die Löcher der Flöte an genau definierten Stellen gebohrt werden. Das ist eine Technik, die schon seit Jahrtausenden bekannt ist, sagt Korff.

>Wie man darauf gekommen ist? Schon in der Steinzeit gibt es Flöten aus Knochen mit Bohrlöchern. Irgendwann mal hat man herausgefunden, dass die Löcher so einen Abstand haben müssen, dass eine reine Quinte erklingt. Wenn es ein unsauberes Intervall ist, dann klingt die Flöte nicht. Aber wenn die Löcher so gebohrt sind, dass sie eine Quarte ergeben haben. Aha. Das klingt gut. Warum klingt es gut? Weil das Gehirn fähig ist, die einfachen Zahlen in Proportionen zu setzen. Das sind alles musikalische Intervalle. Die das Gehirn sofort sieht. Und wenn das Gehirn etwas erkennt, dann freut er sich. (***Friedrich Wilhelm Korff***)

Bei der altägyptischen Nay-Flöte wird die Länge des Rohres proportional in 24 gleiche Teile geteilt und nur an diesen Stellen wird gebohrt. Marwan Hassan rechnet die richtigen Positionen vorher immer genau aus. Aber konnten das auch schon die alten Ägypter?

 >Gut. Wie hat man das früher gemacht? Man macht es ja stellenweise heute noch so. Also ich war vor drei Jahren in Ägypten. Da bin ich also auf den super Flötenbauer gestoßen. Na, ja, also es war (lacht) ne ungewöhnliche Begegnung. Jedenfalls hat er drauf bestanden, dass ich die Flöten ganz falsch baue. Wir haben darüber gesprochen, ich hab gesagt, ich berechne, da sagt er nix mit rechnen. Das machst du einfach so. Machst die drei Finger drauf und hier drei Finger, also er nutzt seine Finger als Maß für die Flöte.(***Marwan Hassan***)

Die Finger als Maß sind in der Tradition des Flötenbaus in Ägypten also immer noch erhalten. Aber was passiert, wenn die Finger der Flötenbauer unterschiedlich dick sind. Stimmt der Ton dann noch? Marwan Hassan lacht. Nein, natürlich nicht. Genauso wenig wie die Angaben mit dem Schilfrohr mit den neun Kammern und dem Daumen als Durchmesser exakte Maßangaben sind.

 >Der Sinn und Zweck dieser 9 Kammer Art war ein Maß. Ein Maß zu finden. Heute haben wir ein Meterband. Also ich kann gehen und mit einem Meterband genau messen was ich brauche. Ferner gab es auch einige Nay, die nicht dieser 9 Kammern Art entsprechen. Man hat ja einige gefunden. Und auch gespielt. (***Marwan Hassan***)

>Es ging ja drum. Es sollte ein Maß sein. Und das Maß war eben, neun Kammern, schöner Ton und daraus eben die Elle.(***Jutta Schwengsbier***)
>Ja.(***Marwan Hassan***)
>Mit den Handbreit und den Fingern.(***Jutta Schwengsbier***)
>Aber die neun Kammern sind ja immer verschieden groß. Also es gab ja nicht die eine Flöte. Das ist ja auch ein Problem. Es gibt nicht die eine ägyptische Flöte, sondern es gab diverse Größen. Also es gibt 7 diverse Größen. Sieben Maße. 68 Zentimeter ist ein C. 60 Zentimeter ist ein D. 54 Zentimeter ist ein E. 51,5 Zentimeter, grob, kann größer oder kleiner sein, je nachdem wie dick die Flöte ist, ist ein F. Das G war dann 44,5. Dann kommt das A. Also heut ist das dies Set, das man braucht, um das ganze Spektrum der Musik zu decken. Welches davon ist die Elle? (***Marwan Hassan***)

## Das altägyptische Maß basiert auf den ersten 5 Primzahlen

Welcher Grundton ist also der richtige Klang, um die Maßeinheit der ägyptischen Elle daraus zu bilden? Die erste Antwort auf diese Frage gibt die Philosophie: Ganz egal mit welchem Grundton man beginnt: Eine Einheit in der Vielfalt der Töne entsteht immer dann, wenn die Längenverhältnisse der Flöte wie von Hassan beschrieben proportional geteilt werden. Denn die Partial- und Obertonreihe existiert unabhängig davon, welchen Grundton die Nay-Flöte hat. Wie Korff herausgefunden hat, gab es in der Antike tatsächlich andere Tonarten als die Dur- und Molltonarten, die wir heute für unsere Musik verwenden. Die Grundtöne waren also andere als die, die wir heute kennen. Für die Harmonie spielt das keine Rolle. Nicht der Grundton, sondern die proportionale Aufteilung der Löcher entscheidet darüber, ob eine Flöte in sich harmonisch klingt.

Die zweite Antwort, warum die alte ägyptische Königs-Elle genau 52,5 cm lang ist, gibt die Mathematik. Genauso wie der Meter heute für uns ein geeichtes Standardmaß ist, etwa um Bauwerke berechnen zu können, so war die Elle früher für die Ägypter ein geeichtes Standardmaß. Abgeleitet aus der Länge und den Lochabständen einer bestimmten Nay-Flöte.

>Die Länge von 52,5 cm entspricht gar nicht unserer natürlichen Elle. Die ist viel zu lang. Ja gut. Aber sie hat die ersten fünf Primzahlen. Und das ist das Wichtige. Diese Elle ist nämlich 52,5 cm, ist 7 × 7,5 handbreit und 1,875 ist ein Finger. Und 28 Finger geben eine Elle oder sieben handbreit. Das ist das ägyptische Mess- und Maß-System. Und dieses Mess- und Maßsytem ist durchgehalten worden. Die Ägypter haben sich dadurch ausgezeichnet, dass sie von Anfang an, wenn sie etwas festgesetzt haben, dabei blieben. Bis in die Römerzeit. (***Friedrich Wilhelm Korff***)

Der Vorteil dieser Rechenweise: Vielfache und Brüche aus den ersten 5 Primzahlen ergeben immer ganze reale Zahlen. Irrationale Zahlen, also Zahlen mit unendlichen vielen Stellen hinter dem Komma, waren zum bauen in der Antike nicht geeignet. Deshalb wurden Dezimalzahlen auch nicht genutzt, glaubt Korff. 

>Man kann Pyramiden mit allen Zahlen bauen. Das ist möglich. Dann kriegt man förmige und unförmige Pyramiden. In der Antike, ob es Ägypten oder Griechenland war, hatten nur die ersten fünf Primzahlen. Und das ist noch nicht bekannt. Die Architektonischen Proportionen sind Zahlen gleich der musikalischen Proportionen. (***Friedrich Wilhelm Korff***)

## Ein Klang komponiert für die Ewigkeit.

Für unsere Augen erscheinen die Pyramiden oder andere Bauwerke der Antike also genauso harmonisch wie die Klänge von Musik in unseren Ohren. Denn ihre Proportionen sind nichts anderes als in Stein gehauene Ton-Intervalle.

>Und warum sie so schön ist, diese Pyramide, hängt damit zusammen, dass in der ersten Reihe der natürlichen Zahlen die Partial- und Obertonreihe erklingt. Eins zu zwei ist die Oktave. Zwei zu drei ist die Quinte. Und so weiter. Unser Gehör sieht die einfachen Konstellationen, die ganz einfachen Konstellationen. Die Intervalle, die harmonisch sind, haben immer im Zähler eines mehr als im Nenner. Zwei zu Eins. Drei zu Zwei. Vier zu Drei. Und so weiter. (***Friedrich Wilhelm Korff***)

Weil Philosophie, Mathematik und Musik in der Antike noch eine Einheit waren, gelang es den Menschen also damals Bauwerke zu schaffen, die bis heute unsere Gefühle und unser Empfinden für Schönheit ansprechen.

>Da ist die Einheit des Pyramidions: Das ist die Spitze der Pyramide. Der Abschlussstein. Und die Proportion des Abschlusssteins hat zugleich auch die ganze Pyramide. Da ist die Einheit in der Vielheit dargestellt. Das Pyramidion wurde vergoldet. Und es war das Letzte, dass man abends sah, wenn die Sonne unterging. Man sah die Sonne nicht mehr. Aber man sah noch die Spitze leuchten. Und es war das erste Licht, dass man morgens sah. Solche emotionale Gewalt hatte dieses Pyramidion auf die Ägypter. (***Friedrich Wilhelm Korff***)

Auch wenn das goldene Licht des Pyramidions bereits zu Zeiten der Römern zerstört wurde. Die ästhetische Schönheit der Pyramiden ist erhalten geblieben. Denn die Sprache der Mathematik und der Musik ist zeitlos. Der Klang der Pyramiden wurde komponiert für die Ewigkeit.
