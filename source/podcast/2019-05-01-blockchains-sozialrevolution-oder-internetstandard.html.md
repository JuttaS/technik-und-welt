---
date: 2019-05-01 23:15 CEST
permalink: blockchains-sozialrevolution-oder-internetstandard
blog: podcast
title: Blockchains - Sozialrevolution oder Internetstandard
short_title: Blockchains - Hoffnung auf ein besseres Leben?
tags:
 - Blockchain
image: blockchains.jpg
image_alt_title: Blockchains bilden eine neue dezentrale Internetstruktur
soundcloud_link: 614166051
published: true
author: Jutta Schwengsbier
introartikel: Mit Hilfe von Blockchains soll die Macht von Internetgiganten wie Google, Facebook, Amazon und Co gebrochen werden. 
---

Eine Solo Performance in einem Berliner Kulturzentrum das Radialsystem heisst. Das ehemalige Pumpspeicherwerk am Spreeufer ist heute eine der wichtigste Schnittstellen zwischen Kunst und Politik in der Hauptstadt. Die alte Maschinenhalle, in prachtvoller Backsteingotik, überragt ein gläsernern Kubus. So facettenreich wie die Aussenfassade ist auch das Program.

Das Bühnenbild ist ein Provisorium. Nur mit einem Stuhl und einem Tisch. Darauf ein Laptop und eine kleine Lampe. Für mehr war keine Zeit, entschuldigt sich der Künstler aus den USA gleich zu Beginn. Er sieht ein bisschen so aus, wie man sich die Romanfigur Harry Potter mit Mitte Dreißig vorstellen würde. Mit schmaler, zierlicher Gestalt. Und kleiner, runder Brille. Auch sein Auftritt ähnelt der Geschichte des unscheinbaren Superhelden Harry Potter. In seiner Lesung vermittelt der Darsteller von sich selbst ein Bild, als sei er ganz alleine los gezogen ist, um die bösen Mächte zu bändigen, die versuchen, die Weltherrschaft zu übernehmen.

 Josh Fox erzählt in seiner Solo Performance die Geschichte seines Lebens. Es ist zugleich die Geschichte der neuen Bürgerbewegung in den USA. Als Aktivist an forderster Front engagiert sich Josh Fox mit Tausenden anderen jungen Amerikanern gegen die Machtübernahme von Oligarchen in den USA. Josh Fox gestikuliert wild mit den Armen, als er einen Brief vorliest. Geschickt hatten ihn amerikanische Gasunternehmen, die Probebohrungen auf seinem Grundstück machen wollten. Mitten in einem Naturschutzgebiet am Fluss Delaware. Auch alle seine Nachbarn hatten so einen Brief bekommen. Die Konzerne boten ihm viel Geld dafür, mitten im wichtigsten Wasserreservoire der Millionenstadt New York nach Gas bohren zu dürfen. 

>Am Anfang wußte niemand etwas. Warum boten sie uns so viel Geld für Probebohrungen? Ich habe versucht es herauszufinden. Was ich gefunden habe, war absolut Angst einflößend. Menschen konnten ihr Wasser anzünden, weil Gas in die Trinkwasserversorgung eingedrungen war. Wegen der vielen Chemikalien im Wasser wurden ihre Kinder und Tiere krank. Ihr komplettes Leben war zerstört. (***Josh Fox***)

Als er mit seinen Recherchen anfing, hatte noch niemand etwas von Fracking und seinen Folgen gehört. Nach der Veröffentlichung seiner beiden Dokumentarfilme Gasland 1 und Gasland 2 entstand eine neue Umweltbewegung, die sich weltweit gegen die Gasgewinnung durch Fracking wehrt. Und dann erzählt Josh Fox vom eigentlichen Anlass für seinen Besuch in Deutschland. Gemeinsam mit anderen amerikanischen Umweltaktivisten ist er nach Berlin gekommen, um an einer Konferenz der Energy-Web-Foundation teilzunehmen. Das Ziel der Organisation: Über die Blockchain-Technologie soll ein weltweites Netzwerk für erneuerbare Energien aufgebaut werden. Josh Fox will mit Hilfe der Blockchain-Technologie die Oligarchen in den USA entmachten, - ganz ohne Zauberstab. Kann die neue internet Technologie wirklich die Welt aus den Angeln heben? Am Tag nach seiner Performance im Radialsystem begleite ich Josh Fox zu einer Blockchain Konferenz.

##Zivilgesellschaft und Technik Visionionäre verbünden sich

Schon auf dem Weg dorthin, in der U-Bahn, ist kaum noch deutsch zu hören. Dafür englisch, spanisch, französisch oder russisch. Die Gesichtszüge der Reisenden sind so vielfältig wie in der UNO-Generalversammlung. Einige sind sicher Touristen. Viele andere könnten aber auch für eines der zahllosen Startups arbeiten, die in Berlin inzwischen viele Hinterhöfe übernommen haben.Dann bin ich in Kreuzberg angekommen. Dem Multi-Kulti-Zentrum der Stadt. Im U-Bahn-Durchgang steht eine Strassensängerin. Mit Headset und kleinem Verstärker tritt Ms. Solity, wie sie sich selbst nennt, gegen Sexismus und Alltagssklaverei auf.

Die neue Technologie ist angetreten mit dem Versprechen, die Welt von gierigen Bankern - und anderen Mittelsmännern - zu befreien. Blockchains sind eine Antwort auf die Finanzkrise von vor 10 Jahren, bei der US Banken ihre faulen Immobilienkedite über die Börsen weiter verkauft hatten, - an nichts ahnende Kundinnen und Kunden weltweit. Über Blockchains, so die Idee, soll künftig jeder mit jedem direkt Handel treiben können. Völlig Transparent. Ohne weiter Banken, Notare oder Großkonzerne als Vermittler zu brauchen. Kann die neue, dezentrale Infrastruktur des Internet wirklich die Welt grundlegend verändern? Ich bin mit vielen Fragen angekommen im Kraftwerk Berlin. Die „Event Horizon“ genannte Blockchain-Konferenz hat das ehemalige Industrie Areal zu einem Gesamtkunstwerk umgebaut.

Mit Sound Installationen im Eingangsbereich. Und Lasershows in den Vorhallen. An den Info-Ständen stehen Anzugträger entspannt neben jungen Hipstern mit bunten Haaren und Kaki Hosen. Computer Hacker und die Geschäftswelt scheinen in der Atmosphäre keine Berührungsängste zu haben.

Oben, im ersten Stock, schwingen zu Beginn wie in einer Zirkuskuppel Trapezartisten über die Bühne. Einige Lichtblitze simulieren hell leuchtende Strombögen wie sie der Spannungswandler einer Trafostation erzeugt. Dann endet die Show. Und die Gesprächsrunden beginnen.

Über eine Videoinstallation sind rundum alle Vortragenden und ihre Grafiken gut zu sehen. Aus dem Off ist immer wieder eine Stimme zu hören, die sich als „Gott“ vorstellt und Regieanweisungen gibt. Die Aktivisten scheinen wirklich große Visionen zu haben. Ewald Hesse, gekleidet in Hawaiihemd und legerer Hose, malt redegewandt das exponentielle Wachstum der Blockchain-Welt aus. Der Mitbegründer der Energy Web Foundation war einer der ersten, der die Etherium genannte Blockchain mit programmiert hat. Nun will er ein neues, universelles Energienetz auf Bockchain-Basis bauen. Wie bei solchen von Experten aus der ganzen Welt geprägten Veranstaltungen üblich wird auf dem Podium englisch gesprochen.

>Im Moment werden zentrale Netzwerke von dezentralen Blockchains abgelöst. Wir stehen am Anfang eines neuen Zeitalters, in dem sich die Informationstechnologie zur Werte-Technologie wandelt. (***Ewald Hesse***)

Bezahlt werden können über Blockchains Transaktionen jeder Art mit Hilfe virtueller Währungen. Zum Beispiel mit Bitcoin oder Ether. So kann über das neue Energienetz bald jeder nicht nur Strom kaufen sondern auch verkaufen. Von Haus zu Haus. Von Strasse zu Strasse. Von Stadt zu Stadt. Weltweit. Ohne Zwischenhändler. Entmachtet werden sollen mit der neuen Werte-Technologie also nicht nur die bisherigen Champions des Informationszeitalters, wie Google, Facebook oder Microsoft. Auch die bisherigen Geschäftsmodell der großen Energiekonzerne werden mit dem neuen weltweiten Energy-Web ausgehebelt. Denn die Energie-Blockchain ermöglicht jedem Hausbesitzer oder Elektorauto-Besitzer selbst zum Stromhändler zu werden, erläutert Christoph Jentzsch.

>Wir hatten mit dem Projekt „Share and Charge“ ein Projekt, wo wir Ladesäulen an die Blockchain angeschlossen haben. Jemand, der ein Elektro Auto hat, der kauft sich oft auch eine Ladesäule dazu. Und er konnte die anbieten auf dem öffentlichen Markt, das andere dort laden. (***Christoph Jentzsch***)

Christoph Jentzsch hat zunächst auch die Etherium Blockchain mit programmiert. Inzwischen hat der Physiker Slock.it gegründet. Das startup will jeder Maschine auch ein Portemonnaie geben und entwickelt dazu neue Bezahlsysteme für das Internet der Dinge. Die Visionen von Jentzsch klingen noch wie Science Fiction,- könnten aber trotzdem in wenigen Jahren Realität sein.

>Eine Maschine braucht ein neues Teil und bestellt es bei einem 3-D Drucker. Der Druck das. Eine Drohne bringt das. Es ist alles völlig automatisiert. Und jetzt müssen die Maschinen sich gegenseitig bezahlen. Weiter geht es dann Maschine zu Mensch. Wir hatten jetzt zum Beispiel eine Anfrage von einem Dienstleister, wo Putzkräfte von dem Türschloss bezahlt werden. D.h. nur wenn sie wirklich kommen, 1 Stunde zum Beispiel sauber gemacht haben, dann etwas raus bekommen. (***Christoph Jentzsch***)

Nach der Vorstellung von technischen Ideen und Konzepten beginnen auf der Bühne Diskussionen über die gesellschaftlichen und sozialen Veränderungen, die mit der neuen Blockchain Infrastruktur möglich werden. Neben den Technologie-Experten sitzen nun Vertreter von UNO Organisationen, von Bürgerinitiativen und von den Kirchen. Auch Josh Fox treffe ich hier wieder. Er tritt jetzt nicht als Performancekünstler auf, sondern spricht für die amerikanische Bürgerrechtsbewegung.

>Sie werden nichts erreichen, ohne den guten alten Aktivismus. Wir sprechen darüber, die mächtigsten Industrien anzugreifen, die es derzeit auf der Erde gibt. Die Industrie fossiler Energieträger wird nicht ohne Kampf aufgeben. Ihnen gegenüber steht die Bewegung gegen Fracking. Die Klimaschutzbewegung. Die Bewegung gegen Oligarchen in unserer Politik. Wenn Blockchains funktionieren sollen, dann müssen Sie demokratisch organisiert werden. Die Öl- und Gasindustrie basiert auf Gier. Auf Gewalt. Auf Rassismus. Auf Kolonialismus. Und Kriminalität. Ist die Blockchain moralischer? Die Frage kann nur mit ja beantwortet werden, wenn die Technologie auch moralisch genutzt wird. Dazu muss sie sich den Bürgerinitiativen öffnen. (***Josh Fox***)

Josh Fox hat sich vom Performancekuenstler auf der Bühne zu einem charismatischen Führer gewandelt, der die Technologieexperten fuer einen politischen Wandel begeistern will. Josh Fox fordert die Blockchain Vordenker auf, sich mit ihm und den Bürgerrechtsaktivisten gegen die Öl- und Gaskonzerne zu verbünden. 

Vom Aufruf zu einer sozialen Revolution bis hin zur Beschreibung voll automatisierter Fabriken, deren Maschinen miteinander verhandeln und sich gegenseitig Geld für ihre Leistungen bezahlen, ist bei der Blockchain Debatte alles dabei.

Am Ende schwebt wieder die Gauklertruppe über die Bühne. Ein Konfetti Feuerwerk bildet den Schlussakkord einer Veranstaltung, die viele Visionen ausgemalt hat aber noch wenig Konkretes zeigen konnte. Im Moment gleicht die ganze Blockchain-Welt einem Jahrmarkt. Fast täglich entstehen neue Angebote, die mit der Blockchain-Technologie bestehende Konzepte verbessern und andere dadurch vom Markt drängen wollen. Wer am Ende gewinnen wird, ist noch völlig unklar. Eines der größten Versuchslabore für die Entwicklung von Blockchains ist Berlin. Um mir ein besseres Bild machen zu können, beschließe ich, mir noch einige konkrete Blockchain Anwendungen anzusehen.

##Mikro-Investment per Blockchain

Eines der Innovationszentren für Blockchain Projekte ist das ehemalige Umspannwerk in Berlin Kreuzberg. Heute arbeiten in dem Backsteinbau kleine startups der IT-Szene Tür an Tür mit Branchengrößen wie Metro. Bald soll in dem Gebäude der google Campus einziehen. In der fünften  Etage teilen sich mehrere Blockchain startups ein Großraum Büro. An den Tischen sitzen überwieged junge Männer, die konzentriert an ihren Laptops arbeiten. Frauen sind immer noch selten in der Tech-Szene zu finden. An der Wand wirbt ein Plakat für Bitwalla, eine neue Bitcoin Bank. Ich habe mich aber mit den Vertretern von Brickblock verabredet. Das startup will Immobilien über einen Blockchain-Marktplatz verkaufen. Brickblock hat seine Büros am linken Seitenflügel. In einem Raum sitzen die Programmierer, die den Kern der Blockchain entwickeln. Beim Gespräch im Konferenzraum sitzen mir dann Experten verschiedener Fachrichtungen gegenüber. Ein Rechtsanwalt im dunklen Anzug. Der Cheftechnologe im bunt bedruckten T-Shirt. Ausserdem Immobilienexperten und Marketingfachleute. Und einer der jungen Gründer von Brickblock. Bei Blockchains geht es nicht nur um die Technologie, erläutert der Chef-Technologe Philip Paetz. Die Blockchain werde vor allem als Vehikel benutzt, um Geschäftsprozesse zu automatisieren. Bei den startups in Berlin ist englisch längst die gängige Kommunikationssprache.

>Ein Notar, einen Rechtsanwalt. Normalerweise muss man für diese vertrauenswürdigen Parteien in der Mitte immer Gebühren bezahlen. Diese Gebühren können vermieden werden, wenn man Blockchain-Technologie benutzt. (***Philip Paetz***)

Wie bei anderen online-Börsen auch werden Kauf und Verkauf der digitalen Anteilsscheine direkt auf der Brickblock Plattform abgewickelt. Alle notwendigen Verträge zu den Immobiliendeals sind bei einem Träuhänder hinterlegt. Die Daten zum Besitzerwechsel werden in der Blockchain gespeichert. Sie sind öffentlich einsehbar und fälschungssicher. Weil Kauf und Verkauf der Immobilienanteile sowie Zinszahlungen über die Blockchain automatisch ablaufen, sei der ganze Prozess viel effektiver aber auch preiswerter, wirbt Jakob Drzazga für das Konzept. Der Immobilienmanager hat Brickblock mit gegründet.

>Ich habe früher ganz traditionell in Immobilien investiert. Es dauert normalerweise sehr lange, bis sie einen Käufer finden. Bei meiner letzten Immobilie brauchte ich sechs Monate, bis ich das Geld tatsächlich erhalten habe. Über die Blockchain geht das viel schneller. Ausserdem können sie auch nur einen Teil der Immobilien, die sie halten, wieder verkaufen. (***Jakob Drzazga***)

Wer normalerweise in Immobilien investieren will, zahlt 5 bis 10 Prozent Gebühren. Bei Brickblock nur 0,5 Prozent. Wegen der vollautomatisierten Prozesse sind ausserdem Investitionen schon ab 1 Euro möglich. Damit sei Brickblock, sagt Manuel Gonzales Alzuru, einer der ersten Anbieter für Mikroinvestitionen weltweit. Alzuru ist Marketingleiter bei Brickblock.

>Mit unserer Technologie kann wirklich jeder am finanziellen System teilnehmen. Nicht nur die Eliten. Für mich ist die Blockchain-Technologie deshalb wirklich revolutionär. (***Manuel Gonzalez Alzuru***)

Da ist sie also wieder, die Diskussion um eine soziale Revolution, die Blockchains ermöglichen sollen. Für Brickblock heißt dass, jedem Menschen weltweit Zugang zu Investitionsmöglichkeiten zu geben. Unabhängig vom Standort und der Höhe der Anlagesumme. Viele der Interessenten kommen tatsächlich aus Afrika und sogar aus Ländern wie Afghanistan oder Pakistan. Dort haben immer noch Millionen Menschen keinen Zugang zu Bankdienstleistungen. Krypto-Währungen wie Bitcoin oder Ether haben vielen Menschen erstmals ermöglicht, ganz einfach über eine Internet Anwendung Geld an Verwandte in anderen Ländern zu senden. Brickblock wäre einer der ersten Marktplätze für Mikroinvestionen. Falls das gelingt, käme das tatsächlich einer sozialen Revolution gleich. Bis es so weit ist, dauert es aber auch bei Brickblock noch einige Zeit. Ich beschließe deshalb, mir noch eine Blockchain Anwendung anzusehen, die nicht nur mit großen Möglichkeiten für die Zukunft wirbt, sondern tatsächlich schon Geld verdient.

##Vernetzung von Automobil Marktplätzen 

Auch shelf.network, so der Name des erfolgreichen Blockchain-startups, arbeitet in einem Zentrum für Unternehmensgründer in Stadtmitte. Es ist eine eher ruhige Wohngegend und lange nicht so quirrlig wie Kreuzberg. In einem von aussen unscheinbar wirkenden Verwaltungsgebäude werden vor allem High-Tech Entrepreneure gefördert. 

Lasha Antadze, geborgen in Geogien, hat lange in der Ukraine Blockchain Technologie mit aufgebaut. Er sieht so aus, wie auch die Stars aus dem Silicon Valley angefangen haben. Ende 20. Gekleidet in ein kariertes Hemd und verwaschene Hose. Ein outfit, das eher nach Holzfäller als nach Geschäftsmann aussieht. Doch trotz seines jugendlichen Aussehens ist Antadze ein alter Hase der Szene. Und ein Experte, was die Blockchain Entwicklung angeht.

>Viele der Infrastruktur Projekte zielen direkt auf die Milchstraßen Galaxie. Sie bieten nichts ausser einer Vision. Tatsächlich verkaufen sie nichts außer dem Traum, dass es dezentralisiert ist. (***Lasha Antadze***)

Ob ein Marktplatz im Internet zentral oder dezentral organisiert werde, interessiere am Ende niemand, glaubt Antadze.

>Diese dezentralisierten Marktplätze können viele Dienstleistungen gar nicht erbringen. Transport muss zum Beispiel zentral organisiert werden. Ein Reiseanbieter, der nur Informationen über Flüge zeigt, aber keine Buchung ermöglicht, wird nicht funktionieren. (***Lasha Antadze***)

Mit dem von ihm gegründeten Unternehmen shelf.network versucht der 28jährige deshalb nicht, bestehende Plattformen zu ersetzen, wie andere das propagieren. Statt dessen verbindet Antadze über seine Blockchain mehrere Auktionsplattformen in Ost- und Westeuropa die Gebrauchtwagen versteigern. Nutzer in der Ukraine oder in Georgien können also gleichzeitig einen Wagen ersteigern, den eine deutsche Leasinggesellschaft verkaufen will. So etwas war bislang nicht möglich. Das ist das wirklich Neue bei der Blockchain, sagt Antadze. Computerprogramme legen transparent und für alle nachvollziehbar das Ergebnis einer Auktion fest.

>Bei der Blockchain gibt es keine zentrale Stelle mehr, der alle vertrauen müssen. Wenn zum Beispiel eine Regierung eine Auktion veranstaltet, hat sie bislang auch entschieden, wer gewonnen hat. Mit der Blockchain Technologie legt nun ein Konsens Algorithmus fest, wer am höchsten geboten hat oder wer bei einer Auktion überhaupt mitmachen durfte. (***Lasha Antadze***)

 Lasha Antadze gehört zu einer jungen Generation von Osteuropäern, die mit Hilfe der Blockchain auch Korruption oder Wahlfälschung bekämpfen wollen.

>Wir haben mit kommunalen Verantwortlichen in der Ukraine gesprochen. Sie sagten ganz offen, ich habe 50 Jahre gebraucht, um an meinen Posten zu kommen. Willst du nun meinen Enkeln die Zukunft nehmen, mit deiner Technologie? (***Lasha Antadze***)

Während Regierungsvertreter in Osteuropa und anderswo Staatseigentum bislang unter dem Tisch zu Spottpreise an Freunde weitergeben, wäre Korruption bei einer Blockchain-Auktion nicht mehr möglich.

>Jeder, der im Moment über Blockchain spricht, nennt einige Feature. Es ist transparent. Die Daten sind öffentlich zugänglich und können von niemandem verändert werden. Es ist ein verteiltes Netzwerk, das zwischen individuellen Nutzern aufgebaut wird. Wenn man diese einzelnen Elemente betrachtet, dann fällt schnell auf: Das alles gibt es schon seit 30 Jahren. Was ist also neu an der Blockchain? Mit der Blockchain verändert sich die Art, wie Entscheidungen getroffen werden. (***Lasha Antadze***)

Mit einer Blockchain braucht man also keine Regierung oder andere zentrale Instanzen mehr, die festlegen, wer bei einer Parlamentswahl gewonnen hat. Das erledigt das dezentrale Netzwerk, das von niemand manipulieren werden kann. Und darin liegt das eigentlich revolutionäre Potential der Blockchain. Der neue Weltcomputer, der gerade aufgebaut wird, ermöglicht endlich demokratische Abstimmungen, die fälschungssicher sind. Wahlmanipulationen, wie sie von Russland über die Ukraine bis in die USA schon vorgekommen sind, wären dadurch deutlich erschwert. Eine soziale Revolution ist dadurch zwar nocht nicht erreicht. Zumindest helfen Blockchains künftig dabei, die Welt tatsächlich ein bisschen gerechter zu machen.