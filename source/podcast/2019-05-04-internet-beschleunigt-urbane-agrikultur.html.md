---
date: 2019-05-04 11:05 CEST
permalink: internet-beschleunigt-urbane-agrikultur
blog: podcast
title: Stadtgärten - Wie das Internet die urbane Agrikultur beschleunigt
short_title: Internet beschleunigt urbane Agrikultur
tags:
 - Stadtgärten
 - Landwirtschaft
image: Konzert-im-Prinzessinengarten-Berlin.JPG
image_alt_title: Konzert im Prinzessinengarten Berlin
soundcloud_link: 615555585
lybsin_link: 9651740
published: true
author: Jutta Schwengsbier
introartikel: Inzwischen leben 50 Prozent der Weltbevölkerung in Städten. Ein wichtiger Trend der Zukunft sind deshalb sicher Stadtgärten.
---

Ob Rinderzucht in städtischen Naturschutzarealen oder mobile Gärten, die auf städtischem Brachland als Zwischennutzung ungewöhnliche Kartoffelsorten in Rollcontainern züchten: Immer mehr Hobbygärtner erobern urbane Räume zurück - für den Natur- und Artenschutz aber auch als Kommunikationsort.

##Der Prinzessinnengarten Berlin ist zum beliebten Kulturort geworden

>Der Prinzessinengarten, wir bezeichnen uns als mobile soziale Landwirtschaft. Ich fange einfach mal am Anfang an. Die Idee dazu kommt aus Kuba. Ich hab da mal als junger Kerl zwei Jahre gelebt auch ohne gärtnerische Ambitionen. (***Robert Shaw***)

Bei seinem Besuch in Kuba lernte Robert Shaw den städtischen Gemüsegarten als Ort der Begegnungen schätzen. Obwohl er selbst kaum spanisch sprach, kam er hier völlig unkompliziert in Kontakt mit Nachbarn und sicherte zudem seinen täglichen Gemüsevorrat.

>Hinten machen irgendwelche Kinder Hausaufgaben. Also in meinem war's so. Vorne saß ein Opi mit ner Thermoskanne Kaffee. Der war sehr süß. Wenn ich da Gemüse ernten wollte, dann hab ich gesagt, ich hätte gerne Bananen. Dann hat er gesagt, ja da hinten. Geh mal selber holen. Bist zum Kochbananenbaum gegangen, hast die Dinger abgeschnitten, hast dich ein bisschen verwirrt, dass die grün waren. Das Kind hat dir erklärt, die müssen grün sein. Die gelben kann man nicht essen. (***Robert Shaw***)

Gemeinsam mit einem Freund wollte Robert Shaw dieses Modell städtischer Landwirtschaft als Ort gemeinschaftlichen Lebens auch in Berlin ausprobieren. Auf einer Brache mitten im Multikultibezirk Kreuzberg gründete er einen mobilen Öko-Garten, mit Kartoffeln, die in Säcken wachsen und Gemüseanbau in großen Brötchenkörben aus Plastik. Das know how haben zunächst die Kreuzberger Nachbarn aus ihrer türkischen, bosnischen oder russischen Heimat  eingebracht, erzählt Robert Shaw lachend. Denn als Filmemacher war er selbst zunächst nur wenig qualifiziert, einen Gartenbaubetrieb zu leiten.

>Wir versuchen eben doll alte Sorten anzubauen. Das hier ist blaue Hilde, so eine Bohnenart, die wahnsinnig lecker ist und beim kochen knackig grün wird. Wir haben jetzt tatsächlich angefangen auch Veranstaltungen zu machen mit unserem Garten. (***Robert Shaw***)

##Erinnerungsorte in Kolumbien

Vor kurzem trat auch der kolumbianische Hip-Hop Sänger Aka im Prinzessinnen-Garten auf. Bei einem Symposium über das Stadtgärtnern stellte der Sänger, der mit bürgerlichem Namen Luis Fernando Àlvarez heißt, eine Kulturinitiative aus Medellin vor. In der für seine Drogendealer und Gewaltexzesse berüchtigten kolumbianischen Metropole entstehen aus Hip-Hop Konzerten gepaart mit städtischer Landwirtschaft Orte des Austauschs und der Erinnerung.

>Bei Agroarte arbeiten drei Generationen mit. Hip-Hop zieht natürlich vor allem die Jüngeren an. Aber Agrarkultur ist ja immer noch ein zentrales Element unserer Kultur, das viele Menschen anspricht. In Kolumbien wurden viele einfach enteignet und mussten ihr eigenes Land verlassen. Hier in der Stadt haben sie sich nun andere Orte zurück erobert. (***Luis Fernando Àlvarez***)

Im Stadtgarten von Medellin erarbeiten Musiker gemeinsam mit Jugendlichen die Texte für Hip-Hop Reime. Meist, sagt Aka, sind ihre Verse sehr politisch. Die ruhige Atmosphäre im Garten und die Gespräche helfen vielen dabei, tief sitzende Traumata zu verarbeiten.

>Das Drogenproblem in Medellin ist immer präsent. Ein Teil unseres Projektes ist deshalb auch die Aufarbeitung unserer eigene Geschichte. In Kolumbien wird das ansonsten totgeschwiegen. Es wird weder in Schulen unterrichtet noch werden die begangenen Kriegsverbrechen sonst thematisiert. Agroarte hat sich von Anfang an damit beschäftigt. Seit 1973 sind in Kolumbien mindestens 60.000 Menschen verschwunden. Um den Vermissten und Ermordeten zu gedenken haben wir eine Mauer bepflanzt. Inzwischen gibt es mehrere solche Orte der Erinnerung. (***Luis Fernando Àlvarez***)

##Stadtgärten sind Stadtoasen – auch in Kanada

Egal ob in Medellin oder in Berlin: Stadtgärten sind viel mehr als Orte, um Gemüse anzubauen. Sehr oft verwandeln sich die Gärten zu städtischen Oasen, die ein Miteinander der Kulturen und sozialen Austausch ermöglichen. Auch Toronto habe das schon früh erkannt, sagt Rhonda Teitel-Payne. Die 50jährige koordiniert den Aufbau und die Pflege der öffentlichen Stadtgärten in der mit rund zweieinhalb Millionen Einwohnern bevölkerungsreichsten Stadt Kanadas.

>Wir haben ein Team, das sich mit einer Ernährungsstrategie für Toronto beschäftigt, als Teil des Gesundheitsmanagement. Wir wollen nicht nur sagen: Sie müssen mehr Gemüse essen. Statt dessen kümmern wir uns darum, wie Nahrungsmittel hergestellt und in der Stadt verteilt werden. Viele Studien belegen: Wer seine eigenen Nahrungsmittel anbaut, der ißt mehr Gemüse. Außerdem fördert gärtnern die Bewegung. Die Leute gehen raus und arbeiten gemeinsam mit anderen auf öffentlichem Land. Das ist gut für die psychische Gesundheit. Es reduziert Isolation, Stress und Unwohlsein. (***Rhonda Teitel-Payne***)

Obwohl das Klima im Norden des amerikanischen Kontinents den Gemüse- oder Obstanbau nicht gerade begünstigt: Das Stadtgärtnern gilt schon lange als ein wichtiger Teil der öffentlichen Gesundheitsvorsorge in Kanada.

>Die Leute fragen oft nach der Quantität der Nahrungsmittel. Welcher Anteil ihrer täglichen Ernährung kommt aus der städtischen Landwirtschaft? Aber die Bedeutung des Stadtgärtnerns ist viel größer als nur Quantität. Oder sogar als der Nährstoffgehalt. Bei uns kommen Menschen aus allen Ländern der Erde. Es gibt viele Nahrungsmittel, die sie gerne essen würden. Sie können es in Toronto vielleicht kaufen. Aber dann ist es nicht frisch sondern importiert. Es ist nicht in der Qualität, die sie gewohnt sind. So etwas selbst anbauen zu können, oder aus einem Nachbargarten kaufen zu können, ist so wichtig. In Bezug auf die geistige Gesundheit und die physische Gesundheit. Zugang zu solchen Nahrungsmitteln zu haben ist für viele wirklich wichtig.  (***Rhonda Teitel-Payne***)

Toronto hat sich deshalb schon vor 20 Jahren entschlossen, Stadtgärten als Teil seiner kommunalen Entwicklungsstrategie zu fördern. Am Anfang, erinnert sich Teitel-Payne, musste sie dafür noch viel Überzeugungsarbeit leisten.

>In allen Städten gibt es Wettbewerb um öffentliches Land. Einige wollen ihre Hunde ausführen. Andere brauchen Sportplätze oder Spielplätze. Als wir Stadtgärten planten, hieß es deshalb: Warte mal, wenn du da einen Stadtgarten hin machst, dann können wir den Platz nicht für etwas anderes nutzen. Es wurde gefragt: Können wir öffentliches Land für private Zwecke freigeben? Wir argumentierten: Selbst wenn jemand nur für sich selbst und seine Familie Nahrungsmittel anbaut, dann ist der Garten trotzdem Teil der Grünflächen. Sie schaffen einen Lebensraum für Insekten. Er ist offen für alle, die sich dort aufhalten wollen. (***Rhonda Teitel-Payne***)

Im Schnitt können Stadtgärtner zwischen 10 und 20 Prozent ihres Eigenbedarfs an Nahrungsmitteln decken. Für die meisten, sagt Teitel-Payne, ist ihr Gemüse aber vor allem eines: Unbezahlbar.

>Wir haben einer Gruppe von Gärtnern angeboten, ihre Produkte zu verkaufen, weil wir eine Marketingstrategie entwickeln wollten. Sie sagten uns, wir wollen gar nichts verkaufen. Alles ist frisch und organisch. So etwas hatten sie selbst immer gesucht. Geld war einfach nicht so wertvoll. (***Rhonda Teitel-Payne***)

##Urban Gardening als kommunale Entwicklungsstrategie

Inzwischen nehmen sich viele Metropolen weltweit Toronto als Vorbild. Stadtgärtnern wird Teil von kommunalen Entwicklungsstrategien. So hat auch Paris, trotz des anfänglichen Widerstandes etlicher Verantwortlicher der Stadtverwaltung, ein Netz von Gärten aufgebaut. Ein neu gegründeter Ernährungsrat in Berlin entwickelt derzeit einen Forderungskatalog für eine neue, regionale Ernährungsstrategie. Eines ist allen gemeinsam: Die Nachfrage nach Stadtgärten übersteigt bei weitem das Angebot. Und bislang, sagt Teitel-Payne mit einem schmunzeln, kann selbst Toronto, als Vorreiter für das Stadtgärtnern, nicht alle Wünsche erfüllen.

>Viele versuchen alles mögliche zu machen, was sie nicht tun sollten, ohne zu fragen. Im Moment müssen wir uns um Hühner kümmern. Es gibt viele in der Stadt, die Hühner in ihrem Hinterhof halten. Das ist eigentlich illegal. Es gibt sogar einen kleinen Industriezweig, der Ferien für Hühner anbietet. Wenn also ein Kontrolleur kommt und ihnen sagt: Sie müssen ihre Hühner loswerden. Dann gibt es jemanden, der für eine Zeit darauf aufpasst, um die Hühner zu verstecken. (***Rhonda Teitel-Payne***)

##Gesundheitsvorsorge durch Stadtgärtnern

Die Nichtregierungsorganisation RUAF wurde gegründet, um unsere Ernährungsversorgung auch künftig zu sichern. Das Ziel: Mehr Raum für urbane Landwirtschaft zu schaffen und Erfahrungen auszutauschen. Im globalen Norden überwiegen vor allem soziale und gesundheitliche Aspekte beim Stadtgärtnern. Dagegen ist die urbane Landwirtschaft im globalen Süden eine Überlebensfrage, sagt RUAF Direktorin Marielle Dubbeling.

>Nahrungsmittel anbauen kann besonders in den Städten des globalen Südens wichtig sein, um Geld zu sparen. / Um ein Beispiel zu geben: Familien, die in Quito Nahrungsmittel anbauen, sparen ungefähr 150 $ jeden Monat. Das entspricht dem Mindestlohn in Ecuador. Aber das können sie nicht kommerzielle Landwirtschaft nennen. Die Stadt stellt das Land und auch die Geräte fürs Gärtnern kostenlos zur Verfügung. (***Marielle Dubbeling***)

##Ernährungssicherung im globalen Süden durch Eigenanbau

Zur Finanzierung hat sich Quito etwas ganz besonderes einfallen lassen. Die Hauptstadt Ecuadors nutzt CO2-Zertifikate, um damit seine Gärten zu finanzieren, erläutert Dubbeling. Industrien, die für Luftverschmutzung verantwortlich sind, müssen für diese Zertifikate bezahlen. Zum Beispiel müssen europäische Kohlekraftwerke Kompensationsmaßnahmen für ihren CO2-Ausstoß finanzieren. Quito bietet im Rahmen dieses Konzeptes seine Stadtgärten als Maßnahme zur Luftreinhaltung an.

>Quito ist eine der ersten Städte, die seine urbane Landwirtschaft über Bonds gegen Klimaveränderungen finanziert. Das ist ein Mechanismus, der von jeder anderen Stadt in der Welt auch genutzt werden könnte. Alle könnten von dem Beispiel aus Quito lernen. (***Marielle Dubbeling***)

##Digitalisierung fördert soziale Bewegung gegen Landgrabbing

Gesunde Ernährung ist ein Menschenrecht, sagt Dubbeling. Ermöglicht durch die Digitalisierung und neue Vernetzungsmöglichkeiten durch das internet sind inzwischen weltweit neue soziale Bewegungen entstanden, die dieses Recht auch durchzusetzen versuchen. Gegen korrupte und diktatorische Regierungen. Gegen die Übermacht von Agrarkonzernen und Supermarktketten. Das Maziringa Institut in Kenia hat zum Beispiel per internet eine Kampagne gegen Landgrabbing durch korrupte Regierungsmitarbeiter organisiert. Mit Erfolg. Dem Direktor des Maziringa Instituts, Davinda Lamba, gelang es schließlich, die illegale Privatisierung von öffentlichem Land wieder rückgängig zu machen.

>Im Jahr 2003, als sich in Kenia die Regierung änderte, gab es eine präsidentielle Kommission, die die illegale und irreguläre Zuweisung von öffentlichem Land untersuchte. Diese Untersuchungskommission veröffentlichte einen Bericht darüber, wie öffentliches Land veruntreut worden war. Wir haben Vorschläge unterbreitet, wie dieses Land wieder zurück gewonnen werden könnte. Ich habe in dieser Kommission mitgearbeitet. Unsere internet-Kampagne: „Blase die Trillerpfeife“, hat wesentlich dazu beigetragen, diese Kommission einzusetzen. (***Davinda Lamba***)

Auf Initiative des Maziringa Instituts hat inzwischen auch Nairobi Stadtgärtnern als Teil seiner kommunalen Entwicklungsstrategie etabliert. Inzwischen sind überall in der kenianischen Hauptstadt auf öffentlichem Land Kleinstgärten zur Selbstversorgung entstanden, erzählt Lamba.

>In den informellen Siedlungen in Nairobi finden sie fast alles. Getreide, Nutztiere. Fischteiche. Aber natürlich auch Gemüse. Weil die Flächen sehr klein sind, finden sie viele Innovationen wie „Gärten in einem Sack“. Dabei werden Säcke so präpariert, dass sie Kohl und Spinat darin anbauen können. Einige halten aber auch Hühner oder Ziegen. Sogar Schweine oder Hasen. Es gibt alles, was sie sich vorstellen können. (***Davinda Lamba***)

##Stadtgärtnern als Überlebensstrategie im Krieg

Wie in Nairobi hat auch in Syrien die Zivilbevölkerung Stadtgärtnern als Überlebensstrategie erkannt. Im Gegensatz zu Kenia werden die Gartenaktivisten im Kriegsgebiet massiv von der eigenen Regierung bekämpft. Das syrische Regime setzt Hunger als Waffe ein. Eine Strategie, die als Kriegsverbrechen gilt. Aktivisten einer 15th Garden genannten Selbsthilfegruppe versuchen die syrische Zivilbevölkerung zu unterstützen. Per internet werden auch Handbücher und Videopodcasts zum Gartenbau ausgetauscht. Eine der Unterstützerinnen ist Julia Watal, Biobäuerin aus Brandenburg. 

>Bauern wurden in der Vorbereitung der Aushungerung der Gemeinden, teilweise hunderte Bauern, verhaftet und sind verschwunden seitdem. Seit Jahren. In Gefängnissen. Oder ermordet. Also das auch jetzt, sechs Jahre nach Beginn der Revolution und dieses massiven Krieges gegen die Zivilbevölkerung in Syrien über 1 Million Menschen aktiven Hungerblockaden ausgesetzt sind. Aus politischen Gründen ausgehungert werden. Ins politische Aufgeben gehungert werden. (***Julia Watal***)

Die Biobäuerin versucht Syrern, die bislang noch nie Nahrungsmittel anbauen mussten, Grundkenntnisse im Gartenbau zu vermitteln. Wann werden die Pflanzen richtig gesetzt? Wie werden sie gedüngt? Wann können sie geerntet werden? Wie vermehrt man Saatgut? Vielen Städter wissen heute überhaupt nicht mehr was sie zum Überleben in einer Krise brauchen. Nicht nur in Syrien. Aber dort kosten die fehlenden Kenntnisse derzeit Menschenleben.

>Die Landwirtschaft hat zwar nie ganz aufgehört. Aber was mit Sicherheit nicht mehr existiert, sind große Farmen. Bauern werden bombardiert. Viele Felder werden ständig angegriffen, um die Ernte zu vernichten oder um den Anbau ganz unmöglich zu machen. Viele ihrer Materialien wurden auch gestohlen. Es ist sehr schwer nun wieder an Geräte für die Landwirtschaft heranzukommen. (***Abdallah Al Shaar***)

Syrische Oppositionelle, die wie Abdallah Al Shaar im Ausland leben, versuchen die in Krieg Eingeschlossenen mit Saatgut, Geräten und allem anderen zu versorgen, was das Überleben sichern kann. Hilfe erhalten die Syrer dabei auch von Netzwerken von Stadtgärtnern aus Europa oder Amerika, erläutert Watal.

>Da war wirklich der direkte Erfahrungsaustausch wichtig.Teilweise gab's schon Hungertote in den Städten. Zum Beispiel gab es ein Stadtviertel im südlichen Damaskus, wo die Leute keine gärtnerische Vorerfahrung hatten und wo sie innerhalb von kürzester Zeit einen großen Kollektivgarten aufgebaut haben. Auf der Fläche von einem ehemaligen Fußballfeld. In kleinen Flächen zwischen Häusern. Auf Balkons. Sie haben es geschafft, in der ersten Saison 20 % des Bedarfes der Bevölkerung zu decken. Von 18.000 Menschen, die damals dort in diesem Stadtviertel noch eingesperrt waren. (***Julia Watal***)

Sprecherin: Durch die Unterstützung von aussen konnten die Preise von Nahrungsmitteln zur Erntezeit fast wieder auf Vorkriegsniveau gedrückt werden, sagt Watal. Zuvor waren Reis oder Gemüse nur noch gegen Mondpreise zu haben gewesen.

>Man muss sich vorstellen, dass es über all da, wo es nichts gibt, in einer Kriegssituation, es dann doch irgendwas gibt. Und es dann ganz massive Schwarzmarktstrukturen gibt, wo Mafia ähnlich Leute sich bereichern am Verhungern der Menschen. Und eben auch diejenigen, die sie belagern, extrem viel Geld daran verdienen, dass sie bestimmte Sachen dann doch durch lassen. Das Leute ein Auto gegen ein Kilo Reis tauschen. 100 € kosten ein Kilo Reis. Und diese Produktion, die die Leute halt aufbauen, innerhalb von diesen Situationen, auch dem wirklich etwas entgegensetzen. (***Julia Watal***)

##Mit Blumen und Gemüse auf dem Dach gegen den Klimawandel

Kreative Lösungen beim Stadtgärtnern sind auch in bevölkerungsreichen Staaten wie Bangladesch gefragt. Im Sommer werden die Megastädte hier fast unerträglich heiß. Während des Monsuns stehen die Straßen dann Meterhoch unter Wasser. Der Bürgermeister Dhakas hat Stadtgärtnern zur wichtigsten Maßnahme seiner Stadtverwaltung gegen den Klimawandel erklärt. Mit neuen Bildungsplänen für die Schulen, mit Kulturveranstaltungen und Informationskampagnen soll die gesamte Bevölkerung zum Gärtnern animiert werden, erläutert Sahjabin Kabir. Die 30 jährige mit Harvard Abschluss in Architektur und Design ist eine der Stadtplanerinnen von Dhaka. 

>Wenn Kinder in den Städten aufwachsen, dann wissen sie nicht mehr woher das Essen kommt. Also kommt es wie Möhren aus der Erde? Oder wächst es wie Trauben am Busch? Woher kommt die Milch? Das wissen Sie alles nicht. Wir organisieren Informationsveranstaltungen darüber, wie Bäume auf dem Dach angepflanzt werden können. Bei uns gibt es sonst kaum noch freie Flächen und ausserdem ist Land viel zu teuer. (***Sahjabin Kabir***)

In Dhaka war lange überhaupt kein Grün mehr zu sehen, erzählt Sahjabin Kabir. Die neuen Stadtgärten sollen nun vor allem dazu beitragen, die Temperaturen in der Stadt zu senken. Es ist Klimaschutz, der vielen auch besser schmeckt.

>Im Moment hängt Dakha von Nahrungsmittellieferungen aus den ländlichen Gebieten ab. Die Lieferwege sind aber viel zu lang. Viele unserer Bauern benutzen gesundheitsschädliche Konservierungsmittel. Die Leute kaufen deshalb gab keine Früchte mehr im Laden, weil sie Angst vor dem Formalin haben. Mit dem eigenen Garten auf dem Dach essen wir nun wieder regionale Lebensmittel. Das ist viel sicherer. (***Sahjabin Kabir***)

In verschiedenen Facebook Gruppen tauschen viele der Neugärtner aus Dhaka inzwischen Fotos ihrer Dachgärten aus und geben sich gegenseitig Tipps zur Pflanzenpflege. Einige der virtuellen Gärtner-Netzwerke haben inzwischen mehr als Hundertvierzigtausend Mitglieder. Auch wenn immer mehr Menschen in Städten leben: Ohne Zugang zur Natur oder zumindest ein bisschen Grün in der Nachbarschaft fühlen wir uns einfach nicht wohl. Mal unterstützt durch kommunale Entwicklungsstrategien. Mal als Teil von Widerstandsbewegung gegen Landgrabbing oder diktatorische Regierungen. Dem Megatrend der Verstädterung folgt nun,- beschleunigt durch das internet – der Trend zum Gärtnern in der Stadt.