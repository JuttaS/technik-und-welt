---
date: 2019-05-06 15:01 CEST
permalink: klimaschutz-und-artenschutz-durch-direktvermarktung
blog: podcast
title: Klimaschutz und Artenschutz durch Direktvermarktung
short_title: Klimaschutz durch Regionalmärkte
tags:
  - Klimaschutz
  - Artenschutz
  - Landwirtschaft
image: Regionale-Zusammenarbeit-in-Landwirtschaft.JPG
image_alt_title: Philosoph Adrian Schwarz hilft Bäurin Iltraut Peters bei der Ferkelzucht
soundcloud_link:
lybsin_link: 9671543
published: true
author: Jutta Schwengsbier
introartikel: "Im Jahr 2050 müssen 50 Prozent mehr Nahrungsmittel hergestellt werden als 2013. Dazu müssen Bauern vor allem in Landwirtschaft investieren. (Kostas Stamoulis - FAO)"
---

>Die Saatgut Konzerne heute, die wollen nicht nur Saatgut verkaufen, die wollen Pestizide verkaufen. Syngenta, Nummer drei bei Saatgut, macht 75 % seines Umsatzes mit Pestiziden. Was also ganz dringend not tut, ist eine unabhängige Züchtung. Kleine, mittlere Züchtungsunternehmen, welche nicht in erster Linie Pestizide verkaufen wollen. Sondern Produkte herstellen für eine ökologische Landwirtschaft der Zukunft.  (***François Meienberg***)

>Ich sehe sehr viel Zukunft, im Regionalen. Weil alles andere wird wegfallen. Das ist einfach mein Gefühl. Es geht so, wie wir Landwirtschaft betreiben, das geht nicht. (***Sandra Kink***)

>Also wir haben hier Freiland Schweine, die jetzt zu Weide Schweinen mutiert sind. Wir haben Legehennen im Hühnermobil aber auch im festen Stall. Wir haben hier Junghennen Aufzucht. Dann haben wir Bruder-Hähnchen Mast. Bruder-Hähnchen sind die Hähnchen, die normalerweise nach dem schlüpfen vergast werden. (***Hans Christoph Peters***)

##Regionalvermarktung stärkt Kleinbauern

Die Peters sind Bauern aus Leidenschaft. In Werneuchen, 19 Kilometer vor Berlin, züchten sie Rinder, Schweine, Ziegen und verschiedene Arten Federvieh. Ausserdem bauen die Peters noch Gemüse an. So eine Vielfalt an Tieren und Ackerbau ist eher ungewöhnlich, - in einer sonst oft streng spezialisierten Landwirtschaft mit großen Mastbetrieben und Monokulturen auf den Feldern.

Möglich macht so ein Geschäftsmodell eine neue Online-Plattform für den Vertrieb regional hergestellter Lebensmittel. Die Marktschwärmer. Es ist der Versuch, Kleinbauern und andere Lebensmittelhandwerker wieder direkt mit ihren Kunden zu verbinden. Ohne Zwischenhandel. Mit 67 Jahren ist Iltraut Peters immer noch jeden Tag unterwegs, um die Tiere auf ihrem Bauernhof zu füttern oder um Unkraut zu jäten.

>Wissen Sie, meine Eltern sind Bauern gewesen. Früher. Die waren Bauern, wissen se, da hatten wir von Huhn, von Gans, von Ziege, alle Tiere. Tauben. Das mussten wir schon als Kinder. Wir haben praktisch davon gelebt. Dann mussten wir auch viel mithelfen. Alleine war das gar nicht möglich. Also jeder hat da seine Aufgabe. Da bin ich dann schon somit eingewachsen. (***Iltraut Peters***)

In offenen Ställen wärmen sich ihre kleinsten Ferkel zunächst noch unter der Höhensonne. Am Anfang brauchen auch Baby-Schweinchen viel Ruhe, erläutert Illtraut Peters, und verschlafen den Tag in ihren Strohbettchen.

>Wir hatten erst, ne Sau. Erste Mal war alles o. k. mit der. Wir hatten da glaube ich zehn Ferkelchen. Aber, ein Aufwand. Mit Lampe. Infrarot. Die brauchen viel Wärme. Wir haben gesagt, na gut, wir machen das weiter. Zehn Ferkel zu ziehen macht ja auch Freude. Macht ja auch Freude, wenn sie bei der Sau dann liegen. Und dann ihre Milch nehmen. Und dann, wie sie das zweite Mal dann schwanger war, dann hat sie die, man muss auch dabei bleiben, wissen se. Und die Ferkel kommen ja nicht alle auf einmal. Und die Zeit haben wir gar nicht. Und da hat sie sich so dumm angestellt und hat alles zerdrückt. Und dann haben wir gesagt: Nee, dieser Aufwand. Man füttert und dann kriegt man doch keine Ferkelchen. Dann haben wir gesagt, nee, dann hören wir damit auf. Kaufen uns lieber die, die schon ein bisschen handlich sind. Und die tollen gleich rum. (***Iltraut Peters***)

Raus zum toben auf die Wiese geht’s schon nach einigen Tagen. In einer Ecke eine große Wasserlache und Schlamm zum suhlen. In der Mitte große Futtertröge mit viel Platz zum raufen. Wenn Adrian Schwarz Kartoffeln oder Blumenkohl vorbei bringt, kann es den Ferkelchen nicht schnell genug gehen, bis sie dran sind.

>Und dann werden die auch gleich frech. Die gewöhnen sich sehr schnell an alles. Und dann sind sie auch super frech. (***Adrian Schwarz***)

>Und dann zwicken Sie einen schon. Und knabbern am Stiefel. Und so, weil sie wissen, da gibt's was. (***Iltraut Peters***)

Eigentlich hat Adrian Schwarz Philosophie studiert und lebt in Berlin. Auf dem Bauernhof der Peters macht er ehrenamtlich mit. Aus Spaß. Und um die Kleinbauern zu unterstützen.

>Da ist es ja einfach hier ein sehr Weg weisendes Modell. Denn Massentierhaltung ist ja leider immer noch sehr aktuell. Und sehr verbreitet. Mir liegt das Tierwohl sehr am Herzen. Auf dem Hof, muss man einfach sagen, geht's den Tieren sehr sehr gut. Da hier wirklich darauf geachtet wird, dass die Tiere Platz haben, frische Luft haben, dass sie eine soziale Ordnung aufbauen können. Das macht einfach Spaß. Es ist tätig, ne, mit den Händen. Man ist immer am machen. Man sieht einfach sehr schnell, was dabei herauskommt.  (***Adrian Schwarz***)

Zweimal pro Woche fährt Adrian von Berlin-Friedrichshain nach Brandenburg raus, um den Peters beim Tiere füttern, ausmisten oder Zäune bauen zu helfen. Deshalb kann Illtraut Peters auch mal selbst eine Auszeit nehmen.

>Sonst arbeite ich sieben Tage die Woche. Und wenn ich weiß, Adrian kommt. Dann kann ich auch mal frei machen. Das kann man sonst auch gar nicht, wissen se, weil man nur am Rennen ist. Jeder schreit morgens nach Futter. Da überlege ich, wen gibst du heute mal als Erste, kriegt heute Futter. (***Illtraut Peters***)

Dass jeder genug zu essen kriegt, ist gar nicht so einfach, erläutert der Philosoph, der inzwischen auch Fachmann für Ferkel-Aufzucht ist. Die kleinsten Ferkel werden oft von den größeren vom Trog gedrängt. Adrian hat deshalb einige Schützlinge, die er per Hand aufzieht und mit Namen benennt.

>Und Winfried, der war auch so einer, der immer nicht an den Trog gekommen ist. Dem bin ich immer hinterher, mit einer Kiste Broccoli und Kartoffeln hinterher gerannt und hab ihn dann per Hand gefüttert. Weil ich dachte, Winfried muss auch mal ein bisschen zunehmen. Das hat er dann auch gemacht. Winfried ist jetzt mittlerweile, hat er sich auch gemacht, hat sich gut getan auf jeden Fall. Das fand ich auch irgendwie süß, Das ist noch mal etwas besonderes. Wenn man dann noch mal so einzelne kleine Freunde hat. (***Adrian Schwarz***)

An individuelle Betreuung von zu klein geratenen Ferkeln wäre in einer industriellen Massentierhaltung gar nicht zu denken. So etwas macht nur ein Kleinbetrieb, der seine Tiere nicht nur als Ware betrachtet, sondern als Lebewesen mit eigenen Rechten. Die Lebensqualität ihrer Schweine und Hühner kann man am Ende auch schmecken: Davon sind die Peters überzeugt. Um sich so eine Vorzugsbehandlung ihrer Tiere auch leisten zu können, mußten sie vor allem Kunden finden, die so etwas auch zu schätzen wissen. Und das war gar nicht so einfach.

##Artgerechte Tierhaltung hat ihren Preis

Bei den Peters ist artgerechte Tierhaltung oberstes Prinzip. Die Hühner leben in offenen Ställen. Und in zwei Hühnermobilen. Dort können sie auf der Wiese nach Würmern scharren oder sich in einen geschützten Raum zurück ziehen: Eine Holzhütten auf Rädern. Mit Laufsteg, damit die Hühner frei rein und raus laufen können. Wenn die Wiesen anfangen braun zu werden, können die Hütten mit dem Fuhrwerk leicht umgesetzt werden. 

Die großen Mastschweine leben auch nicht wie sonst üblich eingesperrt in engen Ställen, sondern frei auf einer Grasweide. Sie gehören Hans-Christoph Peters, der als Jungbauer den Hof seiner Eltern mit bewirtschaftet.

>Also wir haben hier Freiland Schweine, die jetzt zu Weide Schweinen mutiert sind. Wir haben Legehennen im Hühnermobil aber auch im festen Stall. Wir haben hier Junghennen Aufzucht. Dann haben wir Bruder-Hähnchen Mast. / Bruder-Hähnchen sind die Hähnchen, die normalerweise nach dem schlüpfen (..) vergast werden. (..) Weil man die als Masttiere nicht weiter mästet. Sie sind für die Industrie nicht brauchbar. Weil sie ja zu lange brauchen, um überhaupt ihr Gewicht zu bekommen. Mindestens 16 Wochen. Und das macht keiner. Weil die normale Mastzeit sind 28-35 Tage. Und nicht 16 Wochen. (***Hans Christoph Peters***)

Die Peters vermarkten die Bruder – Hähnchen trotzdem, - als besondere Spezialität. Da kann der Preis dann auch etwas höher sein, erläutert Hans Christoph. So lohnt es sich wieder, die männlichen Tiere länger zu mästen, statt sie gleich nach der Geburt zu vergasen.

>Und in dem Fall hat mein Vater da schon vor über 20 Jahren angefangen. Wir bedienen da den vietnamesischen Markt in Berlin. Die Vietnamese kaufen sehr gerne diese Hähnchen. Weil die eben noch einen anderen Geschmack haben, beziehungsweise, weil die auch die Kämme, die wollen sie alle haben. Und die bleiben dran, nach dem Schlachten. Wir haben auch ein eigenes Schlachthaus auf dem Hof. Ein kleines Geflügel Schlachthaus. Da schlachten wir hier die Woche diverses Geflügel und auch das Weihnachtsgeflügel. Und dann vermarkten wir das nach Berlin. (***Hans Christoph Peters***)

Der gelernte Landwirtschaftsmeister hatte zunächst versucht, einen eigenen Laden für regionale Produkte aufzubauen. Doch es kamen einfach nicht genug Kunden. Deshalb musste er Insolvenz anmelden. Danach wieder ganz neu anzufangen, war sehr schwer, erinnert sich Hans - Christoph. Keine Bank will ihm mehr Kredit geben. Ohne groß investieren zu können, fing er deshalb auf dem Hof seines Vaters an, einige Hühner und Schweine zu züchten. Alles was er jetzt verdient, fließt direkt wieder in seine Betriebsentwicklung. Er leistet sich nicht mal eine eigene Wohnung, sondern lebt in einem Bauwagen am Waldrand.

>Man muss ja nicht glauben, dass die Brandenburger uns jetzt hier die Bude einrennen. Hier in der Pampa. Und dann sagen, Mensch, schön, schöner Bauernhof, hier kaufe ich jetzt immer ein. Die kommen zwar mal kucken. Aber eine Gans zu Weihnachten, das ist so das bisherige Geschäft gewesen. (***Hans Christoph Peters***)

##Onlineplattform Marktschwärmer ermöglicht Direktvertrieb

Kunden, die direkt im Hofladen kaufen, reichten den Peters bei weitem nicht aus, um den Betrieb halten zu können. Deshalb überlegte Hans-Christoph zunächst noch einmal, gemeinsam mit einem Freund einen online-Vertrieb für regionale Produkte aufzubauen. Bis sie bei ihren Recherchen schließlich entdeckten: So etwas gibt es schon. Auch in Berlin und Brandenburg. Auf einer Marktschwärmer genannten online-Plattform kann jeder, der es will,  regionale Produkte aus Berlin und Brandenburg kaufen. Für Hans-Christoph waren die Marktschwärmer entscheidend dafür, auch ohne Kredite nach seiner Insolvenz wieder als Kleinbauer starten zu können.

>Für mich war es eben von Anfang an einer der wichtigsten Vermarktungszweige, die ich habe. Ich mach damit Zwei-Drittel meines Umsatzes mittlerweile. Also ich würde mal sagen, es sind in der Woche bis zu 200 Haushalte. Also das ist ja spitze. Also es schwankt immer so zwischen 100 und 150, manchmal drüber, 180, sowas. Aber ich würd mal sagen, so 120 im Durchschnitt. Kommt schon hin. (***Hans Christoph Peters***)

Die Idee, die bäuerliche Landwirtschaft durch regionale Vermarktung zu fördern, stammt ursprünglich aus Frankreich. Wie das funktioniert? Die Kunden bestellen und bezahlen die Produkte auf der Internetplattform „Marktschwärmer“ und holen sie einmal in der Woche auf einem für sie nahe gelegenen Regionalmarkt ab. Das ist eine enorme Arbeitserleichterung für kleine Produzenten wie ihn, freut sich Hans-Christoph Peters.

>Und somit ist das System einfach total cool. Weil ich packe jetzt hier zusammen, kann tagsüber meinen Kram machen und verteile es abends nur noch. Wenn ich mal nicht kann, dann finde ich immer jemand, der sich die eineinhalb oder 2 Stunden ans Bein bindet und es dann im Notfall auch mal für mich macht. Dann kann ich noch Heu machen und alles und kriege meinen Tag auch ganz anders hin, als wenn ich tatsächlich auf dem Markt wäre. Oder irgendwo, wo ich den ganzen Tag stehe. Und auch nicht weiß, was ich verkaufe. Ich kann genau gucken schon, oh, wie ist der Verkauf diese Woche. Und dann kann ich eben reagieren. (***Hans Christoph Peters***)

Inzwischen hat Hans-Christoph über die Marktschwärmer viele Stammkunden in Berlin gefunden, die die Qualität seiner Produkte zu schätzen wissen. Und die auch bereit sind, dafür einen etwas höheren Preis zu zahlen.

>Und jetzt mit den Schweinen, da schlachten wir jetzt jede Woche. Da brauchen wir jede Woche Kunden, die das gerne wollen und auch 15 € für das Kilo ausgeben wollen. Die das bezahlen. Die haben wir in Berlin. (***Hans Christoph Peters***)

## Klimaschutz durch regionale Angebote statt Welthandel mit Nahrungsmitteln

Wir essen inzwischen wie selbstverständlich Lammfleisch aus Neuseeland und im Winter Erdbeeren aus Südafrika. Unsere Ernährung hat in Zeiten der Globalisierung kaum noch etwas mit regionalen Angeboten oder Jahreszeiten zu tun. Davon profitieren vor allem einige Großkonzerne, die weltweit Nahrungsmittel zu Billigpreisen einkaufen, sie über die Weltmeere transportieren und dann über Supermarktketten vertreiben. Das ist zwar billig. Schmeckt aber kaum noch und ist auch nicht ökologisch nachhaltig. Deshalb gibt es inzwischen wieder einen entgegengesetzten Trend.

Ermöglicht durch die Digitalisierung und neue Vernetzungsmöglichkeiten per internet entstehen wieder regionale Wirtschaftskreisläufe. Sandra Kink hat die Chancen gesehen. Mit viel Energie und Begeisterung für die Sache hat die Jungunternehmerin neben dem Kaffee Eden in Berlin Pankow einen Marktplatz für regionale Lebensmittel aufgebaut.

>Ich war einfach so begeistert von dieser Idee und hab gesagt, das geht. Das wird gehen, ich brauche einfach ein Angebot, dass verlockend ist. Mit den Worten bin ich zu den Bauern einfach auch gegangen und hab gesagt, so, ich hab wahrscheinlich auch eher gestrahlt, als mit den Worten viel ausgesprochen. Und hab einfach gesagt: Ich brauche euch. Ich brauche euch jetzt hier. Und das hat peu à peu immer mehr geklappt. (***Sandra Kink***)

##Digitalisierung ermöglicht regionale Wirtschaftskreisläufe

Die Gelegenheit, so einen regionalen Marktplatz im eigenen Viertel aufzubauen, bietet die online-Plattform Marktschwärmer. Dort kann jeder, der das will, regionale Lebensmittel anbieten. In Berlin und Brandenburg machen das neben Kaffees oder Markthallen zum Beispiel auch start-up Zentren als Nebengeschäft. Oder eben Jungunternehmerinnen wie Sandra Kink, die von der Sache so begeistert sind, dass sie das als Haupttätigkeit begreifen. Die Einnahmen teilen sich die Anbieter von Nahrungsmitteln mit der online-Plattform und den Regionalmarkt Organisatoren. Noch sind nicht alle Märkte erfolgreich. Aber Sandra Kink hat viel Zeit und Energie in ihr Projekt gesteckt. Das Geschäft brummt.

Auf improvisierten Tapeziertischen als Marktständen stapeln Gärtnerinnen beim Kaffee Eden nun einmal pro Woche ihre Gemüsekisten. Daneben stehen Kühlboxen mit geräuchertem Fisch von Glut und Späne. Einer Fischräucherei aus der Uckermark. Oder Steaks von Angus Rindern. Der „Schwarze Kuh“ genannte Nebenerwerbsbetrieb von Jungbauern will nachhaltig Qualitätsfleisch produzieren und regional vermarkten. Es gibt aber auch Backwaren, lokal gebrautes Bier, selbstgemachten Tofu und Schokolade, Kiezhonig und sogar Wild aus der Schorfheide. Je vielfältiger Ihr Angebot auf ihrem Regionalmarkt geworden sei, sagt Sandra Kink, um so mehr Kundinnen konnte sie auch von ihrem Konzept überzeugen.

>Ich hab dann Hans Christoph Peters Landwirtschaft im Sommer 2015 dazu bekommen. Die Gärtnerinnen haben mir gesagt, ja, wenn der Umsatz nicht steigt, dann gehen sie. Und da habe ich gesagt, o. k., dann mache ich das, dass der Umsatz steigt. Und hab ganz viel versucht. Das hat sich alles so toll entwickelt. Aber ich hab schon viel Energie rein gegeben. (***Sandra Kink***)

Die Idee stammt ursprünglich aus Frankreich, erzählt Sandra Kinke. Dort gibt es noch viel mehr bäuerliche Landwirtschaft als etwa in Deutschland. Um weg von industrieller Massentierhaltung zu kommen und hin zu Qualität und Tierschutz auch bei der Nahrungsmittelproduktion, ist eine Re-Regionalisierung der Wirtschaft ihrer Ansicht nach unverzichtbar. 

>Ich sehe sehr viel Zukunft, im Regionalen. Weil alles andere wird wegfallen. Das ist einfach mein Gefühl. Es geht so, wie wir Landwirtschaft betreiben, das geht nicht. Das ist irgendwann mal an der Grenze des machbaren. Und deswegen sind wir jetzt gerade sowieso auch in der Umstellung. Manchmal ist es sehr mysteriös, warum manches nicht geht. Es gibt schon auch Verkaufstage, da haben Sie vielleicht die Hälfte an Umsatz. Und je mehr außergewöhnliche Sachen sind, also so im Wochenwechsel, umso mehr kommen auch die Kunden. Weil sie sagen, ich bekomme das nirgends anders. (***Sandra Kink***)

Wo kann man sonst schon in Berlin Wild aus der Schorfheide kaufen? Für die gelernte Försterin Irina König ist der Regionalvertrieb über die Marktschwärmer eine Gelegenheit Damm-Wild, Rot-Wild, oder Schwarzwild direkt aus dem Wald an Kundinnen zu liefern.

>Das Wild lebt frei im Biosphären Gebiet. Bis es dann auf der Einzeljagd erlegt wird. Also es hat wirklich keinen Stress. Keinen Transportstress. Und ich denke, ein besseres Fleisch ist fast nicht zu bekommen. (***Irina König***)

Als kleine regionale Produzentin kann Irina König nicht immer alles anbieten. Sie verkauft, was es gerade gibt. Über einen Supermarkt oder einen normalen Wochenmarkt ginge das nicht. Um dort mitmachen zu können, muss man gleichbleibende Waren und große Mengen anbieten können. Deshalb hat sich Irina König entschieden, ihre Produkte über die Marktschwärmer-Plattform zu verkaufen. Von Wachtel-Eiern bis zum Hirschgeweih ist alles dabei, was die Schorfheide zu bieten hat. Durch den direkten Kontakt zu ihren Kundinnen kann sie auch seltene Nutztierrassen verkaufen, die auf ihrem Försterhof bei Görlsdorf leben.

>Also, wir haben deutsche Sperber Hühner. Das ist eine Rasse, die ist jetzt 100 Jahre alt. Ist aber stark gefährdet, steht also auch auf der roten Liste der gefährdeten Nutztier Rassen. / Man schafft es nicht, im Supermarkt deutsche Sperber Eier zu verkaufen. Aber hier hat man einen anderen Kontakt zu den Käufern einfach und kann auch was zum Leben der Hühner erzählen. Auch erklären, warum dieses Ei jetzt ein bisschen teurer ist als das Ei vom Aldi. Also es ist schon eine Riesen Unterstützung. Ich denke schon, dass sie erhalten werden können durch den Markt auch. (***Irina König***)

Als sie vor zwei Jahren angefangen hat, erinnert sich Sandra Kink, konnte sie sich selbst nicht vorstellen, dass sich alles so gut entwickelt. Offensichtlich hat sie den Nerv der Zeit getroffen. Auch viele ihrer Kundinnen wollten weg vom Discounter und warteten nur auf das richtige Angebot.

>Das war eine super große Herausforderung, für mich, vor allem im Winter anzufangen, also im Februar anzufangen, wo einfach regional nicht so viel im Übermaß ist an Angebot. Ich hatte keinen Käse. Ich hatte kein Wild Fleisch und musste dann wirklich auch gucken, was kann ich denn da jetzt zaubern? Ich hatte sehr motivierte Kunden, die einfach für diese Sache gebrannt haben. Die gesagt haben toll, finden wir super. (***Sandra Klink***)

Was ihre Kundinnen so begeistere? Die aussergewöhnliche Qualität und das gute Gefühl, beim Essen auch etwas für die Umwelt und das Tierwohl zu tun, meint Kink. An dieser Stelle darf ein Selbstversuch nicht fehlen: Ich koche mir Steaks vom Dammwild-Kalb mit rosa und lila Kartoffeln. „Mmmhh“. Eine richtige Delikatesse. Das Wildfleisch schmeck unvergleichlich zart und würzig. Die Kartoffeln sind bissfest, schmücken den Teller mit bunten Farben und sind wirklich sehr schmackhaft. Die Marktschwärmer liefern tatsächlich das, was der Name verspricht. Artenschutz, der ganz vorzüglich schmeckt.


