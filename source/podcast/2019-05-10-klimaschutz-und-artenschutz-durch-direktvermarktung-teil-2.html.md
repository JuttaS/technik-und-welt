---
date: 2019-05-10 09:17 CEST
permalink: klimaschutz-und-artenschutz-durch-direktvermarktung-teil-2
blog: podcast
title: Artenschutz durch Direktvermarktung - Die Marktschwärmer Episode 2
short_title: Klimaschutz der besser schmeckt - Teil 2
tags:
  - Klimaschutz
  - Landwirtschaft
  - Regionalwirtschaft
image: wasserbueffel.JPG
image_alt_title: Wasserbüffel zur Landschaftspflege in Berlin
soundcloud_link:
lybsin_link: 9731846
published: true
author: Jutta Schwengsbier
introartikel: Saatgut Konzerne wollen nicht nur Saatgut verkaufen sondern vor allem Pestizide. Wir brauchen aber eine ökologische Landwirtschaft.
---

Gleich hinter dem Bauernhof der Familie Peters in Werneuchen unweit von Berlin sind Wiesen und Ackerflächen zu sehen, - so weit das Auge reicht. Bei einem Hoffest, das die Peters jedes Jahr für ihre Kundinnen veranstalten, gibt’s Grillbratwürstchen und Bier. Und als besondere Attraktion: Eine Rundfahrt mit den Traktoren, die viele Nachbarn mitgebracht haben. Die meisten Traktoren sind eher historisch wertvoll, aus den 50er und 60er Jahren des vergangenen Jahrhunderts. Einer hat sogar noch Dampfantrieb.

Die meisten Nachbarn der Peters haben ihre Traktoren nur noch aus nostalgischen Gründen. Landwirtschaft macht hier um Werneuchen kaum noch wer. Ausser den Peters und einigen Großbauern aus Holland. Die haben nach der Wende 1989 viele ehemalige LPGs aus DDR-Zeiten aufgekauft. In Brandenburg hatte zu der Zeit niemand das Geld, um in Landwirtschaft zu investieren. Oder bekam Kredit von Banken. Kleinbauern wie die Peters haben immer noch Schwierigkeiten, einen oder zwei Hektar Land dazu zu kaufen.

>Also, hier hinter sieht man 100 Hektar. Das geht bis zum Horizont. Davon wurden jetzt ein Drittel verkauft. Wir bieten gar nicht mit. Der ist direkt am Hof dran. Das würden wir mit Kusshand nehmen. Aber der Einstiegspreis ist so hoch für den Hektar. Das können wir gar nicht finanzieren.Wir Bauern kommen nicht mal annähernd in die Nähe so einer Fläche als Familienbetrieb. So etwas zu erwerben hier mittlerweile in Brandenburg. Also selbst wenn man hier so einen oder zwei  Hektar kaufen wollte. Geht das gar nicht. Weil die Lose sind halt 30 Hektar.

Sie gehören zu den UHUs, sagt Hans-Christoph Peters lachend. Das sind Bauern, die in Brandenburg unter 100 Hektar Land besitzen. Seine Familie hat bislang gerade mal 80 Hektar. Auf einen Schlag 30 Hektar dazu zu kaufen, sei für sie undenkbar. Ausserdem seien die Preise pro Hektar inzwischen so hoch, dass Landwirtschaft darauf für sie einfach nicht mehr profitabel wäre. Bei den öffentlichen Ausschreibungen könnten nur noch die ganz Großen mitmachen, meint Peters.

##Landgrabbing auch in Brandenburg

>Das sind hier Betriebe, die haben 8000 Hektar. Die bieten mit. Aber nicht mal die werden es wahrscheinlich bekommen. Am Ende wird es darauf hinauslaufen, dass es irgendwo einen Investor gibt, der diese Flächen kauft. Und dann vielleicht irgend jemand anders wieder verpachtet. Also das ist schon ein bisschen pervers. (***Hans Christoph Peters***)

Statt Bauern wie die Peters zu fördern, versteigert die öffentliche Hand die Flächen meistbietend. Dadurch ist das Problem des Landgrabbing, der Aufkauf von Agrarflächen durch Finanzinvestoren, auch in Brandenburg angekommen. Was kurzfristig Geld in die öffentlichen Kassen bringt, könnte sich langfristig rächen. Experten kritisieren schon lange, dass die Ernährungssicherheit nicht länger Finanzinteressen untergeordnet werden dürfe. Um Hunger zu beseitigen müssen weltweit mehr Nahrungsmittel produziert werden, sagt Kostas Stamoulis. Der Agrarökonom ist Vizedirektor der Welternährungsorganisation FAO in Rom.

>Im Jahr 2050 müssen wir 50 % mehr Nahrungsmittel herstellen als im Jahr 2013./ Zunächst einmal müssen wir dazu die Investitionen in Landwirtschaft erhöhen. Wir brauchen öffentliches Investment in Landwirtschaft. Aber der Großteil der Investitionen kommt aus dem privaten Sektor. Von den Bauern selbst. Also müssen wir uns überlegen, was für Bedingungen wir schaffen müssen, damit Bauern in Landwirtschaft investieren können. (***Kostas Stamoulis***)

Wenn Kleinbauern wie Hans-Christoph Peters keinen Kredit kriegen und sich Agrarflächen auch sonst nicht mehr leisten können, dann wird es schwierig, - mit der geplanten Ausweitung der Nahrungsmittelproduktion. Nicht nur in Ländern der Dritten Welt, sondern auch direkt hier in Brandenburg. Noch immer werden die meisten Farmen weltweit als kleine Familienbetriebe geführt. 

>Es ist nicht so, dass die industrielle Landwirtschaft die Welt übernommen hat. Und dass sie ganz massiv Familienbetriebe von ihrem Land verdrängt hat. Mehr als 90 % aller Farmen sind Familienbetriebe. Und ungefähr 80 % davon sind Farmen mit weniger als 2 Hektar Land./ Wenn sie dann aber genauer hinsehen und fragen, wie viel Land diese 90 % Kleinfarmen kontrollieren, im Vergleich zu den grossen Farmen, dann sehen Sie, dass die Zahlen sich etwas anders darstellen. Weil die großen Farmen, und darunter verstehen wir mehr als 50 Hektar, sie besitzen sehr viel Land. Können wir sie also ignorieren, wenn wir fragen, was die Zukunft der Nahrungsproduktion ist? Was genau machen wir dann? (***Kostas Stamoulis***)

Die FAO versuche zwar Kleinbauern zu fördern, sagt Stamoulis. Schon um Hunger bei  den Ärmsten der Armen zu bekämpfen. Und das sind oft die Kleinbauern selbst, die Nahrungsmittel produzieren. Doch auf die industrielle Landwirtschaft verzichten geht seiner Ansicht nach auch nicht, wenn die Preise nicht unbezahlbar werden sollen.

>Unsere Vorhersagen belegen, dass 85 % der notwendigen Steigerung der Nahrungsmittelproduktion, die wir im Jahr 2050 brauchen, wird durch die Steigerung der Ernteerträge kommen. Wenn wir nachhaltig sein wollen, dann müssen wir mehr pro Tropfen Wasser produzieren und mehr pro landwirtschaftlicher Fläche. Wenn wir das nicht schaffen, dann wird Nahrung so teuer werden, dass wir überall eine humanitäre Krise auslösen. (***Kostas Stamoulis***)

Um die Ernteerträge steigern zu können, sind auf jeden Fall mehr Investitionen in die Landwirtschaft notwendig. Am Ende werden auch ihre Kundinnen mit darüber entscheiden, glaubt Sandra Kink, ob sich in Brandenburg mehr industrielle Landwirtschaft oder mehr bäuerlich wirtschaftende Betriebe durchsetzen können. Denn nur wer ausreichend viel verkauft, kann auch investieren. Das klappe bei ihrem Marktschwärmer Angebot aber schon ganz gut.

>Im Sommer vor allem gab's sehr große Umsätze bei Peters Landwirtschaft und den Gärtnerinnen. Das ist absolut vergleichbar mit dem Wochenmarkt. Es ist nicht so, dass die das hier so als Spaß oder Freude machen. Auch. Das freut mich dann auch. Das ist dann hier wirklich nicht nur darum geht, Geld irgendwie zu machen. Sondern es wirklich auch so ein Treffpunkt ist. Aber es rentiert sich für sehr viele schon sehr gut. (***Kostas Stamoulis***)

Hans-Christoph Peters hat mit den Marktschwärmern erst mal die ersten Startschwierigkeiten überwunden. Nun geht’s für ihn darum, auch noch etwas ansparen zu können für weitere Investitionen in seinen Tierzuchtbetrieb.

>Ich muss auf jeden Fall von allem auch noch mehr haben. Definitiv. Ich brauche mehr Kunden. Die Schwärmereien müssen sich auch noch entwickeln. Aber ich bin ja auch erst seit zwei Jahren am Markt. Ich kann erst mal meine Rechnungen bezahlen. Das in der Landwirtschaft, das ist schon viel wert. Weil normal, wenn einer so anfängt, aus dem Bauch heraus, ohne irgend einen Kredit oder so, funktioniert das gar nicht. (***Hans-Christoph Peters***)

Über die Marktschwärmern kann er seinen Betrieb langfristig aufbauen. Viele seiner Stammkunden aus Berlin sind auch zum Fest auf den Hof der Peters raus gefahren. Hier können sie sich selbst überzeugen: Den Weide-Schweinen und Freiland-Hühnern geht’s sehr gut. Auch Sandra Kink, die Organisatorin des Regionalmarktes in Berlin Panow ist gekommen, um sich den Hof mal selber anzusehen. 

>Jetzt tut es mir einfach auch gut zu sehen, o. k., hier, die Schweine sind draußen, die Kühe sind draußen. Hier gibt es die Eier von dem Hühnermobil. Jetzt sehe ich es auch noch mal von der Seite.  (***Sandra Kink***)

Das Hoffest sei Teil seiner Kundenbindungsstrategie, lacht Hans-Christoph Peters. Es scheint zu funktionieren. Die meisten seiner Kundinnen kommen immer wieder bei Ihm einkaufen. Weil's besser schmeckt. Und weil's auch ein gutes Gefühl ist, glückliche Schweine und Hühner zu sehen, die man später mal selber isst.

##Rosa und lila Kartoffeln als Spaßlandwirtschaft

Henry Kosse ist Nebenerwerbslandwirt. Oder eigentlich eher Spaßlandwirt. Denn von Beruf ist Kosse eigentlich Techniker und repariert Faxgeräte, Kopierer oder Computer. Landwirt ist er eher zufällig geworden. Weil er sich in Sonneberg bei Gransee einen Bauernhof gekauft hat und irgend etwas sinnvolles damit machen wollte.

>Hab dort, über so ein Zwangsversteigerungsobjekt einen Bauernhof praktisch mal ersteigert. Der früher mal LPG war. Also völlig heruntergekommen. Wie es eben so ist und denn da so ein bisschen angefangen auszubauen. Und da war auch so ein bisschen Land bei, so knapp vier Morgen. Vier Morgen sind 1 Hektar. Das sind 10.000 m². Ein Morgen ist früher gewesen, was ein Bauer bis Mittag geschafft hat mit einem Pferdefuhrwerk alleine. Da hat man gesagt, ein Morgen Land. Ich sag immer, ich hab vier Morgen oder vier Tagwerke. (***Henry Kosse***)

Als er sich zunächst überlegt hatte, was er mit seinen vier Morgen Land auf dem gerade ersteigerten Bauernhof anfangen soll, entschied er sich, etwas für den Artenschutz zu tun. Und in seinem Fall hieß das: Kartoffel Sorten anbauen, die sonst fast schon ausgestorben sind. 

>Da gibt's in Norddeutschland, in Nordfriesland einen, bei Barum, da der hat sich auch so ein bisschen der Erhaltung der alten Sorten verschrieben. Es gibt ja Sorten, die fast ausgestorben sind. Weil sich keiner erbarmt hat, die anzubauen. Selbst das Bamberger Hörnchen damals, stand auf der Kippe, weil's keiner mehr angebaut hat, weil’s nicht rentabel war. (***Henry Kosse***)

Henry Kosse verkauft nun auch rosa oder lila Kartoffeln über die online-Plattform Marktschwärmer. Zuvor wollte seine ungewöhnlichen Sorten niemand haben. Doch bei dem Regionalmarktplatz kommt sein Angebot sehr gut an.

>Also ich hab auch diese Hörnchen Kartoffeln in einer violetten Ausführung. Das sind dann diese. Die haben eine violette Schale. Sind aber innen hell. Und dann habe ich noch die Rosa Zapfen. Die rosa Tannenzapfen. Den blauen Schweden zum Beispiel, die blaue Elise, die blaue Anneliese, die rote Emily, die Trüffel Kartoffel, die Liselotte. Das ist eine schrumpelige, eine ziemlich alte, das ist diese hier. Alte Kartoffeln erkennt man auch daran, dass die nicht so glatt und rund sind, wie die neu gezüchteten. Sondern dass die alle diese Vertiefungen haben. Daran erkennt man schon die Form der Urkartoffel. Also die ersten Kartoffeln, die von Mittelamerika nach Europa gebracht wurden, die sahen gar nicht aus, wie eine heutige Kartoffel. Die sahen so schrumpelig, gnubbelig aus. (***Henry Kosse***)

Inzwischen hat sich Kosse vom Techniker zu einem richtigen Kartoffel Experten gemausert und erklärt seinen Kundinnen fachmännisch, die Herkunft und auch die besonderen Inhaltsstoffe seiner vielfarbigen Kartoffeln. Viel Profit macht auch Kosse mit seinem Kartoffelanbau nicht. Zumal er nur rund 2000 Quadratmeter jedes Jahr bestellt. Um seine alten Kartoffelsorten trotzdem richtig professionell legen und ernten zu können, besorgte sich Kosse passende Gerätschaften günstig per internet. 

>Na, zum legen der Kartoffeln habe ich schon eine Maschine. Das wäre mir zu müßig. Ich habe einen Traktor. Der Boden wird gepflügt, mit Maschinenkraft. Ich habe auch eine alte Legemaschine, aus den sechziger Jahren, aus Bayern mal mir geholt, den ganzen Hausrat von Bayern mal geholt, für den Kartoffelanbau. Da hat einer aufgehört und meinte, er bringt es alles zum Schrott und hat es dann aber doch bei eBay reingestellt. Und dann habe ich das gekauft und hab das alles abgeholt. So eine Legemaschine, eine Erntemaschine, da war auch eine Egge bei, ein Pflug, also ein komplettes Paket von so einem alten Bauern. 600 Euro. Das war schon ganz toll. (***Henry Kosse***)

Weil er ganz ohne Pflanzenschutzmittel oder künstlichen Dünger auskommen will, läuft er nun jeden morgen über sein Kartoffelfeld und sammelt Eier oder Larven vom Kartoffelkäfer ab. 

>Der Käfer selbst ist ja so richtig Erdfarben. Der hat so eine gelb, gepunktete Oberfläche. Wenn man den auf der Pflanze sieht, auf dem grünen Blatt und man kommt ihm zu nahe, dann lässt er sich einfach von der Pflanze herunter fallen. Dann liegt er auf dem Boden. Du kuckst hin und siehst ihn nicht. Der ist wie ein Chamäleon sofort abgetarnt. Das ist total spannend. Die Eier, die er legt, die sind aber im Gegensatz zu seiner Tarnfarbe Signalfarben gelb. Also Quitten gelb. Die sieht man sofort. Wenn sie oben auf dem Blatt wären. Aber die sind ja unten. Richtige Nester mit 50 gleichzeitig. Also habe ich mir überlegt: Was machst du? Was wir früher an der Grenze hatten, so um unter die Autos zu gucken. Diese Spiegelsysteme. Dann habe ich mir einen Kosmetik Spiegel geholt und so einen Besenstil ran gemacht. Nach drei Tagen habe ich das Feld durch und dann kann ich vorne wieder anfangen. Und wenn du dann mal sagst, du hast keine Lust. Oder es geht dir nicht so gut und dann machst du mal drei Tage nix, dann kann es dir passieren, dass du hinkommst und dann sind nur noch Strünke. (***Henry Kosse***)

So viel Mühe wie Henry Kosse macht sich normalerweise niemand mehr in der Landwirtschaft. Dabei wäre es dringend notwendig, wie er alte Sorten weiter anzubauen und damit den Rohstoff für Saatgut zu erhalten, urteilt François Meienberg. Der Saatgut-Experte der Schweizer Bürgerrechtsorganisation PublicEye setzt sich für eine agrarökologische Wende ein. 

##“Wir brauchen eine agrarökologische Wende“

>Und hier ist das große Problem dabei, dass die Saatgut Konzerne heute, die wollen nicht nur Saatgut verkaufen, die wollen Pestizide verkaufen. Syngenta, Nummer drei bei Saatgut, macht 75 % seines Umsatzes mit Pestiziden. Das heißt natürlich, ich entwickle kein Saatgut für eine Landwirtschaft, welche möglichst wenig Pestizide braucht, welche möglichst wenig Dünger braucht. Sondern ich produziere Saatgut für eine industrielle Landwirtschaft, welche auch meine Absatzkanäle von meinem Pestizid-Produktionsbetrieb garantiert. Und das ist heute der Fall. Die Sorten, die wir heute von diesen Firmen auf den Markt bekommen, sind ganz spezifisch für die industrielle Landwirtschaft entwickelt worden. Und sind abhängig von diesen Pestiziden. Was also ganz dringend not tut, ist eine unabhängige Züchtung. Kleine, mittlere Züchtungsunternehmen, welche nicht in erster Linie Pestizide verkaufen wollen. Sondern Produkte herstellen für eine ökologische Landwirtschaft der Zukunft. (***François Meienberg***)

Das Recht der Bauern, alle Sorten frei zu verwenden und anzubauen, hatte über Jahrhunderte die Artenvielfalt erhalten und die Bio-Diversität gefördert, urteilt François Meienberg. Erst in den letzten 10 Jahren wurde dieses Bauern- und Züchter-Privileg auf Druck der großen Saatgut Konzerne immer weiter eingeschränkt. Sie wollen Bauern dazu verpflichten, jedes Jahr Lizenzgebühren zu zahlen, wenn sie Saatgut aus der eigenen Ernte wieder aussähen.

>Das ist meiner Meinung nach insbesondere schlimm nicht nur in Europa, sondern auch in den Entwicklungsländern. Dieses Recht wird heute in die ganze Welt quasi durchgedrückt. Zum Beispiel, wenn die europäische Union ein Freihandelsabkommen macht mit einem Staat, sagt sie immer, ihr müsst dann aber auch euer Sortenschutz-Recht ändern, um den Bauern zu verbieten frei nachzubauen. Was wir heute in den Gen-Banken weltweit haben, die Basis unserer Zucht, ist die Vielfalt, die durch Bäuerinnen und Bauern in den letzten 100, 200, 300 Jahren geschaffen wurde. Und mit diesem Sortenschutz Recht, welche die Bauern einschränken, diese Bauernrechte nachzubauen, zerstören wir ein ganzes Innovationssystem, um die agrarische Vielfalt weiter zu entwickeln und zu erhalten. (***François Meienberg***)

Der Verkauf von rosa oder lila Kartoffeln, angefangen als Spaßprojekt eines Freizeitbauern, ist so ein kleiner Beitrag, um auch in Zukunft die notwendige Sortenvielfalt weiter zu erhalten. Für Sandra Kink, die Organisatorin des Regionalmarktes in Berlin-Pankow, sind genau solche Angebote wie die der „Kartoffelkiste“ von Henry Kosse wichtig für den Erfolg. Denn das ist es, was viele Kundinnen und auch sie selbst wollen.

>Nicht mit Pestiziden und auch nicht mit irgendwelchen chemischen Düngern. Bei mir .in meiner Schwärmerei, will ich das nicht haben. (***Sandra Kink***)

Über die Marktschwärmer findet Henry Kosse inzwischen genügend Liebhaber, die seine ungewöhnlichen Kartoffeln zu schätzen wissen. Und auch bereit sind, dafür etwas mehr zu bezahlen.

##Markthallen in Berlin waren mal Teil der regionalen Ernährungssicherheit

In der Markthalle Neun in Berlin Kreuzberg gibt es wie auf einem Wochenmarkt Obst, Gemüse und Fleisch. Ausserdem mehrere Bäcker. Ingwer-Tee. Jede Menge exotische Gewürze. Wein und Käse. Oder Wurst aus Südfrankreich. Und natürlich auch gastronomische Angebote für fast jede Geschmacksrichtung. In einer Ecke der Halle bietet aber auch Aldi als Discounter seine Waren an. Auf den ersten Blick sieht die Markthalle Neun also aus, wie viele andere in Berlin. Doch die Geschäftsführer um Florian Niedermeyer sind mit einem besonderen Konzept angetreten. 

>Wir haben aus der Region Erzeuger, Landwirte, die Direktvermarktung machen. Und wir haben natürlich Lebensmittel Handwerker hier, die teilweise auch direkt in der Halle produzieren. Am Ende ist es ein bunter Markt, der Lebensmittel verkauft und natürlich ist für uns schön, wenn jemand unverarbeitete Lebensmittel verkauft für die Verarbeitung zu Hause. Gleichzeitig ist doch das eine Realität, das viele Leute auch viel weniger oder gar nicht mehr zu Hause kochen. Deswegen ist natürlich auch insgesamt im Lebensmittelhandel Convenienz also der Handel mit fertigen oder halb-fertigen Produkten ein größeres Thema beziehungsweise die Gastronomie. Und das spiegelt sich natürlich auch auf unserem Markt wieder. (***Florian Niedermeyer***)

Die Markthallen wurden in Berlin lange von der Stadt selbst betrieben. Doch das war am Ende meist ein Verlustgeschäft. Deshalb hat die Stadt die Markthalle Neun verkauft. Nicht an die Meistbietenden, wie Florian Niedermeier betont, sondern an die Bewerber mit dem besten Entwicklungskonzept. Zu einem Festpreis. Er und seine Mitgesellschafter hatten sich bei dem Wettbewerb am Ende durchgesetzt. Sie wollen regionalen Erzeugern aus der Region Berlin-Brandenburg eine Verkaufsfläche bieten. Und damit die lange Tradition der regionalen Markthallen in Berlin wiederbeleben.

>Jetzt sind wir im Keller und jetzt also vom Markt geschehen wieder abseits runter in den Keller. Der Keller spielt eine wesentliche Rolle für den Markt, weil natürlich die Stände oben, die brauchen Lager. Lagerflächen. Wir haben hier eben Trockenlager und Kühllager. Damals hat die Stadt die Markthallen gebaut, eben als Stadt die Verantwortung übernommen, dass die Stadternährung besser wird. Deswegen gab es eine zentrale Großmarkthalle auf dem Alex, und 13 Stadtteil Markthallen. Eine davon ist die Markthalle Neun. Die hatten eben diese Ordnungszahlen und die wurde 1891 eröffnet. (***Florian Niedermeyer***)

Florian Niedermeier ist aber nicht nur Geschäftsführer der Markthalle Neun sondern zugleich auch einer der Sprecher des neu gegründeten Ernährungsrates in Berlin. Die Vision dahinter: Die Stadt soll sich wieder mehr für eine Lebensmittelversorgung direkt aus der Region Berlin-Brandenburg engagieren.

##Der Ernährungsrat Berlin – „Wir brauchen Lebensmittel aus der Region“

>Wenn es nach mir geht, sollte natürlich die öffentliche Hand sagen, in öffentlichen Kantinen, oder alles was an öffentlicher Ernährung stattfindet, sollte auf 100 % regional umgestellt werden. Da ist dann natürlich schon die Überschneidung mit der Markthalle, ich arbeite und lebe sozusagen in einem Raum, der von der Stadt gebaut wurde. Als die Stadt Ernährung noch als wesentlichen Inhalt der Politik auch betrachtet hat. Und mittlerweile ist es so, dass Ernährung bei der Senatsverwaltung für Justiz aufgehängt ist. Da gibt's eine Staatssekretärin für Verbraucherschutz. Da ist irgendwie das Thema Ernährung angesiedelt. Es ist zu wenig, meines Erachtens. Ich wünsche mir eigentlich, dass es einen Senator für Ernährung gibt. Also, Senatorin. Aber Fakt ist, dass das ein ganz wichtiges politisches Thema ist. (***Florian Niedermeyer***)

Natürlich weiss auch Florian Niedermeier, dass die Regionalversorgung mit Lebensmitteln ohne die Discounter teuerer wäre. Kritiker seines Regionalkonzeptes argumentieren, viele könnten sich teuere Lebensmittel gar nicht mehr leisten. 

>70 % gehen glaube ich über den Discounter. Und der Rest ist dann eben normaler Supermarkt. Biosupermarkt. Das ist verschwindend gering, die Nische über die wir reden. (***Florian Niedermeyer***)

Um auch Nachbarn aus Berlin Kreuzberg anzusprechen, die nicht so viel Geld haben, hat sich Niedermeier entschlossen, das Angebot von Aldi in seiner Markthalle zu behalten. Auch wenn es seinem Konzept der kleinteiligen Regionalvermarktung eigentlich widerspricht.

>Wenn man sehr genau schaut, dann ist es auch so, dass teilweise in der Saison Gemüse durchaus in der Direktvermarktung beim Bauern günstiger sein kann als bei Aldi. Gleichzeitig ist es aber schon so, dass es in der Regel etwas teurer ist. Es geht natürlich auch immer eine andere Qualität. Es ist auch so, man sieht das dann auch immer schwarz-weiß. Kaufe ich beim Aldi ein? Oder kaufe ich nicht bei Aldi ein? Die Realität ist aber nicht schwarz-weiß. Sondern grau. Dass wir Leute kriegen, die eigentlich nur bei Aldi einkaufen wollen, die sich dann doch mal verlocken lassen, ein Brötchen beim italienischen Bäcker zu kaufen. Oder sehr beliebt auch, ein Kuchen hier bei Annette Zeller, die Konditormeisterin ist auch in vierter Generation, aus Baden-Württemberg. Ein klassischer Türöffner eben in dem Sinne ist, dass sie all die Kunden verführt, weil der Preisunterschied sehr gering ist, im Vergleich zum Genuss und Qualitätsunterschied, den sie bieten kann. (***Florian Niedermeyer***)

Gute regionale Qualität, die besser schmeckt: Dieses Konzept wird sich langfristig wieder durchsetzen. Davon ist Florian Niedermeier fest überzeugt und setzt sich dafür ein, mir seiner Markthalle Neun und auch als  Sprecher des Ernährungsrates Berlin. 

##Marktschwärmer Plattform fördert regionalen Zusammenhalt

Über den Regionalvertrieb von Lebensmitteln in Berlin und Brandenburg sind inzwischen auch wieder direkte Verbindungen zwischen Kundinnen und Bauern entstanden. Der Marktplatz der Marktschwärmer hat sich für viele zu einem regelrechten Treffpunkt entwickelt, über die persönliche Beziehungen entstehen. Bei Bauer Peters helfen seine Kundinnen auch schon mal selber mit. 

>Ich hab einfach auch viele, die von der Marktschwärmerei kommen, das ist auch ein Effekt, der entstanden ist. Dass ich hier einfach auch Helfer habe, die hier unentgeltlich arbeiten. Die Spaß haben draußen zu sein. Es toll finden, mit den Tieren zu arbeiten. Und gerne das Hühner Mobil ausmisten. Das dauert einfach Stunden und dann das Hühnermobil umsetzen, die Schweine umsetzen. Das kostet alles viel Zeit, die ich mir irgendwo abknappsten muss. Wenn dann irgendwie eine helfende Hand da ist, dann ist das natürlich ein Traum. (***Hans Christoph Peters***)

Hans-Christoph Peter muss sparen wo er nur kann. Die Direktvermarktung hilft ihm , seinen Betrieb auch ohne Kredit aufzubauen. Nur richtige Bio Qualität zu produzieren, kann sich der Jungbauer noch nicht leisten. Für so einen kleinen Betrieb wie seinen, rechne sich das einfach nicht.

>Dadurch, dass mein Vater auch nicht Bio ist, würde das sehr kompliziert werden, hier eine biologische Haltung zu machen. Alles hin und her zu rechnen. Ich füttere meinen Schweinen ja zum Beispiel auch Kartoffeln, die nicht nur von mir sind, sondern auch von so einem kleinen Bauern aus der Gegend hier. Habe ich zwei, drei Kleine. Mancher ist zertifiziert, mancher ist nicht zertifiziert. Das würde alles durcheinanderbringen. Ich müsste auch viel mehr dokumentieren. Die Kontrolle kostet eben auch. Tja, über 1000 Euro im Jahr. Mein Low Budget Prinzip ist, dass ich eben auch gucke, dass ich eben dieses Geld lieber in andere Sachen stecke, als in Kontrolle. (***Hans Christoph Peters***)

Bei Bioqualität wäre das Futter für seine Tiere einfach zu teuer. Und die Kosten für die Kontrollen würden ihn ruinieren, meint Peters lachend. 

>Wir laden die Leute ja hier herein. Alle können gucken. Entweder sind die überzeugt oder eben nicht. Jetzt im Vergleich zu einer ökologischen Haltung müsste ich eben viel größer sein. Ich sage mal, die Reise fängt bei Hühnern beispielsweise an bei Dreieinhalbtausend Hühnern. Ich habe jetzt 650. Ungefähr. Damit kann man das eigentlich nur so betreiben, dass man eben kein Zertifikat hat. Wenn ich mir das jetzt noch holen würde, dann bin ich übermorgen pleite. (***Hans Christoph Peters***)

Seine Kunden sind von der Qualität überzeugt. Weil's besser schmeckt. Und weil die Tiere artgerecht gehalten werden. Auch Florian Niedermeier, Sprecher des Ernährungsrates Berlin, findet das Konzept der Marktschwärmer sehr gut. Die Konkurrenz zu seinem eigenen Konzept mit der Markthalle Neun fürchtet er nicht. Im Gegenteil. Alles was helfe, den Anbau regionaler Lebensmittel wieder zu fördern, sei gut.

>Es wäre gut, wenn viel viel mehr Leute darüber nachdenken würden, aja, ich könnte eigentlich auch mal wieder etwas frisches kaufen. Und ich könnte sogar etwas regionales beim Bauern kaufen. Das können nicht genug Leute voranbringen. Es ist zäh. Aber es ist die Arbeit wert. Und natürlich geht es auch nur in kleinen Schritten vorwärts. Aber wenn es vorwärts geht, ist es aber immerhin nicht rückwärts. (***Florian Niedermeier***)
