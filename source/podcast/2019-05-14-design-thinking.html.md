---
date: 2019-05-14 19:38 CEST
permalink: design-thinking
blog: podcast
title: Design Thinking - Innovation durch Kulturwandel
short_title: Design Thinking
tags:
  - Innovationen
  - Design Thinking
image: design-thinking.jpg
image_alt_title: Design Thinking produziert ungewöhnliche Lösungen
soundcloud_link:
lybsin_link: 9779315
published: true
author: Jutta Schwengsbier
introartikel: Design Thinking führt durch Teamarbeit und Kundenorientierung zu neuen Problemlösung. Studenten und Unternehmen lernen um.
---

##Baby-Schlafsack statt Inkubator

Die Babystation im Mulago Krankenhaus in Kampala. Dutzende Neugeborene liegen hier in kleine Bettchen gekuschelt. Gleich hinter dem großen Babyschlafsaal ist die Baby-Intensivstation angeschlossen. Hier sind die Frühchen oder andere kleine Patienten untergebracht, die besonders betreut werden müssen. Einige brauchen zusätzlich Sauerstoff.  Etwa 20 der Winzlinge liegen in großen Inkubatoren in kleine blaue Schlafsäckchen gewickelt.

>Die Inkubatoren sind defekt. Deshalb liegen die Babies darin in einem Wärmebeutel. Die Inkubatoren werden nur noch als Betten benutzt. Die Babybeutel halten vier Stunden lang eine Temperatur von 37 Grad Celsius, damit die Babies immer warm bleiben. (***Rosie Iga***)

20.000 Dollar teuere Inkubatoren nur noch als Betten zu benutzen, ist sicher eine enorme Verschwendung von Ressourcen. Doch es gibt in Uganda keinen einzigen Medizintechniker, der in der Lage wäre, die teueren Geräte zu reparieren. Selbst die Bedienung der High-Tech Inkubatoren war für viele Krankenschwestern zu kompliziert, sagt die Sozialarbeiterin Rosie Iga. Dabei ist das Mulago Krankenhaus das Referenzzentrum für schwierige Geburten in Uganda. Deshalb verwendet die Babystation statt High-Tech nun Low-Tech. Eine günstig aber gut funktionierende Lösung mit kleinen blauen Wärmebeuteln für Frühchen. Aber das, was da auf den ersten Blick aussieht wie einfache Mini-Schlafsäcke, hat es in sich. Auf der Rückseite haben diese Schlafsäcke eingenähte Taschen für Wärmplatten, erläutert Rosie Iga.

>Wenn diese Wärmplatten für 30 Minuten in ein Heizgerät gelegt werden leuchtet anschließend ein grünes Licht auf. Das grüne Licht zeigt: die Wärmplatte ist fertig. Bevor wir das Baby dann in den Beutel legen können, müssen wir die Schlafsäcke mit Alkohol sterilisieren. Dann öffnen wir hinten einen Reissverschluss und stecken das Wärmepack auf der Rückseite hinein. (***Rosie Iga***)

Die durchsichtige, geleeartige Masse der Wärmplatten wird wieder fest und undurchsichtig, wenn die Temperatur sinkt. Ein einfaches Messgerät im Schlafsack wechselt dann von der Farbe grün auf rot. Das zeigt an: Die Wärmelemente sind zu kalt geworden und müssen ausgetauscht werden. Ein ausgeklügeltes System, das aus einem Design Thinking Kurs an der Universität Stanford hervorgegangen ist. George Campbell ist Direktor der Dschool in Standford und hat das Projekt von Anfang an begleitet.

>Einige unserer Studenten haben eine Firma gegründet, die sehr preiswert Baby-Inkubatoren herstellt für Frühchen, die in Ländern wie Nepal geboren wurden. Statt 20.000 $ für einen Inkubator auszugeben, müssen sie nun nur noch 1 % davon ausgeben, um ein Baby, dass es sonst nicht bis zum Krankenhaus geschafft hätte, am Leben zu halten. Das ist wirklich sehr spannend. (***George Campbell***)

##Empathie ist das wichtigste Konzept

Die Studenten im Design Thinking Kurs hatten die Aufgabe, die teueren, komplizierten High-Tech Inkubatoren mit einer einfachen und billigen Lösung zu ersetzen, erläutert Campbell. Das Ziel: Statt sich von Anfang an nur auf technische Fragen zu konzentrieren, werden beim Design Thinking zunächst die Probleme von Menschen betrachtet, die gelöst werden sollen.

>Wir unterrichten eine Methode, die ein Problem nicht erst durchdenkt. Unsere Studentinnen sollen Empathie entwickeln für diejenigen, die am Ende das Produkt benutzen. Sie sollen kollaborativ arbeiten. Und sie sollen Prototypen für die Lösungen entwickeln. Was wir herausgefunden haben, ist sehr interessant. Wenn sie so arbeiten, dann kommen sie viel schneller zu einem Durchbruch. So können wir die Industrie verändern: Indem Studenten anders lernen und arbeiten. (***George Campbell***)

Wenn der Mensch und nicht die Technik der Ausgangspunkt ist, dann kommen ganz andere Lösungen zustande, ist Campbell überzeugt.

>Einer unserer Alumni war eine Führungskraft bei General Electric, der zweitgrößten Firma der Welt. Er kam in einen unserer Workshops. Und obwohl er schon eine Führungskraft war, kam er zu uns als Student, um die Design Thinking Methode kennenzulernen. Er arbeitete im Bereich medizinischer Bildgebung, diesen MRT Maschinen. Das sind sehr teure Geräte, die sehr lange entwickelt werden und entsprechend sehr viel Geld kosten. Mit so einem Projekt wollen sie am Ende nicht scheitern. In unserem Programm hat er gelernt, durch Empathie festzustellen, ob sie wirklich das richtige Bedürfnis befriedigen. Das heißt, sie müssen raus gehen und mit den Leuten reden, die wirklich mit den Geräten umgehen müssen. (***George Campbell***)

Wie bei den teueren Baby-Inkubatoren, die in Afrika niemand reparieren oder auch nur richtig bedienen kann, so stellte auch der GE Manager fest: Hightech ist nicht unbedingt die beste Lösung für medizinische Probleme.

>Er wollte immer ein Krankenhaus besuchen, hatte das zuvor aber noch nie gemacht. Nach unserem Design Thinking workshop dachte er, ich sollte Empathie zeigen. Ich gehe in ein Krankenhaus und sehe mir dort unser MRT-Gerät an. Also geht er in sein lokales Krankenhaus und sieht sein MRT Gerät in der Radiologie. Er fühlt sich sehr stolz. Da steht es. Tata! (***George Campbell***)

Doch seine Begeisterung für die eigene Entwicklungsleistung schwindet bei genauerer Betrachtung schnell. Seine medizinischen Scanner sind zwar technisch hervorragend. Doch im Krankenhaus sieht er zum ersten mal selbst, welche Probleme viele Eltern oder Ärzte damit tatsächlich bei der Untersuchung von kranken Kindern haben.

>Er kommt zufällig dazu, wie ein siebenjähriges Kind untersucht werden soll. Er sieht die Eltern, die im Gang hin und her laufen und sehr gestresst sind. Das kleine Kind hatte Todesangst. Das Mädchen fing an zu weinen und zu schreien, als es das riesige MRT - Gerät zum ersten mal sah. 80 Prozent der Kinder mussten betäubt werden, bevor sie gescannt werden konnten. Weil er das im Krankenhaus selbst gesehen hatte, konnte er zum ersten mal mit Kindern mitfühlen. Ihm wurde klar: Er hatte das falsche Problem gelöst. (***George Campbell***)

Nach seinem Krankenhausbesuch fing der GE Manager deshalb an, den ganzen Prozess zu überdenken und für Kinder anders zu strukturieren. Statt aufs Krankenhaus wurden die Kleinen nun auf einen großen Abenteuerurlaub vorbereitet, wie beim Besuch eines Sommerlagers.

>Sie bekamen Zuhause einen kleinen Rucksack mit einem Comicbuch, dass den Prozess wie eine Abenteuer Reise beschrieb. Im Krankenhaus wurden die Kinder dann von einem Camp-Berater empfangen und nicht mehr von einer Krankenschwester. Wenn die Kinder jetzt in den MRT Raum kommen ist alles bunt bemalt. Die Maschine sieht nun aus wie ein Zelt, in das sich die Kinder hinein legen sollen. Durch diese Veränderungen haben Kinder eine ganz andere Erfahrung mit diesen Geräten gemacht. (***George Campbell***)

Weil die Kinder nun keine Angst mehr vor der Untersuchung haben, hat sich die Qualität der medizinischen Untersuchung deutlich verbessert.

>Die Kinder mussten nicht mehr betäubt werden. Deshalb konnten mehr Kinder in der gleichen Zeit behandelt werden und die Patientenzufriedenheit war sehr viel größer. (***George Campbell***)

Ein Ergebnis, das nicht durch High-Tech sondern durch mitfühlende Begleitung möglich wurde, sagt Campbell. Wenn sich das Denken nicht mehr zuerst an Geschäftsprozessen oder Technik orientiert, sondern am Menschen, rücken ganz andere Probleme in den Mittelpunkt. Und dann werden Lösungen möglich, an die zuvor niemand gedacht hatte. Eines der besten Beispiele für so eine grundlegende Innovation durch Design Thinking sind die teueren High-Tech Brutkästen, die durch kleine Babyschlafsäckchen getauscht werden konnten. Im Mulago Krankenhaus in Uganda haben die neuen, kleinen Babywärmer schon viele Leben gerettet, ist Sozialarbeiterin Rosie Iga überzeugt.

>Ungefähr 30 Prozent unserer Babies brauchen die Babywärmer. Das sind 3 bis 5 Frühchen am Tag. Sie werden aus der Geburtsstation zur Babystation in den Wärmebeuteln gebracht, damit sie dauernd warm sind. Von der Wärme der Mütter in die warmen Schlafsäckchen und dann in die Babystation zur weiteren Versorgung. Dadurch bleiben die Babies durchgehend warm. (***Rose Iga ***)

Von der Geburtsstation zur Babystation muss man im Mulago ungefähr 15 Minuten laufen. Früher sind auf dem kurzen Weg einige der schwächeren Neugeborenen gestorben, erinnert sich Krankenschwester Emmely.

>Ohne diese Babywärmer sind viele Babies an Unterkühlung gestorben. Die Hebammen haben festgestellt, dass die Lebensgeister der Babies nun durch die Wärme dieser Beutel geweckt wird. Wenn sie jetzt aus dem Operationssaal zur Babystation  kommen, dann sind sie ganz rosig. Die Babies sind viel lebendiger. Aber am wichtigsten ist: Sie überleben. (***Emmely***)

Viele der Neugeborenen im Mulago sind zu klein und untergewichtig, auch weil oft die Mütter zu dünn und selbst schlecht ernährt sind. Ohne Fett auf den Rippen können die Kleinen keine Wärme speichern und kühlen viel zu schnell aus.

>Die Babywärmer sind eine enorme Verbesserung. Viele der Mütter haben nicht einmal warme Kleidung für ihre Babies. Wenn wir die Babies in die Schlafsäckchen legen, fühlen sie sich warm und behaglich. Wenn wir ihnen Sauerstoff geben müssen, fühlen sie sich in den Wärmern viel besser. (***Emmely***)

Noch läuft die Testphase, doch schon bald sollen die Babywärmer in ganz Afrika eingesetzt werden. Um solche genialen Veränderungen möglich zu machen, muss niemand ein Genie sein, ist Georg Campbell überzeugt.

##Innovation durch Design Thinking 

>Manchmal denken große oder auch kleine Firmen, Innovationen sind schwer. Aber viele kleine Schritte führen auch zum Ziel. Kleine Schritte, Empathie und verschiedene Prototypen. All diese kleinen Schritte ergeben am Ende eine sehr große Veränderung. (***George Campbell***)

Statt auf den einen Geistesblitz zu warten, sei es besser, unsere bisherige Art zu denken und zu lernen prinzipiell zu verändern. Aus seinen eigenen Erfahrungen hat Campbell deshalb für die Dschool an der Universität Stanford ein ganz neues Lehrkonzept entwickelt: Das Design Thinking.

>Früher war ich selbst Entrepreneur. Ich habe mich oft gefragt, wie schaffe ich eine große Innovation? Manchmal habe ich es geschafft. Bei anderen Firmen bin ich damit gescheitert. Meistens haben wir sehr hart gearbeitet. Oft endete es damit, dass ich selbst und auch mein Team völlig erschöpft waren. Das ist die normale start-up Kultur. In der Dschool haben wir uns nicht auf die Innovation sondern auf die Innovatoren konzentriert. Zu meiner eigenen Überraschung habe ich festgestellt: Wenn wir uns nicht auf die Innovation konzentrieren sondern auf die Innovatoren, dann entstehen Innovationen schneller und haben viel größere Auswirkungen. (***George Campbell***)

##Kreativ sein im Team

Die Frage laute also nicht: Wie entsteht eine wirklich große Idee? Die Frage laute: Wie kann jeder selbst kreativ werden? Wie kann ein Team gefördert werden. Wenn das Team im Mittelpunkt steht, dann kommen gute Ideen wie von selbst. Dann verändere sich die ganze Unternehmenskultur, ist Campbell überzeugt.

>Wenn wir annehmen, dass der Weg einfach ist und wir erfolgreich sein werden, dann haben wir eine Firmenkultur, die Erfolg belohnt und Mißerfolg bestraft. Die eigentliche Frage ist aber: Wo soll das größte Risiko liegen? Wenn du zu lange wartest und das Projekt ganz am Ende scheitert, dann ist das ein sehr großer Mißerfolg. Deshalb ermutigen wir unsere Studenten sehr früh zu scheitern. Wenn sie nach einigen Stunden der Investition scheitern, dann spielt das keine große Rolle. Dadurch lernen sie sehr schnell. Am nächsten Tag können Sie dann einen besseren Schritt machen. Dann scheitern sie vielleicht wieder. Aber sie haben schon wieder etwas gelernt. Und dann, nach einigen Tagen oder Wochen oder Monaten, haben sie eine viel nachhaltigere Lösung. Und am Ende werden sie Erfolg haben. (***George Campbell***)

Große Innovationen waren selten beim ersten Versuch erfolgreich, sagt Campbell. Im Gegenteil: Neuerungen fordern fast immer Mut und Ausdauer.

>Wenn wir die menschliche Geschichte über eine längere Periode betrachten, waren einige der größten Durchbrüche nicht erfolgreich als sie begonnen hatten. Zum Beispiel die Glühbirne. Sie dürfen nicht vergessen wie viele gescheiterte Glühbirnen es gab, bis die Erste funktionierte. Fast 1000 Versuche mit Glühbirnen sind gescheitert bis die Erste funktionierte. Das ist unsere wichtigste Erkenntnis: Erfolg kommt immer erst nach dem Scheitern. (***George Campbell***)

##Schnell scheitern ist das Erfolgsrezept der Zukunft

Zu experimentieren. Schnell und oft zu scheitern, gehörte deshalb von Anfang zur Design Thinking Methode. Was Campbell seinen Studentinnen vermitteln will, ist der Mut, gemeinsam etwas ausprobieren und dabei auch scheitern zu können.

>Die Studenten sollen lernen Prototypen zu entwickeln. Statt Probleme erst zu durchdenken, sollen sie schon zu Beginn etwas ausprobieren. Durch Experimente lernen sie, welcher Weg erfolgreich sein könnte. Dann denken Sie auch anders darüber nach. Dann nennen sie es nicht mehr scheitern sondern fragen sich selbst: Lernen wir schnell genug? (***George Campbell***)

Ausgehend von der D.School in Stanford hat sich die Methode des Design Thinking inzwischen ausgebreitet und wird an mehreren Universitäten weltweit unterrichtet. Seit 2007 bietet ein eigener Fachbereich des Hasso Plattner Instituts in Potsdam Design Thinking für Studentinnen und Studenten auch als Studienfach an. Ebenso workshops für Führungskräfte von Unternehmen. In einer „Innovationen für Jobs“ genannten Veranstaltungsreihe sollten Mitarbeiter verschiedener Unternehmen zum Beispiel gemeinsam überlegen, wie Arbeitsplätze trotz zunehmender Automatisierung gesichert werden können.

##Kreatives Gestalten braucht einen neuen Rahmen

Eine ganze Etage am Hasso Plattner Institut wurde speziell für die Design Thinking Kurse neu gestaltet. In mehreren Arbeitsbereichen zwischen raumteilenden Pinn-Wänden stehen dabei hohe Stehpulte. Die Arbeitsatmosphäre trage entscheidend dazu bei, dass die Teams gut zusammenarbeiten können, erzählt Ulrich Weinberg. Er leitet die HPI School of Design Thinking.

>Das ist ein sehr nutzerorientierter, menschenorientierter Prozess, der sich sehr stark einlässt, empathisch sich einlässt, auf die Bedürfnisse von Menschen. Und den praktizieren wir hier und trainieren ihn hier. Das Wesentliche Momentum, was sie hier erleben können, ist der Raum. Wenn diese Teams in diesem Prozess zusammenarbeiten, das sind im wesentlichen fünf oder sechs Schritte. Dann brauchen Sie auch ein spezielles Ambiente. Die Teams müssen unterstützt werden. (***Ulrich Weinberg***)

Weil Büromöbel bislang vor allem für eine Person gebaut sind, müsste sich das HPI sogar seine eigenen Möbel designen, erinnert sich Weinberg.

>Und wir haben dann Stehmöbel extra hier selber entwickelt. Weil wir sie nicht finden konnten in der Industrie. Wir haben jetzt zweistöckige Tische, an denen man im Stehen arbeiten kann. Speziell designt zur Unterstützung eben dieser Teams von 4-6. (***Ulrich Weinberg***)

Unsere bisheriges Lehrsystem war vor allem darauf ausgerichtet war, sagt Weinberg, Einzelne zu fördern und die Einzelleistungen in Konkurrenz mit anderen zu bewerten. Doch viele Probleme seien allein einfach nicht zu lösen.

##Das Wir entscheidet - nicht der einsame Held

>Es geht nicht mehr um den Heroen, um den Einzelnen, der die tolle Idee hat. Wir sind konfrontiert mit komplexen Problemen. Ich komme viel schneller zu interessanten und spannenden Lösungen, wie auch zu Geschäftsmodellen, wenn ich das in komplex zusammengesetzten Teams versuche. Mich also auf das Team einlasse. Und also die Leistungsfähigkeit von Teams stärker in den Vordergrund rücke als die Leistungsfähigkeit von Einzelnen. (***Ulrich Weinberg***)

Um die Kreativität zu fördern, stellt das HPI den Teams Kisten mit Bastelmaterialien bereit, die auch in einem Kindergarten zu finden sein könnten. Lego Bausteine und Spielfiguren. Filzmatten. Gummifrösche. Luftschlangen. Was wie Spielmaterialien aussieht, soll helfen die Phantasie anzuregen. Daraus sollen die Prototypen zur Problemlösung entstehen.

>Sie noch 10 Minuten, um darüber nachzudenken, wie sie die Ergebnisse präsentieren wollen. Und Start. Fangen Sie jetzt an. 15 Minuten für den Prototypen. Und dann kommen wir zurück. Sie können auch zu den anderen Teams gehen und sich dort inspirieren lassen. Ich wollte nicht sagen, dass sie es stehen sollen. Aber ja. Machen Sie das. Leihen Sie sich Ihre Ideen. Leihen Sie sich Ihr Business Modell.

Nachdem an den Pinwänden im Eiltempo Ideen entwickelt wurden, sollen die anschließend zusammen gebastelten Prototypen dann im großen Plenum vorgestellt werden.

>Haben Sie keine Angst davor, Arbeiten zu zeigen, die noch nicht vollendet sind. Sie haben so viele Dinge diskutiert. Es gibt schon viel zu zeigen. Sie müssen sich nur noch überlegen wie. Sie haben noch 20 Minuten.

Einige Präsentationen der Ergebnisse ähneln noch eher einem Schülertheater. Viele Teams stellen ihre Prototypen mit verteilten Rollen vor. Es sind noch keine perfekten Lösungen. Es geht vor allem darum, möglichst schnell Rückmeldung für eine Ideen zu bekommen.

>Wir wissen, dass dieser erste Wurf nicht 100 % am Ziel ist. Aber wir haben ihn mal gemacht. Sie sagen dann vielleicht, das ist schon die richtige Richtung. Aber es stimmt da und dort noch nicht. Und genau das nehmen die Studenten jetzt auf, um die nächste Version des Prototypen zu machen. (..) Da hat jedes Team basierend auf den Erfahrungen von realen Personen versucht, Lösungen zu entwickeln. (..) Früh und oft scheitern. Das ist in der Ideenfindungsphase. (..) Diese ganze Etage, die sie hier sehen, ist ein Ort des  Scheiterns. Hier ist jeder Fehler erlaubt, der einem nur einfällt. Es sind wilde Ideen erlaubt. Die wildesten, die man sich auch nur ausdenken kann. Und die führen nicht alle zum Ziel. Viele landen im Nirwana. Oder im Jenseits. Aber bei einigen kann man dann tatsächlich nächste Schritte aufbauen. Aus einigen entwickeln sich dann auch unternehmerische Ideen. Produkte. Projekte. Services. (***Uli Weinberg***)

Inzwischen beauftragen auch einige große Unternehmen die Studentinnen und Studenten des HPI damit, Projektideen für neue Unternehmenskonzept zu entwicklen, erzählt Uli Weinberg. 

>Da kam die Metro zu uns mit der Frage, wir haben Real Märkte und die Konkurrenz macht Home Delivery. Wir machen das noch nicht. Wir wollen das auch machen. (***Uli Weinberg***)

##Design Thinking erobert Konzern Zentralen

Auch wenn die Metro von den Ergebnissen des Design Thinking Prozesses zunächst überrascht war. Der Handelskonzern war bereit, das Konzept zumindest mal selbst auszuprobieren. Und die Ideen dann selbst weiter zu entwickeln.

>Wir haben festgestellt, der einfache Prozess - Laden – Zuhause, ist nicht das, was die Leute wollen. Die Klientel, die wir uns angesehen haben, war viel eher interessiert, onlineshopping ja. Sachen online bestellen. Aber dann will ich die irgendwo abholen, wo ich ohnehin im Laufe des Tages vorbeikomme. Ich will nicht extra in den Laden laufen. Aber ich will auch nicht zuhause warten müssen, 2 Stunden, bis irgend einer ankommt mit dem Zeug. Ich will das bestellen und dann soll das irgendwo in einer Litfaßsäule, in einer Paketstation oder was weiß ich wo, sollen die Sachen stehen. Die Sachen sind fertig gepackt. Das Bier ist gekühlt. Das Obst es frisch und so weiter. (..) Metro war erst mal sehr irritiert, dass da in der Mitte noch etwas ist. (..) Dann haben Sie das noch einmal neu skaliert. Dann gab es als erstes 2010 in Hannover, konnte man sich im Netz angucken - Real drive. So heißt das ganze Konzept jetzt. Und Sie können online bestellen. Und ab 2 Stunden später steht das Ganze fertig gepackt in einer großen Halle. Weil die haben gesagt, Litfaßsäule ist zu klein. Wir müssen richtige Hallen anbieten. An großen Kreuzungen. Und das bewährt sich extrem gut. (***Uli Weinberg***)

Die Erfolge der Design Thinking Methode haben inzwischen viele überzeugt, urteilt Uli Weinberg. Immer mehr Unternehmen bauen deshalb eigene Design Thinking Abteilungen als eigene Kreativ-Labore auf. Dort wird in fachlich gemischten Teams an der Zukunft gearbeitet. Als eine Folge werden dann nicht mehr Einzelleistungen mit Bonuszahlungen prämiert sondern es wird die Teamarbeit gefördert. Langfristig wird Design Thinking so sicher neue Diskussionen anstoßen, wie wir in Zukunft zusammen leben und arbeiten wollen.


