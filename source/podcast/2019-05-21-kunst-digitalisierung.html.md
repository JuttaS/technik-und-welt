---
date: 2019-05-21 19:34 CEST
permalink: kunst-digitalisierung
blog: podcast
title: Digitalisierung in der Kunst - Wie die Bilder fliegen lernen
short_title: Kunst und Digitalisierung
tags:
 - Digitalisierung
 - Kunst
image: dinosaurier.jpg
image_alt_title: Ein Dinosaurier steht wie real plötzlich im Raum neben mir. 
soundcloud_link:
lybsin_link: 9869510
published: true
author: Jutta Schwengsbier
introartikel: Immer mehr Künstler nutzen digitale Werkzeuge, um ihre Kunst über VR-Brillen oder als Augmented Reality ganz neu zu präsentieren.
---

Wer Bilder in der Berliner Gemäldegalerie über eine Smartphone App betrachtet, der sieht plötzlich Gemälde aus den Rahmen fallen oder die Figuren treten lebensecht hervor. Carla Streckwall hat diese App entwickelt. Sie ist Teil der Künstlergruppe Refrakt und hat die App im Rahmen ihrer Meisterschülerinnen Arbeit an der Universität der Künste entworfen. Ihr Ziel dabei war, unsere Wahrnehmung von Kunst grundlegend zu verändern. 

>Das Ganze war nie so angelegt, dass wir gedacht haben, wir müssen jetzt den kulturhistorischen Kontext erklären. Wir haben auch von vornherein eigentlich nicht versucht, mit dem Museum in Kontakt zu treten. Sondern haben dann überlegt, wie wäre es eigentlich, wenn man Gemälde hijacked, wie wäre es, wenn man Gemälde unerlaubterweise vielleicht einfach verändert. (***Carla Streckwall***)

Den per Foto gekidnappten Bildern haben die Künstler Carla Streckwall und Alexander Govoni - mit digitalen Werkzeugen - dann ihre eigene Interpretation hinzugefügt. Besucher können nun neben dem Original aus ihrem Smartphone die von Refrakt hinzugefügten Effekte sehen.

>Teilweise haben wir kleine Details verändert, Röcke wehen lassen, aber auch unsere eigenen 3-D Objekte dazu gefügt. Wir haben die Bilder verschwimmen lassen. Also es ist wirklich ganz unterschiedlich von Mal zu Mal. (***Carla Streckwall***)

##Augmented Reality eröffnet neue Sichtweisen in der Kunst

Über die Werkzeuge der Augmented Reality, was übersetzt so viel wie verstärkte Realität heißt, entsteht so eine ganz neue Kunstform. Konkret bedeutet Augmented Reality: Wir können über die Betrachtung per Smartphone oder Tablet, analoge Kunstwerke mit digitalen Objekten interagieren lassen, erläutert Alexander Govoni. Die Frage „Was ist Original? Was ist Fälschung?“ spielt für den Mitgründer der Künstlergruppe Refrakt keine Rolle mehr.

>Wenn man jetzt das Porträt einer Person her nimmt, dann ist es immer ein Abbild. Und entspricht nicht dem Original. Das Original bleibt immer die Person. Also in dem Sinne ist die digitale Kunst oder die digitalisierte Kunst auch nur ein weiterer Schritt dieser Simulation, die dann durch unsere heutige Technik einfach bedingt wird. Genauso wie die Fotografie am Anfang vielleicht nicht als Kunstform gesehen wurde, kann man jetzt auch sagen, dass Digitale wird von vielen noch nicht als Kunst verstanden. Aber ich denke, wir sind auf dem Weg dahin, dass man das auch als Werkzeug in der Kunst wahrnimmt und auch zu schätzen lernt. (***Alexander Govoni***)

Inzwischen konzipiert Refrakt nicht nur neue digitale Interpretationen von Gemälden oder Skulpturen, sondern gestaltet analoge und virtuelle Objekte für eigene Ausstellungen. 

>Wir haben uns im analogen Raum, also in der Galerie selbst, sehr viel an den Optiken und visuellen Tools, die im digitalen vorherrschen und in der Werbung, also sehr glatte Oberflächen, perfekte Surfaces, bedient. Und haben sie als Prints im analogen Raum gezeigt. (***Alexander Govoni***)

Die Drucke, die man in der Ausstellung sieht, zeigen gefaltete Hände oder schön geschwungenen Körperformen. Alles, was uns Menschen glücklich macht. Schönheit, Gesundheit, Reichtum. Doch wer dann etwa die Collage aus Diamanten per Smartphone näher betrachtet, wird ganz bewußt irritiert, sagt Govoni mit einem verschmitzten Lächeln. Plötzlich kündigt Siri, die bekannte Stimme der iPhone Assistentin, eine ganz unerwartete Aktion an.

>Zu mindestens denkt man, dass es Siri ist. Und sagt einem, dass gerade 1000 $ von der Kreditkarte abgebucht wurden. Das haben manche Menschen dann auch im ersten Moment ernst genommen. Und sind ja quasi fast vom Hocker gefallen, wie sie das gesehen haben. (***Alexander Govoni***)

##Digitalisierung ermöglicht individuelle Erlebnisse im Museum 

Neben Künstlergruppen wie Refrakt beginnen auch immer mehr Museen, die von ihnen analog gezeigten Kunstwerke durch digitale Angebote zu ergänzen. Als einer der Geschäftsführer der Frankfurter Buchmesse ist Holger Volland daran beteiligt, solche neuen Anwendungen zu vermarkten.

>Das van Gogh Museum hat eine App entwickelt, für das iPad und das iPhone, die vor allem Kindern und Schulklassen dabei hilft, van Gogh ganz neu zu entdecken. Und in der App kann man dann eben sehen, wenn man die Kamera seines Telefons auf einen dieser Originale van Goghs hält, wie der van Gogh in der damaligen Zeit farblich aussah. Denn so ein Bild,  das vergilbt im Laufe der Zeit. Und mit dieser App kann man eben sehen, wie van Gogh die Farben damals gemeint hat und  sie auch gemalt hat. Und das finde ich ist ein sehr schönes Beispiel, dafür wie Technologie helfen, dass wir Kunst neu erleben können. (***Holger Volland***)

Künftig werden moderne Technologien die Art grundlegend verändern, wie wir Kunst betrachten können. Davon ist Holger Volland überzeugt. Mit der von ihm mitbegründeten Kunstmesse The Arts+ will er das Potential zur neuen „Digitalen Kunst“ weiter fördern. 

>Ich hoffe sehr, dass die Kultur durch die Ideen, durch die vielen Möglichkeiten, die wir momentan haben, sich eher erweitert als einschränkt in der Zukunft. Der Hunger nach Inhalten und die Vielfalt von Inhalten, die wir momentan auf der Welt haben, das lässt nicht nach. (***Holger Volland***)

So wie Jugendliche per Smartphone virtuelle Monster auf den Straßen jagen können, so werden Kunstinteressierte bald überall virtuelle Kunstobjekte sehen können, meint Volland. In Museen aber auch einfach auf den Strassen. Einer, der das möglich macht, ist der Jungunternehmer Ronald Liebermann. Als Student für Informatik und Kulturwissenschaften hatte er lange für eine Agentur in Berliner Museen Besucherbefragungen durchgeführt. Dabei fiel ihm auf, dass viele kunstinteressierte mehr Informationen und eine bessere Orientierung durch die Ausstellungen wollten. Doch ein geeignetes digitales Systeme zur Ergänzung der Ausstellungen habe er nicht gefunden, sagt Liebermann.

>Ich hab auch im Rahmen meines Studiums Ausstellungen organisiert und kuratiert. Unter anderem für das medizinhistorische Museum der Charité. Und wir hatten damals viel mehr Inhalte, als wir in der Ausstellung präsentieren konnten. Da habe ich mich dann auch darüber informiert, was es denn aktuell für Technologie gibt und neue Medien, um halt eben dann Inhalte zu vermitteln. Und hab halt gemerkt, dass es da noch eine Lücke gibt. (***Ronald Liebermann***)

##Museen werden mit digitalen Inhalten ergänzt

Gemeinsam mit drei Informatiker entwickelte Liebermann deshalb selbst ein neues digitales Informationssystem für Museen, dass die analogen Kunstwerke mit Zusatzinformationen ergänzt und dabei neue Technologien einbezieht.

>Das System ist eigentlich ein Plug und Play Informationssystem. Die shoutr Box sieht aus wie ein Rauchmelder, besteht grob gesagt aus einem kleinen Computer, aus einer Festplatte und aus einem WLAN Modul. Wir speichern die ganzen Informationen und Inhalte zu Ausstellungen auf dieser Box. Klar natürlich, ein Audio Guide, auch Bilder, Texte, Video und eben auch ganz interessant, Augmented und Virtual Reality. (***Ronald Liebermann***)

Über die von ihm entwickelten sogenannten shoutr Boxen können Besucher also über ihre eigenen Smartphones auf die Datenbanken der Museen zugreifen und sich Zusatzinformationen zu analogen Kunstwerken anzeigen lassen. Bei modernen Bildern genauso wie bei Klassikern wie Rembrandts Nachtwache.

>Wir haben ein Beispiel mit der Nachtwache, wo man dann einzelne Figuren in dem Gemälde anklicken kann und dann im Handy die Informationen angezeigt bekommt. Oder ein Beispiel, da haben wir ein Selbstportrait von Modigliani und die Röntgenaufnahmen des Selbstportraits, dass unter dem Selbstportrait das Porträt einer Frau war. Und auf dem Handy dann das Röntgenbild des Selbstportraits sehen. Das Handy erlaubt sozusagen den Blick hinter das Gemälde. (***Ronald Liebermann***)

##Bilder werden per Tablet oder Smartphone animiert

In Filmen können Zeichentrickfiguren schon lange mit menschlichen Schauspielern interagieren. Durch die Weiterentwicklung der Technik kann nun jeder über sein eigenes Smartphone Infografiken zu Gemälden anzeigen lassen oder animierte digitale Kunstwerke, erläutert Liebermann.

>Wenn man auf sein Handy schaut, dann sieht es so aus, als ob man die ganz normale Kamerafunktion aktiviert hat. In dieses Kamera Bild muss jetzt die erweiterte Realität eingefügt werden. Die gängigste Methode, die man gerade dafür hat, ist, das man eben einen Marker verwendet. Das kann zum Beispiel ein Plakat sein, oder die Seite einer Zeitschrift, und wenn die Kamera des Smartphones diesen Marker kennt, wird das 3-D Modell in Relationen zu dem Marker auf dem Telefon angezeigt. Genau an der richtigen Stelle. (***Ronald Liebermann***)

Wenn in der Handykamera also eine Skulptur, ein Plakat oder ein Gemälde zu sehen ist, dass Künstler oder ein Museum zuvor als Marker gewählt haben, starten digitale Effekte. Dann sind über das Handy etwa Töne zu hören oder im Monitor sind zusätzlich zum Umgebungsbild Grafiken oder Videos zu sehen. Liebermann hat sein System bewußt so gestaltet, dass die Museen ihre analogen und digitalen Kunstwerke selbst kombinieren können. Ohne auf VR Brillen oder eine externe Agentur für die Ausführung angewiesen zu sein.

>Für die Museen und Aussteller hat's natürlich den ganz großen Vorteil, dass es extrem viel Kosten spart. Man braucht keine Leihgeräte mehr, man braucht viel weniger Personal, um Leihgeräte auszugeben oder eben halt dann wieder einzusammeln. Dazu ist es so, wir haben ein eigenes CMS, ein Content Management Systeme, das heißt, die Museen, mit denen wir zusammenarbeiten, können ihre Inhalte auch komplett selbstständig erstellen. Ein schönes Beispiel ist die Max Liebermann Villa am Wannsee. Die schreiben ihre Texte selber. Die sprechen sie selber ein mit einem Mikrofon, das sie jetzt im Museum haben. Und übersetzen das alles auch, in Deutsch und Englisch. Und über unser CMS bauen die mehr oder weniger ihren Multimedia Guide jetzt komplett selbständig. (***Ronald Liebermann***)

##Künstliche Intelligenz soll individuelle Führungen ermöglichen

Über ihr Smartphone können die Besucher bald nicht nur Zusatzinformationen sehen. Liebermann will sein System so ergänzen, dass eine digitale Museums-Assistentin künftig auch von Besucherinnen gestellte Fragen individuell beantworten kann. 

>Ich möchte sobald wie möglich eine künstliche Intelligenz mit unserem System verknüpfen, dass die Besucher im Museum halt auch Fragen stellen können über unser System. Und dann eine künstliche Intelligenz, das nennt sich dann Chat Bot, dann aus der Museumsdatenbank diese Frage dann eben holt und diese Antwort an den Besuche automatisch liefert. (***Ronald Liebermann***)

Anhand der Frage und Antwort Möglichkeit per digitaler Assistentin soll die künstliche Intelligenz dann auch auf die jeweiligen Interessen der Besucherinnen schließen und  individuell anders gestaltete Führungen vorschlagen können. 

>Also ein Traum ist wirklich, ich habe fünf Besucher, die kommen als Gruppe gleichzeitig ins Museum. Und nach zwei 3 Stunden kommen die raus, und jeder hatte ein komplett anderes Erlebnis. Sie waren im gleichen Museum, aber jeder hat andere Exponate gesehen. Jeder hat andere Informationen auch aufgenommen, weil das System erkennt, für was interessiert sich denn der Besucher. Interessiert er sich denn zum Beispiel mehr für Kunst, mehr für Geschichte, oder mehr für Design. Das erkennt das System und kann dadurch eben auch die Tour für den Besucher individuell und anders gestalten. Je nach Vorlieben der Besuche. (***Ronald Liebermann***)

##Virtuelle Realität macht Kunst weltweit verfügbar

Die Digitalisierung von Kunst verändere also nicht nur unsere Wahrnehmung, sagt Diane Drubai. Die Gründerin der Innovationsplattform „We are Museums“ ist überzeugt: Die neuen Technologien ermöglichen uns bald einen neuen, emotionalen Zugang zu Kunst. Mit individuell anders gestalteten Erlebnissen.

>Wir reden nicht mehr nur über eine Ausstellung. Wir reden nicht mehr nur über die Sammlung. Wir reden darüber, dass unser Museum viel mehr sein kann. Es heißt wirklich, den Wert des Digitalen zu verstehen und es anzuwenden auf den Begriff des Museums. Sie werden in der Lage sein, wirklich in ein Kunstwerk hinein zu tauchen. Zuvor war das nicht möglich. Die digitale Kunst spricht viel mehr unserer Sinne an und ist viel näher an dem dran, was wir sind. Näher an unserem Körper, näher an unserem Verstand. (***Diane Drubai***)

Es wird nicht mehr lange dauert, sagt Diane Drubai, bis wir Kunstwerken wie Lebewesen begegnen können. Schon jetzt können wir in virtuellen Räumen ganz neue Lernerfahrungen machen. Zum Beispiel die Naturkundemuseen in London oder in Berlin nutzen inzwischen auch 360 Grad Videos, um Dinosaurier virtuell wieder zum Leben zu erwecken. Im Naturkundemuseum in Berlin wird aus einem Skelett eines T-Rex über eine 3-D Animation ein lebendigen Dinosaurier. Der T-Rex kann im virtuellen Raum herumlaufen, schnaubt wütend, - bevor er auf die Betrachterin zurennt, -  und sie am Ende auffrisst. Eine Schrecksekunde, die wie Diane Drubai niemand so leicht vergisst.

>Es hilft wirklich dabei die Evolution der Dinosaurier zu verstehen. Sie können so nah ran an diesen Dinosaurier. Es schaut so aus, als würde er mit Ihnen reden. Er ist so schön und so riesengroß. Sie bekommen so eine starke emotionale Verbindung zu diesem Dinosaurier. (***Diane Drubai***)

Zu finden ist der lebensechte Dinosaurier unter dem Titel „Tristan erwacht“ auch auf youtube. Um ihn in einem dreidimensionalen Video wirklich erleben zu können, braucht man eine VR-Brille oder ein sogenanntes Cardboard. Das ist ein zum Rechteck gefalteter Pappkarton mit Sichtgläsern, in den vorne ein Smartphone gelegt werden kann. Wenn man dann das Video auf dem Smartphone über die youtube App aufruft, erscheint unten rechts ein kleines weißes Symbol für den Cardboard Modus. Genauer die 3-D Ansicht. Dabei wird der Smartphone-Bildschirm zweigeteilt und ermöglicht so eine dreidimensionale Darstellung. Anders als beim Fernsehen, wo das Bild nur in einem Rahmen zu sehen ist, ändert sich bei VR-Brillen durch Kopfdrehen oder Bewegungen das Sichtfeld. Wenn etwa der wiedererweckte Dinosaurier im britischen Naturkundemuseum aus dem Bild zu schwimmen scheint, kann man ihm über die VR-Brille durch Kopf drehen einfach hinterher sehen. Ganz so, als würden wir direkt neben dem Dinosaurier im Wasser schwimmen. Für Diane Drubai steht fest: Mit Hilfe von neuen Technologien wird es bald möglich sein alle unsere Sinne anzusprechen.

>Sie können es riechen. Sie können es fühlen. Sie können es anfassen. Und in der Lage zu sein, etwas anzufassen in der Kunst, das ist schon wie eine große Revolution. (***Diane Drubai***)

##Virtuelle Kulturräume machen Kunst begehbar

Eine der ersten Firmen in Europa, die sich mit begehbaren virtuellen Kulturräumen beschäftigte, ist die italienische Multimedia Firma Oniride. Mitchell Broner Squire, von Beruf eigentlich Jurist, wollte gleich von Anfang an bei der Entwicklung der neuen virtuellen Welten dabei sein.

>Im Jahr 2014 war virtuelle Realität nur ein Konzept. Sie konnten sich ein Entwicklungstool auf Kickstarter kaufen für 300 $. Aber alles war damals eine große Pionierarbeit. Unsere Idee war, diese Technologie für Kunst und Kultur zu benutzen. Wir dachten, wäre es nicht schön, wenn wir in einem Gemälde herumlaufen könnten? Wenn wir uns fühlen könnten, als wären wir in dem Dorf „Saints Remis de Provence“ wie Van Gogh es gemalt hat? Oder in der Sternen-Nacht von Vincent van Gogh? (***Mitchell Broner Squire***)

Das wichtigste Problem dabei: Wir kann aus der zweidimensionalen Ansicht eines Gemäldes von Van Gogh ein dreidimensionaler Raum werden? Dazu habe Oniride zunächst hoch auflösende digitale Kopien der Gemälde gemacht, erinnert sich Broner Squire. Und dann in einem weiteren Schritt von seinen Grafikern die notwendige Raumtiefe hinzufügen lassen.

>Natürlich haben wir damit selbst eine neue Kunstform geschaffen, denn sie können nicht ein zweidimensionales Gemälde in einen dreidimensionalen Raum umformen, ohne etwas Neues hinzuzufügen. Aber wir haben uns die Briefe des Künstlers angesehen und uns außerdem mit Wissenschaftlern und mit Kunstkritikern abgesprochen. Um etwas zu schaffen, das einigermaßen kompatibel war mit den generellen Ansichten des Künstlers. Was herauskam, war ein Beispiel für virtuelle Realität, das auch von einem Film gezeigt wurde, mit dem Titel „Eine neue Art zu sehen“. Er wurde in ganz Europa verteilt und mit Hilfe des van Gogh Museum in Amsterdam gezeigt. (***Mitchell Broner Squire***)

Zu Beginn ihrer Arbeit musste Oniride die notwendige Technik noch selbst entwickeln. Denn es gab nirgends Geräte zu kaufen, um einen virtuellen Raum tatsächlich begehbar zu machen.

>Das erste Erlebnis 2014 basierte auf dem Headset, das sie vor ihren Augen hatten. Dann haben wir selbst eine sehr einfache Matte entwickelt, auf die sie treten und damit eine Laufbewegung simulieren konnten. Sie konnten kontrollieren, wohin sie laufen, ohne sich tatsächlich zu bewegen. Zu der Zeit waren die Geräte, die sie nutzen konnten, sehr beschränkt. Als wir dann das Projekt weiter entwickelten, gab es schon Raum umfassende virtuelle Realitäten. Damit können sie zum Beispiel einen Raum von 4 Meter mal 4 Meter definieren, durch den sie dann mit ihren eigenen Beinen laufen. Begrenzt wird das Ganze mit einem virtuellen Zaun. Sie sehen also nicht die tatsächliche Wand, wissen aber wo die virtuelle Grenze ist. (***Mitchell Broner Squire***)

Um dann von einem virtuellen Raum in den nächsten zu kommen, entwickelte Oniride ein System, das an Science Fiction Romane wie das Raumschiff Enterprise erinnert. Besucherinnen können sich jederzeit an einen anderen virtuellen Ort teleportieren.

>Das ist das wirklich fantastische an VR. Sie können hier Dinge tun, die in der Realität nicht möglich sind. In einer Sekunde sind sie von der Erde auf dem Mars. Im Bereich des Van Gogh Erlebnisses können sie am Anfang in dem Gemälde Sternen-Nacht sein und im nächsten Moment an der Kathedrale in dem Dorf „Saint Remis de Provence“.  Das hat den meisten sofort gefallen. Oh, ich kann mich von einem Ort zum nächsten bewegen. Ich kann mit meinem eigenen Beinen herum laufen. Ich kann mir ansehen, wie diese Kirche bemalt worden ist. Das Ganze ist viel näher als jede Erfahrung in einem Museum. Dort gibt es immer Trennschreiben und einen gewissen Abstand, den wir einhalten müssen. Aber die virtuelle Realität erlaubt Ihnen sehr nah an die Kunst heranzugehen. Sie können wirklich erleben, wie der Künstler seine Striche setzte, um bestimmte Effekte zu erzielen. (***Mitchell Broner Squire***)

##Neue digitale Werkzeuge machen Kunst bald mit allen  Sinnen erlebbar

Tatsächlich anfassen konnten Besucher die Gemälde von Van Gogh nicht. Dazu fehlten Oniride bei der Entwicklung geeignete technische Geräte. Doch lange wird es nicht mehr dauern, ist Mitchell Broner Squire überzeugt. Dann werden Künstler neue virtuelle Kulturräume erschaffen, die alle unsere Sinne ansprechen.

>Es ist so, als würden sie einem Kind ein neues Spielzeug geben. Künstler sind immer sehr experimentierfreudig. Wenn Sie Ihnen eine ganz neue Ebene für Ihre Ausdrucksfähigkeit geben, dann ist es immer sehr faszinierend zu sehen was sie sich einfallen lassen. Und für uns ist es immer eine Herausforderung, mit der Softwareentwicklung so schnell voran zu schreiten wie sie sich das wünschen. (***Mitchell Broner Squire***)

##Durch digitale Pinsel entstehen 3-D Kunstwerke 

Unter anderem google ist dabei, die Technologie in dem Sinne weiter zu entwickeln. Bei ihrem Tiltbrush genannten Artists in Residence Programm, erhalten zum Beispiel Zeichner neue digitale Malwerkzeuge, um Fabelwesen direkt als dreidimensionale Figuren in virtuellen Räumen gestalten zu können. Das erklärte Ziel des Projektes: Die Technik gemeinsam mit Künstlern so weiter zu entwickeln, dass neue digitale Kulturräume begehbar werden und wir virtuelle Kunstwerke berühren können. Erste Versuche mit rein virtueller Kunst laufen schon, erzählt Diane Drubai. Das Neue Museum in New York lädt derzeit zu einer virtuelle Ausstellung, die weltweit frei zugänglich ist über eine APP mit dem Titel: „First Look - Artists VR.“

>Es ist im wesentlichen eine Ausstellung die kuratiert und organisiert vom Neuen Museum nur über ein Cardboard zugänglich ist. Sie schauen in ihr Handy, stecken es in ein Cardboard und können sofort den Besuch beginnen. Egal wo sie sind. Zuhause, in ihrem Wohnzimmer oder auf der Strasse. Sie können sich virtuell von einem Ausstellungsraum in den nächsten bewegen. Und natürlich können sie die von Künstlern entworfenen virtuellen Realitäten ausprobieren. (***Diane Drubai***)

In einem der virtuellen Ausstellungsräume des Neuen Museums irren die Betrachterinnen mit den Blicken hin und her durch ein Labyrinth. Egal wie man den Kopf dreht. Links. Rechts. Nach oben oder nach unten. Es scheint, als ob die Wände vorbei fliegen. Karos, Linien, Zikzak-Muster wechseln einander in schnellem Tempo ab. Doch ein Ausgang kommt nicht in Sicht, egal wohin man sich wendet. In einer anderen Konzeption sieht man zunächst nur einen weiten Sternenhimmel. Man dreht und wendet den Kopf. Aber zunächst passiert nichts. Erst wenn man einen der vielen hell leuchtenden Sterne über einen virtuellen Lichtstrahl länger fokussiert werden umliegenden Sterne mit Linien zu Figuren verbunden. Neben den Sternbildern erscheinen Infografiken: Es sind Details zu Opfern von Polizeigewalt in den USA. 

Über solche virtuellen Zugänge ist digitale Kunst schon jetzt unabhängig von Raum und Zeit überall verfügbar. Und der nächste Schritt in der Entwicklung ist auch schon absehbar. Bald werden sich die virtuelle und die reale Welt untrennbar miteinander verbinden. Die Zukunft hat bereits begonnen, sagt Mitchell Broner Squire von Oniride.

>Eine der Technologien, die wir entwickeln, heißt gemischte Realität. Und gemischte Realität bedeutet, dass man digitale Elemente, eine digitale Ebene, über die reale Ansicht projiziert. Um das zu erreichen, braucht man ein besonderes Headset. Also eine virtuelle Brille, die sie vor ihre Augen setzen. Mit Gläsern, durch die sie ihre Umgebung sehen und frei herumlaufen können, ohne Angst haben zu müssen, gegen etwas zu stoßen. Aber darüber sehen Sie eine digitale Ebene oder 3-D Modelle oder andere Bilder. Also können Sie in ihrem Sichtfeld etwas sehen, was eigentlich nicht da sein sollte. Wie etwa einen kleinen Elefanten. Oder eine riesengroße Ameise. Oder ein Auto, das gar nicht da ist. (***Mitchell Broner Squire***)

##Auch digitale Kunst braucht Grenzen 

Es ist nur eine Frage der Zeit, bis wir solche digitalen Kunstwerke im öffentlichen Raum auch über Kontaktlinsen oder unsere normalen Brillen sehen  – und mit allen anderen Sinnen erleben können. Die Entwicklung könnte so weit gehen, dass es uns irgendwann vielleicht sogar schwer fallen wird, zwischen Realität und Projektion zu unterscheiden, sagt Holger Volland, Geschäftsführer der Messe für digitale Kunst – ArtsPlus. Mit allen damit verbundenen Chancen und Risiken.

>Kultur kann ein Anker für eine moralische Orientierung sein. Kultur kann aber, und wurde auch schon immer, zur Manipulation eingesetzt. Das heißt, Kultur per se ist ja nicht neutral. Sie können mit einem Bild oder mit einer Fotografie können Sie auch Emotionen wecken. Und die Emotionen, die sie damit wecken, können positiv oder negativ sein. Das hat sich im Laufe der Jahrhunderte glaube ich nicht verändert. Was sich verändert hat, ist die Qualität, die zum Beispiel Fälschungen annehmen können. Ob das jetzt ein Bild ist, ein Text oder ein Video. Wir müssen lernen, das zu hinterfragen. (***Holger Volland***)

Damit nach Fake News nicht auch Fake Art unsere Sinne vernebelt. Und uns eine Realität vorgegaukelt wird, die es so gar nicht gibt. Bislang wurde die Entwicklung im Bereich digitaler Kunst vor allem von technologischen Trends bestimmt. Aber nicht alles, was technisch machbar wäre, ist auch gesellschaftlich akzeptabel. Das hat etwa der heftige Widerstand gegen die von google entwickelte Brille gezeigt, mit der jeder seine Umwelt beliebig abfilmen und live im internet zeigen konnte. Ob totale Überwachung oder manipulierte Realität: Experten wie Volland sind sich bewußt, dass wir dringend darüber reden müssen, wie wir unseren Leben im Spagat zwischen virtueller und realer Welt in Zukunft gemeinsam gestalten wollen.
