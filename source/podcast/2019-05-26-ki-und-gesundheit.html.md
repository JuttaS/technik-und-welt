---
date: 2019-05-26 13:45 CEST
permalink: ki-und-gesundheit
blog: podcast
title: KI und Gesundheit
short_title: KI und Gesundheit
tags:
 - KI
 - Gesundheit
 - Künstliche Intelligenz
image: ki-heatmap.jpg
image_alt_title: KI Hitzebilder im Vergleich zum Original bei Krebsdiagnpse
soundcloud_link:
lybsin_link: 9925001
published: true
author: Jutta Schwengsbier
introartikel: KI in der Medizin kann keinen Arzt ersetzen. Doch künstliche Intelligenz kann im Krankenhaus die medizinische Diagnose unterstützen.
---

Das Institut für Pathologie im Berliner Charité Krankenhaus trägt immer noch den Namen seines ersten Direktors: Rudolf Virchow Haus. Im Eingangsbereich ist in großen Glasvitrinen die Geschichte des Instituts und seines Fachbereichs zu lesen. Eine Seuche sorgte Anfang des 19. Jahrhunderts für ein Umdenken in der Medizin. Eine Choleraepidemie an den Grenzen Preussens, die rasch auch in Berlin um sich griff, hatte innerhalb kürzester Zeit über Tausend Tote gefordert. Darunter auch so bekannte Persönlichkeiten wie den Philosophen Georg Wilhelm Friedrich Hegel. Die Ärzte mussten möglichst schnell Therapien entwickeln. Und Vorbeugemaßnahmen. Das bis dahin übliche Verfahren, die Obduktion von Leichen, war nicht geeignet, die Ursachen der sich schnell ausbreitenden Cholera zu ergründen. In der Folge etablierten die Ärzte einen neuen Fachbereich: Die Lehre von der Entstehung von Krankheiten, die Pathologie. Bis heute ist es die  Aufgabe von Pathologen, Krankheiten anhand von organisch-anatomischen Veränderungen zu analysieren. 

##Pathologie früher und heute

Einige der Arbeiten im Eingangslabor der Pathologie am Berliner Charité Krankenhaus laufen fast noch genau so ab wie zu Zeiten Rudolf Virchows. An einem Seziertisch sitzt ein junger Assistenzarzt und untersucht einen großen Enddarm. Aus der ca 2 bis 3 Kilo schweren Fleischmasse schneidet er kleine  Gewebeproben heraus. Es ist das Ausgangsmaterial, um später eine Diagnose stellen zu können, erläutert Oberarzt Frederick Klauschen. Der erfahrene Pathologe führt mich durch das Labor und erklärt die einzelnen Schritte, die nötig sind, um eine Gewebeprobe analysieren zu können. 

>Vielleicht kann Herr Kajal das kurz mal zeigen und einige Worte dazu sagen, was sie machen? (***Frederick Klauschen***)

>Das bekommen wir hier die Gewebe von die Entwässerunggeräte. Und wir das ausbetten. Das bedeutet in Wachs giessen hier und wenn sie gekühlt kann man ausklopfen hier. (Klack) Und sortiere ich nach Objektträger. Nach Patienten. Und die Kollegin schneiden das für Objektträger. (***Herr Kajal***)

>Das Gewebe ist ja so viel zu dick. Das kann man nicht mikroskopisch untersuchen. Aber man muss jetzt sehr sehr dünne, so ungefähr drei Mikrometer dicke Schnitte erzeugen. Und dafür wird es in dieses Parafin eingebettet, weil es dann einen Block ergibt, den man so schneiden kann. Frau Kajal kann vielleicht kurz ein paar Worte dazu sagen, was sie machen.  (***Frederick Klauschen***)

>Wir machen die Vorarbeit zum färben. Damit der Pathologe das dann nachher beurteilen kann. Wir schneiden hierbei einen Mikrometer. Und pro  Schnitt oder pro Objektträger wird so ein Schnitt aufgezogen. (***Frau Kajal***)


##Medizinische Diagnostik basiert auf Gewebeproben

Im Jahr 2019 wird die Gewebeprobe also immer noch zuerst in Wachs eingelegt, dann getrocknet, hauchdünn geschnitten und schließlich auf ein Glasblättchen gelegt und mit einer Patientennummer beschriftet. Am  Ende der Bearbeitungskette kommen die Glasblättchen dann in große Maschinen mit verschiedenen Flüssigkeiten. Hier wird das Gewebe eingefärbt. Bis die Proben in unterschiedlichen Farbtönen strukturiert sind, von dunklem Lila bis zu hellem Rosa. Je nachdem ob der Zellkern oder umliegende Strukturen markiert werden, hat das Gewebe schließlich sichtbar andere Farben. 

>Es ist so, dass in erster Linie die unterschiedlichen Komponenten des Gewebes unterschiedliche Säure-Basen enthalten. Also die DNA ist ja eine Säure. Das heißt, der Farbstoff, der die DNA anfärbt, färbt eben besonders bevorzugt saure Komponenten des Gewebes an. Und so ist es mit dem EOSIN entsprechend weniger saure oder basische Anteilesodass unterschiedliche Strukturen dann spezifisch angefärbt werden. Und dass ist auch eine Färbung, die schon sehr lange in der Pathologie, meines Erachtens auch schon seit Rudolf Virchow Zeiten sicher, im Wesentlichen so Bestand hat. (***Frederick Klauschen***)

Bis hierhin funktioniert also alles fast noch genau so wie vor 150 Jahren. Nur dass heute deutlich mehr Proben analysiert werden müssen als früher. Heute werden allein in der Charité pro Jahr Zehntausende Glasblättchen mit den angefärbten Gewebeproben untersucht. Weil die Ärzte das Pensum kaum noch alleine schaffen, werden intelligente Verfahren entwickelt, die ihnen bei der Analyse helfen. Und hier beginnt unsere eigentliche Geschichte. Es ist eine Geschichte über die sich rasant wandelnden Möglichkeiten, Krankheiten mit Hilfe künstlicher Intelligenz zu analysieren.

##Medizinische Diagnostik durch Künstliche Intelligenz 

In seinem Büro angekommen, erläutert Oberarzt Frederick Klauschen, woran genau ein menschlicher Pathologe, sprich Krankheitserforscher erkennt, ob ein Patient Krebs hat oder nicht. Dazu schaut er zunächst in sein Mikroskop und untersucht die Gewebestruktur.

>Anhand der morphologischen Merkmale, wie sehen die einzelnen Zellen aus, sind die vielgestaltig, sehen die sehr uniform aus und auch an der Architektur, nicht der einzelnen Zellen sondern der Konfiguration: Wie sind die Zellen angeordnet? Wir würden das als Gewebe-Architektur bezeichnen. Diese beiden Komponenten, das sind sozusagen die wesentlichen Aspekte, die uns dann dazu führen, dass wir sagen, handelt es sich um eine gutartige Veränderung oder einen bösartigen Tumor. (***Frederick Klauschen***)

Gesundes Schilddrüsen-Gewebe sieht über das Mikroskop betrachtet zum Beispiel aus wie eine harmonische Landschaft, in der gleichmäßig verteilt große Seen liegen. Ein Schilddrüsentumor gleicht dagegen eher einem unregelmäßigen Krater. Ohne Flüssigkeit. Sein Mikroskop ist über eine Schnittstelle auch mit seinem Computer verbunden. Frederik Klauschen deutet auf den Monitor und erläutert die Mikroskopansicht. 

>Wenn wir uns jetzt den eigentlichen Tumor ansehen, dann fällt sofort auf, dass die Struktur irregulärer ist. Wir sehen nicht mehr diese gleichmäßig geformten Anteile sondern wir sehen hier so erstens sehr irregulär konfigurierte Anteile teilweise auch die Ausbildung solcher papillären Strukturen. (***Frederick Klauschen***)

##Harmonie im Bild bedeutet Gesundheit

Ist die Harmonie des Bildes gestört, deutet das auf eine krankhafte Veränderung hin. Das, was der Pathologe mit dem menschlichen Auge erkennt, können inzwischen auch Computermodelle analysieren.  Allerdings  können sie viel schneller - und präziser – Gewebeveränderungen finden und zählen. Um zu demonstrieren, wie das funktioniert, schickt Klauschen ein digitales Gewebeabbild auf einen externen Server. Vor der Analyse werden zunächst die Daten anonymisiert, um die Privatsphäre der Patienten zu schützen, erläutert Klauschen. Trotzdem erscheinen nur 2 Sekunden später zwei Bilder ohne die Patientennummer des Originals nebeneinander auf seinem Monitor. Neben der Kopie des Originals ist nun ein Hitzebild zu sehen. Eine sogenannte Heatmap. Der Name ist daraus abgeleitet, dass einige Regionen im Bild rot markiert sind. Damit wird sichtbar gemacht, welche Bildanteile die künstlichen Intelligenz als Tumorzellen erkannt hat.

>Wo die künstliche Intelligenz helfen kann ist bei der quantitativen Auswertung von gewissen Merkmalen. Wie zum Beispiel wie viel Zellteilung findet in einem Tumor statt. Oder wie groß ist der Tumor genau. Verschiedene quantitative Merkmale, die so schwer für den Menschen schnell zu erfassen sind. Natürlich können wir die auszählen. Können messen. Aber das ist sehr sehr zeitaufwendig und teilweise auch ein bisschen ungenau. Und zum Beispiel eine Frage, wie viele Zellen des Immunsystems sind in so einem Tumor zu finden. Das sind genau die Sachen, wo die künstliche Intelligenz, wo der Computer dem Pathologen Arbeit abnehmen kann. (***Frederick Klauschen***)

##KI kann auch zu Fehlanalysen führen

In fast allen Lebensbereichen entscheiden Computer inzwischen mit. Bis vor einigen Jahren war es aber nicht möglich zu überprüfen, wie genau die künstliche Intelligenz aus Beispieldaten lernt. Wenn Computer etwa falsche Kriterien heranziehen, die also eigentlich nichts mit dem gewünschten Ergebnis zu tun haben, ist auch die Entscheidung falsch und manchmal sogar diskriminierend. Wer die falsche Hautfarbe oder das falsche Geschlecht hat, oder wer einfach nur im falschen Stadtviertel wohnt, bekommt dann eventuell keinen Job oder keinen Kredit mehr. Einfach weil Computer menschliche Vorurteile aus Daten mitlernen und generalisieren. Das ist an sich schon problematisch. In der Medizin hängen von Entscheidungen aber Leben oder Tod von Menschen ab, sagt Klauschen. Die medizinische Diagnose per künstlicher Intelligenz müsse deshalb immer vom Arzt überprüft werden können. 

>Es gibt einige Tumoren, die haben eine sehr große Ähnlichkeit zu Immunzellen. Da ist es auch für den Pathologen teilweise sehr schwierig zu entscheiden. Handelt sich um einen bösartigen Tumor? Oder handelt es sich um Zellen des Immunsystems? Da möchte man dann gerne auf Plausibilität besonders prüfen, dass der Computer hier wirklich nicht fälschlicherweise Immunzellen als Krebs erkannt hat. (***Frederick Klauschen***)

##Fehler korrigieren geht schneller als ohne KI zu arbeiten

Dazu kann Klauschen einfach vergleichen, welche Bildanteile in der Übersichtskarte markiert sind. Sind Immunzellen auch rot, ist die Computeranalyse falsch.  Derzeit liege die Trefferquote der künstlichen Intelligenz bei der Analyse bei etwa  90 Prozent. Fehler zu korrigieren, gehe aber bedeutend schneller, als Gewebeproben ganz ohne die Hilfe von künstlicher Intelligenz zu untersuchen, sagt Klauschen.

>Es wird vielleicht in Zukunft auch Verfahren geben, die die klinischen Daten und die  Komponenten so integrieren, dass sie tatsächlich auch eine ärztliche Entscheidung dann maßgeblich mit unterstützen können. Das wird es in Zukunft womöglich auch geben oder vielleicht sogar wahrscheinlich. Aber davon sind wir noch relativ weit entfernt. (***Frederick Klauschen***)

Um den nächsten Schritt der medizinischen Diagnostik zu erklären, führt mich Klauschen in ein Gemeinschaftsbüro. Dort arbeiten mehrere Helfer daran, in digitalen  Gewebeabbildern die wichtigen Elemente einzeln zu markieren.

>Ich bin Isabel Waltermann. Ich bin hier als Praktikantin. Und hier sieht man einen Tumor und zwar ein Brustkrebs. Und ich kuck dann hier was die Tumorzellen sind und label die, indem man drauf klickt. Und durch dieses drauf klicken lernt sozusagen die Maschine immer ein bisschen mehr. Wie diese Zellen aussehen und woran man die jetzt erkennt. (***Isabel Waltermann***)

Wo liegt der Zellkern? Was sind Immunzellen? Wie genau sieht dagegen eine Tumorzelle aus. Am Ende ist jedes Bild mit Hunderten von Kreuzchen markiert, die die verschiedenen Gewebeelemente je nach ihrer Zuordnung beschreiben. Anhand solcher Beispieldaten sollen anschließend Computermodelle automatisch Ähnlichkeiten in Bildern erkennen und Übersichtskarten  daraus machen, erläutert Klauschen.

>Das ist wirklich sehr viel manuelle Arbeit. Denn das, was der Computer hinterher automatisch und mit einer hohen Geschwindigkeit und hohen Präzision durchführt, das muss erst einmal mühsam trainiert werden. Denn natürlich wollen wir, dass der Computer dem Facharzt für Pathologie quasi assistiert. Nur wenn die Qualität entsprechend hoch ist, kann auch hinterher das Ergebnis natürlich dann stimmen. (***Frederick Klauschen***)

##KI muss von ExpertInnen trainiert werden

Die Experten der Charité haben in monatelanger Kleinarbeit Bilddaten mit Millionen von medizinischen  Anmerkungen zu Zellstrukturen versehen. Diese riesige Datenbank dient nun als Grundlage für die Computeranalyse. Die künstliche Intelligenz, die die digitalisierten Gewebeschnitte nach den vorgegeben Kriterien markiert, wird aber nicht in der Charité selbst konzipiert. Die wichtigsten Köpfe hinter der Programmierung dieser Algorithmen arbeiten an der Technischen Universität Berlin.

##TU Berlin entwickelt Algorithmen für medizinische Diagnostik

Man sieht dem eher unscheinbaren Universitätsgebäude im Zentrum Berlins nicht an, dass es ein Hotspot für künstliche Intelligenz in Deutschland ist. Professor Klaus Robert Müller, Lehrstuhlleiter für maschinelles Lernen,  gilt als einer der führenden deutschen Experten für künstliche Intelligenz im Bereich medizinischer Anwendungen. Das, was heute als künstliche Intelligenz bezeichnet wird, erläutert Müller,  hieß früher einfach maschinelles Lernen. Ein Prozess, der so ähnlich funktioniere wie der Lernprozess bei Kindern.

>Zeigt man einem Kind, dass hier heißt Stuhl und noch einen anderen Stuhl. Fragt das Kind ist es auch ein Stuhl. Nein. Ein Tisch. Das ist ein Lernen aus Beispielen. Kinder sind in der Lage ihr ganzes Weltwissen so einzubringen, dass sie besonders effizient aus sehr wenigen Daten lernen. Das maschinelle Lernen ist in der Regel nicht in der Lage aus ganz wenigen Daten dazu zu lernen. Sondern man braucht eigentlich immer mehr größere Datenmengen um komplexe Zusammenhänge zu verstehen oder die Maschine verstehen zu lassen, in dem Falle der Krebsdaten. (***Klaus Robert Müller***)

##Computer finden Ähnlichkeiten und Unterschiede

Die Lernfähigkeit von Computern beruhe im Wesentlichen darauf Ähnlichkeiten oder Unterschiede in Bildern oder anderen Daten erkennen zu können. Beim Projekt mit der Charité ging es zunächst nur darum, Veränderungen an den Organen im Vergleich zu den digitalen Abbildern der Gewebestruktur automatisch zu erkennen und zu markieren.

>Wenn wir unsere Lernmaschine trainieren, so dass sie gute Vorhersagen treffen kann, also Krebs nicht Krebs, bestimmte diagnostische Eigenschaften, dann können wir die Frage stellen: Wie hat die Lernmaschine das gemacht. Und wir können das erklären. Das ist sehr sehr wichtig, weil es könnte ja sein, dass die Maschine gelernt hat, ein spezielles Artefakt in den Messungen zu nutzen. (***Klaus Robert Müller***)

##KI muss lernen, relevante Unterschiede zu entdecken

Als Artefakte bezeichnen Wissenschaftler Merkmale in Bildern oder Daten, die immer wieder vorkommen, aber eigentlich nichts mit den gewünschten Lernmerkmalen zu tun haben. Um zu demonstrieren, was genau damit gemeint ist, zeigt mir Müller einige Beispieldatensätze für maschinelles Lernen. Damit wurden verschiedene Algorithmen darauf trainiert, Bilder zu erkennen, die ein Pferd mit Reiter darstellen.  Im Ergebnis lag die Trefferquote – dies ist ein Bild mit Pferd und Reiter - meist zwischen 80 und 90 Prozent. Doch die Systeme hatten auf ganz unterschiedliche Weise gelernt, worum es geht.  
>Von beiden wird das Bild klassifiziert und man sieht hier das neuronaler Netze guckt auf die Nase des Pferdes auf den Popo des Pferdes auf den Reiter und sonst aber nicht. Dieses Modell das auch sehr gut generalisiert und gute Ergebnisse liefert das guckt zwar ein bisschen auf den Popo guckt aber überall hin. (***Klaus Robert Müller***)

Während ein Algorithmus sich also an den Umrissen von Pferd und Reiter orientiert , hat ein anderer links unten in einer Ecke der Bilder eine Beschriftung entdeckt.  Darauf hatte jemand notiert:  Pferd mit Reiter. Diese Lernmaschine hatte also auch völlig richtig Bilder mit Pferd und Reiter identifiziert. Aber nicht, wie gewünscht, an den Umrissen, sondern an einer von Menschen gemachten Anmerkung. Normalerweise analysieren Algorithmen Millionen von Datensätzen, erläutert Müller. Bis vor kurzem konnte niemand nachvollziehen, was genau die Computer lernen und wie sie zu ihren Ergebnissen kommen.

##Menschen müssen Ergebnisse von Computern prüfen können

>Und das Interessante ist, dass wir mit entsprechenden mathematischen Verfahren schaffen können, diese Blackbox unserer Maschinen zu öffnen und sozusagen aus der Maschine rauszukriegen, was hat sie eigentlich gelernt und daraus Schlüsse zu ziehen, die zum Beispiel hilfreich sind um bestimmte Mechanismen zu verstehen. (***Klaus Robert Müller***)

Dazu hat Müller so genannte Hitzebilder entwickelt, die optisch verdeutlichen, welche Merkmale die Algorithmen für ihren Lernprozess  verwenden und wie sie zu ihren Ergebnissen kommen. Dabei werden Bildregionen, die für das maschinelle Lernen benutzt werden, rot eingefärbt. So können Menschen, wie beim Pferdebeispiel, auf den ersten Blick erkennen, welche Merkmale als Entscheidungsgrundlage der Lernmaschinen dienten.  Und die Ergebnisse der Computer korrigieren, wenn diese falsche Kriterien genutzt haben.

>Das ist seit drei, vier Jahren, ist das Stand der Technik. Die meisten machen das nicht. Aber wenn ich Nutzerverhalten im Internet vorhersage und irgendwelche Artefakte dabei nutze und es trotzdem gut vorhersagen kann, dann stirbt davon niemand. Wenn ich das aber in der Pathologie mit maschinellem Lernen einsetze dann muss ich wissen, ob das stimmt oder nicht stimmt. Wir können nicht unreflektiert eine Technologie einsetzen und nicht verstehen, was da passiert. (***Klaus Robert Müller***)

Denn hier geht es im Zweifel um Leben oder Tod. Ärzte durch Computer zu ersetzen, sei überhaupt noch nicht vorstellbar. Eher im Gegenteil, sagt Müller. Es gebe immer mehr Daten, die analysiert werden müssen und immer wenigen Spezialisten, die das können.

>Diese ganzen Techniken sind meiner Meinung nach nur Unterstützung für den Arzt. Der Pathologe ist die beste Lernmaschine. Ein  alter Pathologe hat ganz viel gesehen und der erkennt, der findet auch alles. Es geht ja nur darum, dass man Fehler vermeidet. Der Patient interessiert sich überhaupt nicht darfür, ob jetzt sozusagen der Pathologe in 98 Prozent richtig liegt oder in 100 Prozent richtig liegt, wenn er zufällig an diesem Morgen das vergeigt. Dasselbe gilt für die Maschine. Man muss einfach 100 Prozent sicher sein in der Diagnose. Es geht nicht in der Diagnose darum, irgendwie möglichst effizient irgendwas zu machen. Sondern es geht darum, richtig zu liegen. Mit allen Mitteln. (***Klaus Robert Müller***)

##KI als Diagnose Assistenzsystem

Die von Müller mit entwickelte künstliche Intelligenz soll am Ende ein zuverlässiges Diagnose-Assistenz-System für menschliche Experten werden. Dabei werden die Computer nicht nur darauf trainiert, Krankheitssymptome in Gewebebildern zu erkennen. Ein anderes Projekt nutzt maschinelles Lernen zur Genanalyse. Es ist zwar noch längst nicht alles entschlüsselt. Doch von einigen Genveränderungen weiß man bereits, mit welchen Krankheiten sie verbunden sind. Und bei anderen vermutet man zumindest, dass sie für weitere klinische Studien interessant sein könnten.

>Für ungefähr zehn bis 20 von diesen unterschiedlichen 60 000 molekularen Markern weiß man aus der Literatur, dass die was mit dem Krebs zu tun haben.  Wenn man jetzt also versucht, diese Vorhersage zu machen für unterschiedliche Krebsarten, kriegen wir ungefähr 50 bis 60 von diesen 60 000, die vorhersagbar sind. Aber nur von 10 bis 20 von diesen 60 wissen wir, dass es wirklich in der Literatur Evidenz gibt. Und Wissen gibt, das die was mit dem Krebs zu tun haben. Heißt, es gibt eventuell 40 mehr Kandidaten, an die man bisher noch nicht gedacht hat. (***Klaus Robert Müller***)

Mit Hilfe der künstlichen Intelligenz sind Wissenschaftler also solchen Genveränderungen auf die Spur gekommen, die von Ärzten bislang  nicht als Krankheitsursache erkannt worden waren. Welche Bedeutung diese Genveränderungen haben, muss zwar noch weiter untersucht werden. Klar ist aber schon jetzt: Künstliche Intelligenz hilft Medizinern nicht nur dabei, die ständig zunehmenden Datenmengen schneller zu analysieren. Künstliche Intelligenz beschleunigt auch den Erkenntnissgewinn. Doch Ärzte ganz ersetzen kann künstliche Intelligenz nicht, ist Fredrik Klauschen überzeugt. Noch nicht.

>Was die quantitative Unterstützung als eine Art Diagnose Assistenzsystem angeht, da sind wir unmittelbar da. Das, was teilweise gesagt wird, bald braucht es keine diagnostisch tätigen Ärzte mehr oder vielleicht sogar keine Internisten. Wir brauchen nur den Chirurgen und auch der wird irgendwann durch den Roboter ersetzt. Das sind Zukunftsvisionen, die vielleicht irgendwann Realität werden. Aber das ist etwas was eher in der Größenordnung von Jahrzehnten meines Erachtens noch entfernt ist als jetzt, wie es teilweise dargestellt wird, schon unmittelbar vor der Tür steht. (***Frederick Klauschen***)
