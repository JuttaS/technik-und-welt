---
date: 2019-05-29 12:31 CEST
permalink: deep-learning
blog: podcast
title: Mit Deep Learning gegen Tuberkulose in Südafrika 
short_title: Deep Learning im Kampf gegen TB
tags:
 - Gesundheit
 - KI
 - Künstliche Intelligenz
image: emily-wong-hpi-digital-health.jpg
image_alt_title: Emily Wong auf der HPI Digital Health Konferenz in Potsdam 
soundcloud_link:
lybsin_link: 9965345
published: true
author: Jutta Schwengsbier
introartikel: Südafrika ist das Epizentrum der weltweiten Aids- und Tuberkulose Epidemie. Eine Gesundheitsstudie per KI soll die Seuchen eindämmen helfen.
---

Vor einiger Zeit machten Google Spezialisten mit Katzenvideos Schlagzeilen. 
Ihrem Algorithmus war es gelungen, in Millionen von youtube Videos Katzen richtig zu indentifizieren. Viele fragten sich: Ist das wirklich ein wichtiger Meilenstein im Einsatz künstlicher Intelligenz? Katzen in Videos markieren? Inzwischen wird die „Deep Learning“ genannte Technologie eingesetzt, um Krankheitssymptome durch Bild- und Datenanalyse zu identifizieren. Und das ist tatsächlich ein Quantensprung für die medizinische Diagnostik.

##Südafrika setzt Deep Learning zur Seuchenbekämpfung ein  

Südafrika gilt als das Epizentrum der weltweiten Aidsepidemie. Gleichzeitig ist etwa in der südafrikanischen Provinz Kwa-Zulu Natal die Mehrheit der erwachsenen Bevölkerung zusätzlich noch an Tuberkulose erkrankt. 

>60 Prozent der Frauen im  Alter zwischen 35 bis 45 haben Aids. 60 Prozent. Was das bedeutet, sprengt wirklich unsere Vorstellungskraft. (***Emily Wong***)

Emily Wong ist Spezialistin für Infektionskrankheiten am afrikanischen Institut für Gesundheitsforschung in Durban. Die US-amerikanische Ärztin hat an der Harvard Medical  School studiert. 

>SAls ich noch Medizinstudentin war, ging die Lebenserwartung in ganz Afrika dramatisch zurück. Besonders in Südafrika. Das ist ein ganz ausserordentlicher Vorgang, wenn man bedenkt, dass die Lebenserwartung seit dem zweiten Weltkrieg kontinuierlich gestiegen ist. Der Rückgang der Lebenserwartung war deshalb sehr schockierend aus der Perspektive von Ärzten. Und der Grund war die Aids Epidemie. (***Emily Wong***)

Emily Wong entschloss sich deshalb nach Afrika zu gehen und mitzuhelfen, die Seuche zu bekämfen.

>Mitzuerleben, wie die Lebenserwartung einer ganzen Bevölkerung sinkt und dann wieder ansteigt durch Maßnahmen der öffentlichen Gesundheitsvorsorge, ist wirklich bemerkenswert. Das haben wir in Südafrika in den vergangen 15 Jahren erlebt. (***Emily Wong***)

Die größte Herausforderung derzeit sei es, die Neuinfektion der nächsten Generation zu verhindern.  In Südafrika ist die Hälfte der Bevölkerung jünger als 15 Jahre.

>Unsere Jugend ist dem großen Risiko ausgesetzt, sich mit Aids oder Tuberkulose anzustecken. Das gilt besonders für junge Frauen. (***Emily Wong***)

##Deep Learning ermöglicht grösste Gesundheitstudie Afrikas

Südafrika hat sich deshalb entschlossen, in der Provinz KwaZulu-Natal die größte Bevölkerungsstudie mit mobilen Kliniken zu machen, die je in Afrika durchgeführt wurde. Dabei wird zum ersten mal die Gesundheit der rund 120000 Menschen in den überwiegend ländlichen Gebieten umfassend untersucht. Emily Wong leitet diese Studie.

>Für viele Menschen ist es das erste Lungen-Röntgenbild in ihrem Leben. Bei unserer  Bevölkerung haben wir eine der höchsten Raten von Tuberkulose in der Welt festgestellt. Trotzdem haben unsere Krankenhäuser keine Röntgengeräte. Mit unseren mobilen Kliniken können wir solche Röntgenbilder zum ersten mal machen. Bislang konnten wir 100 Tuberkulosekranke identifizieren und die Behandlung beginnen. Das ist ein direkter Erfolg des Gesundheitsscreenings. (***Emily Wong***)

Die Kliniken in der Provinz Kwa Zulu Natal haben nicht nur keine Röntgengeräte. Es gibt auch kaum Ärzte, die nun die von den neuen mobilen Kliniken gemachten Röntgenbilder analysieren könnten. Das Hasso Plattner Institut in Berlin unterstützt Emily Wong deshalb dabei, die Röntgenbilder mit Methoden des maschinellen Lernens nach Anzeichen von Tuberkulose zu untersuchen. Christoph Lippert leitet den Lehrstuhl für digitale Gesungheit und maschinelles Lernen am HPI in Potsdam. Der Informatiker ist darauf spezialisiert, Informationstechnologien für große Gesundheitsstudien einzusetzen. 

>Was wir jetzt aber sehen ist, dass es viele Leute gibt, die keine aktive Tuberkulose haben sondern irgendwann mal in ihrem Leben eine Tuberkulose hatten. Dadurch haben die Zeichen von Narben in der Lunge, die wir auf den Bildern sehen. Deswegen werden Leute, die quasi heute gesund sind, werden dort fehlklassifiziert. (***Christoph Lippert***)


##Afrika braucht neue Algorithmen

Die bislang in Europa entwickelten Computermodelle können zwar schon Anzeichen von Tuberkulose in Röntenbildern erkennen. Die Algorithmen können aber noch nicht unterscheiden zwischen akut Kranken und bereits geheilten Patienten, erläuert Lippert. Für die Gesundheitsstudie in Südafrika soll deshalb ein neuer Algorithmus mit den Röntgenbildern aus Südafrika trainiert werden. 

>Genau das ist in dem Feld von Superviced Learning. Dass man Bilder rein gibt, wo man dann auch wirklich dem Algorithmus sagt: Das ist ein Krankheitsbild. Das ist ein gesundes Bild. Und dann lernt der Algorithmus anhand dessen genau die Krankheitsbilder von den gesunden Bildern zu unterscheiden. (***Jana Fehr***)

Als Teil Ihrer Doktorarbeit am Hasso Plattner Institut will Jana Fehr einen neuen, auf südafrikanische Tuberkulosebilder abgestimmten Algorithmus entwickeln. Dafür müssen die Röntgenbilder der akut Erkrankten zunächst von Experten markiert werden. Irgendwann soll das Programm dann aber selbständig die Anzeichen der unterschiedlichen TB-Narbenformen in den Lungen von Kranken und Geheilten  unterscheiden können. Das ist alle andere als einfach, erläutert Emily Wong. Selbst menschlichen Spezialisten fällt die Entscheidung oft schwer.

>Wir wissen, dass HIV sich auch auf die Tuberkulose Erkrankung auswirkt. Die Lungen-Röntgenbilder sehen ganz anders aus bei Menschen, die beide Krankheiten haben. Bei unserem Gesundheitsscreening bekomen wir also Röntgenbilder die einzigartig sind. Jana hat sich schon mit unserem Radiologen ausgetauscht. Er hat eine Wette darauf abgeschlossen, dass Ihr Algorithmus niemals so gut wird, wie er selbst. Wir freuen uns auf den Wettbewerb. Wir haben einfach nicht genug Radiologen wie ihn, um unseren Bedarf zu decken. (***Emily Wong***)

In Südafrika werden bei der Gesundheitsstudie Hunderttausende Röntgenbilder gemacht. Der vom HPI entwickelte Algorithmus soll zunächst vor allem eine Vorauswahl treffen. Die betroffenen Patientinnen und Patienten werden dann weiter untersucht. Und, sofern nötig, behandelt. Wer am Ende aber die Wette in Südafrika gewinnt, ist noch völlig offen. Könnten die  Algorithmen irgendwann bessere medizinische Diagnosen stellen als Menschen? Für Joel Dudley hat sich die Frage schon beantwortet - zugunsten der  Computer.

##KI gibt neue Impulse in der medizinischen Diagnostik

>Die zweite Generation von Algorithmen kann biologische Daten bewerten, ohne zuvor von  Krankheitsexperten angeleitet worden zu sein. Wir haben das mit Daten von Alzheimer-Patienten ausprobiert. Ohne Hypothesen vorzugeben, was die Krankheit verursacht, haben Algorithmen die Unterschiede in riesigen Datensätzen zwischen gesundem Gehirn und krankem Gehirn analysiert. Völlig unerwartet wurde ein Virus gefunden, der zur Alzheimer Krankheit beitragen könnte. Darauf war zuvor noch niemand gekommen. (***Joel Dudley***)

Joel Dudley ist auf Gesundheitsinnovationen spezialisiert und arbeitet für das Mount Sinai Gesundheitssystems, einen Krankenhausverbund in New York. Gemeinsam mit anderen Forschern ist Dudley nun dabei, mit klinischen Studien zu überprüfen, welche Rolle Viren bei der Alzheimer Erkrankung spielen. Klar scheint aber jetzt schon: Künstliche Intelligenz kann Menschen bei der medizinischen Diagnostik mit entscheidenden Impulen unterstützen.

