---
date: 2019-06-04 07:04 CEST
permalink: estlands-biodatenbank
blog: podcast
title: "Estlands Biodatenbank – Genomanalyse zur vorbeugenden Gesundheitsversorgung"
short_title: Estlands Biodatenbank
tags:
 - Genom
 - Estland
 - Gesundheit
 - Biodatenbank
image: estlands-gesundheitssystem-ist-vorreiter-bei-genom-analysen.jpg
image_alt_title: Stadtschloss in Tallin
soundcloud_link:
lybsin_link: 10012673
published: true
author: Jutta Schwengsbier
introartikel: Estland lässt die Genome seiner Bevölkerung analysieren um genetische Risikofaktoren zu erkennen. Gesundheit rangiert vor Datenschutz, sagen die Mediziner. 
---

 
Wer wird früh krank werden und wer wird vermutlich lange gesund bleiben? Das Gesundheitssystem könnte viel Geld sparen, wenn das rechtzeitig bekannt wäre. Estland hat sich deshalb entschieden, eine Biodatenbank aufzubauen. Anhand von Genomanalysen seiner Bevölkerung sollen so Krankheitsrisiken möglichst frühzeitig erkannt und behandelt werden. 

Andres Metspalu ist Professor an der Universität Tartu in Estland. Als Mediziner und Mikrobiologe beschäftigt sich Metspalus seit 25 Jahren mit Humangenetik. In den vergangenen 10 Jahren hat er eine Biobank und ein Forschungsinstitut für Genetik gegründet. Die bislang von Wissenschaftlern gesammelten Erbinformationen repräsentieren ungefähr 15 Prozent der estnischen Bevölkerung. In Estland leben rund eineinhalb Millionen Menschen.

>Wir müssen nicht das Genom jedes Einzelnen sequenzieren, um repräsentativ zu sein. Im Moment sind wir bei 5000. Wenn wir 20.000 haben, reicht das für Estlands Bevölkerung völlig aus. Damit können wir sehr gute Vorhersagen machen, die auch seltene Variationen einschliessen. (***Andres Metspalu***)

##Der genetische Risikofaktor zeigt Krankheitsrisiken

Der von Andres Metspalu und seinem Team entwickelte „genetische Risikofaktor“ soll künftig dabei helfen, Krankheiten vorherzusagen. Klaus Robert Müller von der Technischen Universität Berlin entwickelt Computermodelle, die die medizinische Diagnostik unterstützen. Längst ist noch nicht alles entschlüsselt, erläutert der Informatiker. Doch von einigen Genveränderungen weiss man bereits, mit welchen Krankheiten sie verbunden sind. Und bei anderen vermutet man zumindest, dass sie für weitere Studien interessant sein könnten. 

>Für ungefähr zehn bis 20 von diesen unterschiedlichen 60 000 molekularen Markern weiß man aus der Literatur, dass die was mit dem Krebs zu tun haben. Wenn man jetzt also versucht, diese Vorhersage zu machen für unterschiedliche Krebsarten, kriegen wir ungefähr 50 bis 60 von diesen 60 000, die vorhersagbar sind. Aber nur von 10 bis 20 von diesen 60 wissen wir, dass es wirklich in der Literatur Evidenz gibt. Und Wissen gibt, das die was mit dem Krebs zu tun haben. Heißt, es gibt eventuell 40 mehr Kandidaten, an die man bisher noch nicht gedacht hat. (***Klaus Robert Müller***)
##Vorbeugende medizinische Versorgung von RisikopatientInnen

Obwohl also noch viele Fragen offen sind: Estland ist schon so weit, aus den wenigen bekannten Genmutationen auf mögliche Krankheitsrisiken zu schließen. Und Menschen vorbeugend medizinisch zu versorgen, die noch gar nicht krank sind.

>Wir haben die ersten Pilotprojekte in Krankenhäusern gestartet.  Damit sollen Ärzte damit vertraut gemacht werden, Menschen zu behandeln, die ein hohes Risiko haben, krank zu werden aber noch keine Anzeichen einer Krankheit zeigen. (***Andres Metspalu***)

Anhand ihrer genetischen Disposition können Ärzte über die Biobank schon jetzt Risikopatientinnen erkennen, erläutert Andres Metspalu. Betroffene sollen in Estland künftig zum Beispiel viel früher zu Vorsorgeuntersuchungen eingeladen werden.

>Einige Frauen bekommen Brustkrebs lange bevor sie 50 Jahre alt sind. Doch unsere Gesundheitsuntersuchungen beginnen erst  ab dem Alter von 50 Jahren. Deshalb untersuchen wir jetzt die Gendatenbank nach Markern, um festzustellen, wer ein erhöhtes Krebsrisiko hat. (***Andres Metspalu***)

##Kosten im Gesundheitssektor sparen und trotzdem gute Versorgung bieten

Es wäre viel zu teuer, sagt Metspalu, regelmäßige Gesundheitsscreenings für die gesamte Bevölkerung zu machen. Durch die gezielte Analyse von genetischen Defekten anhand der Biodatenbank können Betroffene in Estland nun aber 20 Jahre früher zur Vorsorgeuntersuchung eingeladen werden. Nicht nur zur Krebsvorsorge sondern zum Beispiel  auch bei erblich bedingten Herzleiden.

>Auf Grund ihrer genetischen Veranlagung, die in unserer Biobank analysiert wurde,  haben wir Menschen zur Untersuchung in Herzkliniken eingeladern. Ungefähr die Hälfte der Untersuchten hatten keine Ahnung, dass sie ernsthafte Herzprobleme haben, die von Genmutationen verusacht werden. (***Andres Metspalu***)

Weil die Krankheit vererbt wird, wurden auch die Familienmitglieder von Betroffenen zur Vorsorgeuntersuchung eingeladen.  Estlands Gesundheitsdaten sind schon lange voll digitalisiert, erläutert Metspalu. Die Biodatenbank ist dabei nur ein weiterer Baustein bei der Gesundheitsvorsorge.

>Wir haben so was wie eine Helikopter Sicht. Wir sehen alles. Alle Gesundheitsdaten sind bei uns elektronisch erfasst. Wir wissen, wer ins Krankenhaus geht, welche Diagnose gestellt wird und was für Medikamente er bekommt. Wenn jemand eine allergische Reaktion hat, merken wir das auch. Dann wissen wir, dass dieses Medikament auch nicht gut für Familienmitglieder ist. Wir sammeln solche Informationen in großen Datenbanken. Damit niemand mehr falsche Medikamente bekommt. (***Andres Metspalu***)

##Reicht Datenschutz auf hohem Niveau um Mißbrauch zu verhindern?

Wer Zugang zu diesen Gesundheitsdaten habe, sei in Estland aber streng reglementiert, sagt Metspalu. Und die Patientinnen und Patienten können selbst darüber entscheiden, wem sie ihre Daten offenlegen wollen, - und wem nicht. Um ein Genome sequenzieren zu dürfen, sei natürlich die ausdrückliche Zustimmung der Betroffenen notwendig.

>Das, was wir mit unserer Biobank machen dürfen, ist per Gesetz geregelt. Ein Genom analysieren dürfen wir nur mit einer ausdrücklichen, informierten Zustimmung des Betroffenen. Das müssen sie unterschreiben. Das machen wir jetzt seit 20 Jahren. Ungefähr 75 Prozent der Bevökerung unterstützen, was wir machen. 20 Prozent zögern noch, weil sie der Genetik nicht vertrauen. Sie meinen, sie sollten das nicht tun. Zumindest jetzt noch nicht. (***Andres Metspalu***)

Obwohl also immerhin fast ein Viertel der Bevölkerung gegen die allgemeine Genomanalyse ist, hat sich Estland entschieden, diesen Weg zu gehen. Davon betroffen sind alle. Eine Genomanalyse reicht aus, um die Erbinformationen von ganzen Familienzweigen zu haben. Estland hat den erwarteten medizinischen Nutzen über den Datenschutz gestellt. Auf andere Länder übertragbar scheint das Modell aber noch lang nicht zu sein.










