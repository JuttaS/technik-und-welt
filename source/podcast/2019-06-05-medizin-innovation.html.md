---
date: 2019-06-05 20:29 CEST
permalink: medizin-innovation
blog: podcast
title: Individualisierte Medizin
short_title: Medizin Innovation
tags:
  - Medizin
  - Gesundheit
  - Innovation
image: human-sceleton.jpg
image_alt_title: Medizin wird neu gedacht
soundcloud_link:
lybsin_link: 9731846
published: true
author: Jutta Schwengsbier
introartikel: Organe auf dem Chip für Medikamententests. Virale Krebstherapie. Innovationszentren für unser Gesundheitssystem. Die Medizin erfindet sich gerade neu.
---

Alle 30 Sekunden stirbt ein Mensch durch Krankheiten, die eigentlich behandelt werden könnten. Wäre es nicht wunderbar, wenn wir uns selbst erneuern könnten? So wie bei Salamandern ein Bein nachwächst, wenn es abgetrennt wurde? Sie meinen, das geht leider nicht? Doch. Mit dieser Vision von Augen haben sich Wissenschaftler im Bereich der regenerativen Medizin schon vor mehr als einem Jahrzehnt daran gemacht, Organe nachwachsen zu lassen. Bei einigen Organen gelingt das schon ganz gut. Andere funktionieren bislang nur in einem Miniaturformat. Doch der medizinische Fortschritt entwickelt sich in exponentiellen Schritten weiter.

Um menschliche Organe nachwachsen lassen zu können, wie bei Salamandern, entwickelte Anthony Atala das Prinzip von 3-D-Druckern für medizinische Anwendungen weiter. Zunächst wird dabei eine Gewebestruktur per Drucker in der Größe und Form des gewünschten Organs ausgedruckt. Anschließend werden körpereigene Zellen der Patienten in die leere Gewebestruktur injiziert. Innerhalb von 4 bis 6 Wochen wachsen die Zellen dann in einem Inkubator zu Muskelzellen oder Blutgefäßen heran. Anschließend kann das nachgebaute Organ implantiert werden. Anders als bei Spenderorganen gibt es dabei keine Abstoßungsreaktion des Körpers, erläutert der Direktor des Wake Forest Instituts für Regenerative Medizin in den USA

>Die meisten Patienten haben die Organe gut vertragen. Sogar Kinder. Das interessante dabei ist: Wenn die Kinder wachsen, dann wachsen die Organe mit. Weil der Körper die Organe als eigene anerkennt. (***Anthony Atala***)

##Organe züchten statt Plastik in den Körper einsetzen

Dass gezüchtete Organe mitwachsen können, betont Atala besonders, weil es für Ihn Antrieb war, überhaupt mit der Forschung anzufangen. Als junger Arzt musste er bei Operationen noch Plastik nutzen, um beschädigte Organe zu reparieren. Bei einigen Kindern musste er sogar fehlende Organe mit Plastikkonstruktionen ganz ersetzen. Er fragte sich: Geht das nicht besser? Auch wenn Atala heute Organe vergleichsweise schnell nachwachsen lassen kann: Es dauerte fast ein Jahrzehnt, bis die Prozesse von der Zellexpansion bis zum Gewebedruck tatsächlich funktionierten und das erste Organ implantiert werden konnte.

>Interessanterweise haben alle Zellen in ihrem Körper genau die Informationen, die gebraucht werden. Alles um ein neues Selbst, um Sie noch einmal zu schaffen. (***Anthony Atala***)

Das Wakefield Institut experimentiert derzeit mit ca. 30 verschiedenen Organen und Gewebestrukturen. Bei Haut, Muskelfasern oder Blutgefäßen ist die Reproduktion inzwischen Routine. Röhren oder Hohlkörper wie Harnblasen können inzwischen als Gewebestruktur vorgedruckt und dann mit Zellen ausgefüllt werden. Ob und wann das langfristig auch bei komplexeren Strukturen gelingt, sei aber noch völlig offen, sagt Atala. 

>Am kompliziertesten sind feste Organe wie Herz oder Lunge. Wir benutzen verschiedene Strategien. Bis jetzt haben wir aber noch kein festes Organ implantiert. (***Anthony Atala***)


##Medikmententest mit Organchips sollen Tierversuche ersetzen

Auch wenn Organe wie Herz, Leber und Lunge noch nicht in Originalgröße reproduziert werden können. Im Miniaturformat funktionieren sie schon. Das brachte Atala auf die Idee, verschiedene Mini-Organe auf einem Mikrochip über Biosensoren zu verbinden und dann Medikamente mit dem Organchip zu testen. Mit Erfolg.

>Zum Beispiel können wir eine Leberbiopsie eines Patienten nehmen, der Leberkrebs hat. Mit den kranken Zellen bauen wir Miniaturorgane, verbinden sie auf einem Miniaturchip und testen daran alle im Moment verfügbaren Medikamente. Und dann können wir das beste Medikament nehmen, das im Moment verfügbar ist, für genau diesen Patienten. Das ist personalisierte Medizin. Alles was wir heute besprochen haben, nutzt die eigenen Zellen der Patienten. (***Anthony Atala***)

Es wird sicher noch dauern, bis solche Forschungsmethoden der individualisierten Medizin tatsächlich im Klinikalltag angekommen sind. Zunächst müsse überprüft werden, ob die miniaturisierten Organe auf dem Mikrochip tatsächlich so funktionieren wie die Organe in unserem Körper, erläutert Reyk Horland. Er ist bei der Berliner Biotech Firma TissUse für die Entwicklung und Vermarktung von Multi-Organ Chips zuständig. 

>Was man jetzt hier hört, dieses klackern, das sind die Pumpen auf dem Chip, die diesen pulsatilen Fluss erzeugen, der ähnlich eines Blutflusses ist, den das Herz erzeugt. Das Klackern sind halt das öffnen und schließen der Ventile, die dann benötigt werden, damit man diesen pulsatilen Fluss erzielt. (***Reyk Horland***)

Zunächst gehe es darum zu überprüfen, dass sich die Miniorgane auf einem Chip tatsächlich so verhalten wie unsere Organe im Körper. Dann könnten bald auch in der Medikamentenentwicklung Tierversuche durch Test mit Organ-Chips ersetzt werden.

##Organchips ermöglichen individualisierte Medizin 

>Dann kann ich definitiv auch irgendwann sagen, ja, ich habe den Patienten auf dem Chip, den individualisierten Patienten auf dem Chip, weil ich dann auch mit diesem System absolut in der Lage wäre zu sagen, ich verkürze die gesamt Medikamentenentwicklung dramatisch. dann schrumpft die ganze Entwicklungszeit, die jetzt ca bei 12 Jahren liegt, dramatisch zusammen. (***Reyk Horland***)

Das wäre nicht nur deutlich billiger. Medikamente könnten auch besser angepaßt werden an individuelle Unterschiede von Patientengruppen.

>Wenn ich Alkohol trinke als Europäer, dann vertrage ich mehr Alkohol als zum Beispiel ein Asiate, weil wir einfach von dem Alkohol abbauenden Enzym mehr haben als der Asiate. Und das ist ein Unterschied, den man in der Medikamentenentwicklung beachten muss. Und das ist der erste Schritt, was man auch mit diesen Chips abbilden kann. Diese unterschiedlichen Genotypen in der Bevölkerung, die sehr wichtig sind. (***Reyk Horland***)

##Medikamente können künftig auf Patientengruppen angepasst werden

Wie Horland sind sich die meisten Experten einig, dass die bisherigen Verfahren von Pharmafirmen für die Medikamentenentwicklung viel zu langwierig sind. Dazu kommt noch, dass Medikamente bislang immer nur in einer Dosierung entwickelt und verabreicht werden. Unterschiede zwischen Patientengruppen, zwischen Männern und Frauen, zwischen Europäern und Asiaten, zwischen Erwachsenen und Kindern, werden bislang einfach nicht ausreichend berücksichtigt. Wenn die Test mit den Mini-Organen auf einem Chip erfolgreich verlaufen sollten, könnten Medikamente in Zukunft viel schneller entwickelt und zudem direkt auf einzelne Patientinnen und Patienten abgestimmt werden. 

##Individualisierte Medizin auch bei Krebstherapie möglich

Andrew Hessel arbeitet als Wissenschaftler für Autodesk Live Sciences in San Francisco. Dort betreibt der Zellbiologe und Genetiker Grundlagenforschung im Bereich synthetischer Biologie. Nach Ansicht von Hessel kommen wir der Vision einer individualisierten Medizin in großen  Schritten näher. 

>Wenn Ihr Gesundheitspartner in seiner Diagnose feststellt, dass sie eine Mikrobe haben oder einen Impfstoff benötigen oder vielleicht Krebs haben, dann wird die Information künftig direkt zu einer Fabrik fließen. Dort wird ihre Medizin speziell für sie hergestellt. Wenn wir am Ende gut genug sind, wenn unsere Geräte klein genug werden, dann ist es aber auch gut möglich, dass sie ihre Medizin sogar zuhause selbst herstellen können. Mit einem kleinen Werkzeug, das nicht viel anders aussieht als eine Spritze oder ein Inhalationsgerät. Darin werden alle notwendigen Prozesse ablaufen, um viele verschiedene medizinische Produkte herstellen zu können. Einfach indem die Medikamenten-Designs aus einer Datenbank herunter geladen werden. (***Andrew Hessel***)

Science Fiction? Keineswegs, lacht Hessel. Heute habe tatsächlich jeder, der sich mit synthetischer Biologie beschäftige, diese Fähigkeiten. Ganze Moleküle oder Proteine können inzwischen am Laptop designt werden, erläutert Hessel, mit vorgefertigten Bauplänen, die aus Datenbanken heruntergeladen werden können. Das sei wesentlich billiger als Medikamente weiter durch Pharmafirmen herstellen zu lassen. Hessel erwartet deshalb einen großen Umbruch der ganzen Branche.

##Synthetische DNA aus dem 3-D Drucker ersetzt Medikamente

>Normalerweise dauert es 15 Jahre ein Medikament zu entwickeln. Wenn sie ein Antibiotikum herstellen wollen, das zum Beispiel speziell gegen Krebs wirkt, müssen sie ganz neu überlegen. Ihr Antibiotikum soll schnell herzustellen und ausserdem billig sein, damit sie so viele Menschen wie möglich behandeln können. Dann müssen sie die Art, wie Medikamente hergestellt werden, ganz neu denken. Die synthetische Biologie, das heißt DNA digital zu programmieren und zu designen, Organismen herzustellen, ist ein ganz wunderbares Werkzeug, um das zu machen. Sie erhalten im Wesentlichen die Macht einer großen Biotechnologiefirma wie Sie vielleicht vor 20 Jahren existierte. (***Andrew Hessel***)

So wie Anthony Atala inzwischen Organe mit einem 3-D Drucker einfach ausdrucken kann, so können auch Zellbiologen wie Andrew Hessel inzwischen DNA einfach ausdrucken. Und in DNA Molekülen stecken alle genetischen Informationen, die für das Wachstum, die Entwicklung, die Funktion und auch die Reproduktion von allen lebenden Organismen verantwortlich sind, erläutert Hessel.

>Das Herzstück dieser Wissenschaft ist tatsächlich synthetische DNA. Ich möchte mich dabei klar ausdrücken: Synthetische DNA ist das gleiche wie natürliche DNA. Sie können ein Molekül aus einem lebenden Organismus gewinnen. Wir setzen im Gegensatz dazu DNA aus den chemischen Einzelteilen Stück für Stück zusammen durch so eine Art Drucker. (***Andrew Hessel***)

Indem der Bauplan der DNA nach und nach immer mehr verändert wird, können auch die Eigenschaften der Moleküle verändert werden. So wie Software-Programme darüber entscheiden, was mit Computern alles gemacht werden kann, so entscheidet der DNA Code darüber, wie sich lebendige Organismen verhalten, erläutert Hessel.

>Einige der ersten DNA Programme, die wir geschrieben haben, waren dazu da, Hormone zu bilden. Zum Beispiel Insulin, das heute von Millionen von Diabetikern genutzt wird. Oder das menschliche Wachstumshormon. Oder Medikamente für Bluter und vieles mehr. Die erste Generation der Biotech-Industrie, die ganze pharmazeutische Industrie, haben am Ende dieses genetische Verfahren genutzt, um Produkte herzustellen, die vielen Patienten geholfen haben. (***Andrew Hessel***)

##Biotechnologie entwickelt sich rasant weiter 

Genauso wie Computerprogramme zu Beginn nur einfache Operationen erlaubten, so hatten auch Genetiker am Anfang nur wenige Möglichkeiten, in den Bauplan von Organismen einzugreifen, erinnert sich Hessel.

>Anfang der 1970er Jahre konnten wir nur sehr einfache Werkzeuge benutzen. Wir haben wirklich die Moleküle verändert. Genauso, wie wir physisch Filme geschnitten haben oder magnetische Bänder, indem wir sie geschnitten und wieder zusammengeklebt haben, um Veränderungen daran vorzunehmen. Diese Art von DNA Editierung erlaubte uns nur ganz einfache Veränderungen. Das hat unsere Fähigkeiten stark begrenzt, etwas Neues auszuprobieren. (***Andrew Hessel***)

Seit kurzer Zeit ermöglichen es neue Genom-Editing Verfahren, die DNA von Pflanzen, Tieren und auch von Menschen sehr treffsicher und zudem ausgesprochen billig zu verändern. 

>Heute benutzen wir digitale Schreibwerkzeuge, um das DNA Programm zu verändern oder um es von Anfang an ganz neu zu schreiben. Das funktioniert nur mit einem Laptop. Wenn das Design erst einmal fertig ist, drücken sie auf einen Knopf und sagen: „Druck die DNA aus“. (***Andrew Hessel***)

Modernen Genom Editing Methoden bringen den Traum näher, Superpflanzen und gesunde produktive Nutztier züchten zu können. Andrew Hessel arbeitet daran, eine Heilung für Krebs zu finden.

##Viren als Superwaffe gegen Krebs?

>Krebs ist eine sehr einfache Krankheit. Die Basis ist ein genetischer Fehler auf Zell-Ebene. Dadurch kann die kranke Zelle sehr schnell wachsen. Aber sie ist nicht so krank, dass sie absterben würde. In vielen Fällen kann Krebs mit einer Infektion verglichen werden. Aber unser Körper ist nicht infiziert mit einer Mikrobe oder einem Virus sondern mit unseren eigenen kranken Zellen. (***Andrew Hessel***)

Statt Tumore herauszuschneiden, ist es inzwischen möglich, körpereigene Prozesse so zu beeinflussen, dass unser Immunsystem sich gegen die kranken Zellen selbst wehren kann, erläutert Hessel.

>Im Wesentlichen werden heute zwei Möglichkeiten ausprobiert als gezielte Krebsmedikamente.// Einmal sind das Antikörper, die Teil unseres Immunsystems sind. Antikörper können fremde Invasoren wie Bakterien oder Viren angreifen. Sie können Antikörper aber auch ganz speziell auf Krebszellen ansetzen. Antikörper sind eigentlich komplexe Proteine, die direkt in die DNA eingefügt werden können. Das ist ein sehr attraktives Verfahren, um biologische Medikamente zu machen. (***Andrew Hessel***)

Andrew Hessel beschäftigt sich selbst nicht mit Antikörpern als Krebstherapie, sondern mit Viren. Normalerweise sind Viren dafür bekannt, Krankheiten wie zum Beispiel Grippe durch Tröpfcheninfektionen zu verbreiten. Alle Viren enthalten also ein Programm, um sich ausbreiten zu können. Viren können sich aber im Gegensatz zu lebenden Organismen nicht selbst vermehren und sind deshalb darauf angewiesen, von befallenen Wirtszelle repliziert zu werden. Diesen Mechanismus versucht Hessel auszunutzen, um Krebszellen mit Viren zu infizieren.

>Wir nennen das eine virale Therapie. Virale Therapien oder onkolytische Viren, sind im wesentlichen Viren die Krebszellen aufbrechen. Sie können es sich so vorstellen, als würden sie eine Krebszelle mit einer Erkältung infizieren. Dann fängt diese Zelle an Viruspartikel zu produzieren. Am Ende wird die Krebszelle in sich zusammenfallen. Sie wird aufbrechen und noch mehr Viren freisetzen. Und diese Viren werden dann andere Krebszellen befallen. Das ist ein sehr interessanter Mechanismus, wie man eine biologische Medizin herstellen kann. So als würde der Virus die Krebszelle kapern und daraus eine kleine Fabrik zur Herstellung von Medikamenten machen. (***Andrew Hessel***)

##Viren werden zur körpereigenen Medizinfabrik

Und das Ganze ohne gravierende Nebenwirkungen. Gesunde Zellen bleiben intakt. Die Krebszellen werden von den Viren zerstört und die Viren im Anschluss von der körpereigene Immunabwehr ausgeschaltet. Eine clevere Idee. Um die Theorie auch in der Praxis zu erproben, hat Hessel gemeinsam mit einem Tiermediziner der Universität Alabama begonnen, Hunde, die an Krebs erkrankt sind, mit onkolytischen Viren zu behandeln. Dazu haben die Wissenschaftler den Bauplan eines bereits klinisch getesteten Virus aus einer Datenbank heruntergeladen und dann ausgedruckt.

>Das Virus war mehrfach für klinische Versuche benutzt und an Hunden ausprobiert worden. Die Nutzung dieses Virus, als ein Krebs bekämpfender Virus, war also zuvor mehrfach bestätigt. Wir haben dieses virale Genom synthetisiert, das heißt es ganz neu konstruiert aus verschiedenen Blöcken von DNA. Was sie am Ende erhalten, ist ein kleines biologisches Programm in einem Röhrchen, das von einer Maschine gedruckt worden ist. // Dann haben wir diese synthetische DNA genommen und sie in eine Krebszelle eingefügt. Diese Krebszelle hat dann dieses Programm gelesen und ausgeführt. Und daraus Virus Partikel hergestellt als eine Art der Behandlung. Am Ende säubert das Immunsystem des Hundes diesen Virus. Genauso wie wir eine Erkältung überwinden. (***Andrew Hessel***)

##Im Tierversuch wird die virale Krebstherapie schon eingesetzt

In seinen Versuchen mit Hunden ist Hessel einer wirksamen Krebstherapie schon sehr nahe gekommen. Hunde könnten deshalb bald besser gegen Krebs behandelt werden als Menschen, meint Hessel.

>Es gibt schon die ersten zielgerichteten Medikamente, die auf gemeinsame Schwächen von Krebszellen ausgerichtet sind. Das Problem dabei: Die seit fast 100 Jahren bestehenden Regeln, wie Medikamente hergestellt werden dürfen. Dadurch wird alles nur viel teurer und sehr langsam. (***Andrew Hessel***)

Wenn die Tests mit den Mini-Organen auf dem Chip erfolgreich verlaufen, könnte das die Entwicklung von Krebstherapien auch für Menschen beschleunigen. Doch das alleine reiche nicht, urteilt Zayna Khayat. Als Expertin für Gesundheitsinnovationen leitet Khayat normalerweise ein medizinisches Innovationszentrum in Toronto. Derzeit ist sie aber von Kanada an Holland ausgeliehen, um auch Europa auf die bevorstehende Revolution im Gesundheitsbereich vorzubereiten.

##Gesundheitsinnovationen entwickeln sich in exponentiellen Schritten weiter

>Die exponentielle Intelligenz, die virtuelle Realität, Block Chain: Diese Dinge werden das Gesundheitssystem wirklich erschüttern. Wir versuchen vorne mit dabei zu sein. Deshalb haben wir ein globales Netzwerk von Gesundheits-Innovationszentren.gebildet. Ein Land alleine schafft das nicht. Wir sind dabei, ungefähr zehn Gesundheitssysteme aus verschiedenen globalen Regionen zusammenzuschließen. (***Zayna Khayat***)

Krankenhäuser, so wie wir sie heute kennen, werde es bald nicht mehr geben, urteilt Khayat. Ein Gesundheitsdienstleister in Kalifornien, in den USA, versorge inzwischen mehr als 50 Prozent seiner Patientinnen und Patienten virtuell. In Kanada oder Deutschland sind es derzeit kaum 3 bis 4 Prozent. Doch der Trend sei eindeutig. Bald werde Virtuelle Realität aus dem medizinischen Alltag nicht mehr wegzudenken sein.

>Es gibt ein Modell mit einem lokalen Assistenten, der ihnen Zuhause zur Seite steht. Das ist eine sehr viel billigere Arbeitskraft als ein Spezialist. Durch virtuelle Werkzeuge können Experten sie mit Hilfe der Assistenten aus der Ferne direkt behandeln, egal wo sie leben. (***Zayna Khayat***)

Durch neue Behandlungsmethoden, wie etwa die virale Therapie gegen Krebs, werde sich das Gesundheitssystem völlig verändern, urteilt Khayat. Neue wissenschaftliche Fachbereiche sind schon dabei, deutlich preiswertere Werkzeuge zu entwickeln, - für ein Gesundheitssystem, das schon lange keine Geld mehr hat.

##Viele medizinische Berufe könnten durch bessere Methoden ersetzt werden

>Am Ende werden auch hochpräzise Tätigkeiten wie Operationen ganz verschwinden. Viele Kardiologen sind heute schon aus dem Geschäft, weil wir viel bessere Methoden entwickelt haben. Seien es Medikamente oder minimal invasive Prozesse. Das macht Kardiologen dann arbeitslos. Wir werden zurück sehen und uns dann fragen: Haben wir tatsächlich Menschen aufgeschnitten? Um einen Tumor zu entfernen? Wirklich? (***Zayna Khayat***)

Viele Diagnosen, die früher Spezialisten vorbehalten waren, werden künftig von Computern übernommen, urteilt Khayat. Dadurch werden Diagnosen schneller, besser und deutlich billiger.

>Die künstliche Intelligenz ermöglicht es, Behandlungsrisiken aus Datenströmen intelligent zu analysieren. Bisher war die Gesundheitsdiagnose beschränkt auf einen wenn auch sehr gut ausgebildeten Gesundheitsexperten. Künstliche Intelligenz beschleunigt diesen Prozess um ein Vielfaches. Wir werden uns erinnern, wie wir früher diagnostiziert oder behandelt haben. Und über uns lachen. Es ist sehr archaisch was wir tun. Bald werden 7,4 Milliarden Datensätze über künstliche Intelligenz Aufschluss geben, wie eine Krankheit behandelt werden sollte. Das wird unsere Gesundheitsversorgung völlig verändern. (***Zayna Khayat***)

##Patientinnen sollen über ihre Daten selbst entscheiden dürfen 

Fragen nach dem Datenschutz schiebt Khayat einfach beiseite. Wer krank sei, wolle die bestmögliche Therapie. Künftig hätten die Patientinnen über neue digitale Werkzeuge selbst die Kontrolle darüber, wer ihre Daten einsehen kann und welche Diagnosemöglichkeiten sie nutzen wollen. Die Medizintechnik entwickle sich derzeit mit exponentieller Geschwindigkeit weiter. Das Problem sei weniger der Datenschutz sondern veraltete Regularien und Geschäftsmodelle, die mit den neuen Anwendungen nicht Schritt halten. Wie in Kanada entwickelt Khayat deshalb auch für Holland ein neues System, mit dem Gesundheitsinnovationen beschleunigt werden können. Damit bessere Behandlungsmethoden schneller zu den Patientinnen kommen.

>Ein gutes Beispiel ist ein Rehabilitationskrankenhaus in Toronto. Sie haben sich angesehen, wie Patienten auf Schlafapnoe getestet werden. Es ist ein riesengroßes Problem, weil Menschen mit Übergewicht oft kurze Phasen mit Atemstillstand beim Schlafen haben. Bisher wird Schlafapnoe im Schlaflabor getestet. Sie werden eine Nacht an Drähte angeschlossen in einer sehr künstliche Umgebung. Dabei sollen sie eine Nacht schlafen, so als ob alles normal wäre. Das ganze kostet bis zu 3000 Dollar. Ausserdem müssen sie rund 18 Monate warten, bis sie dran sind. Es ist viel zu teuer und umständlich. Deshalb können nicht viele Menschen getestet werden. Viele wollen es ganz vermeiden, weil es eine so furchtbare Erfahrung für Patienten ist. (***Zayna Khayat***)

##Widerstände gegen Innovationen hemmen die Entwicklung 

Um das ganze Verfahren zu erleichtern und für Patientinnen angenehmer zu machen, hat die Firma Breathotec nun eine Maske für zuhause entwickelt, die bei der Diagnose von kurzem Atemstillstand während des Schlafens hilft. 

>Ein guter Vergleich ist der Schwangerschaftstest von Frauen mit einem kleinen Streifen aus der Apotheke. Im Unterschied dazu musste man früher zum Gynäkologen gehen und die Beine breit machen. So viel zur Demokratisierung dieses ganzen Verfahrens zu einem Bruchteil der Kosten. Es ist viel zugänglicher und besser für die Patientin. Und noch viel wichtiger: Sie können mehrere Tests hintereinander machen. Der Vergleich von Schlaflabor und Schlafmaske fällt ähnlich aus. Im Schlaflabor haben sie eine Nacht, um einen Schlaftest zu machen. Alle Daten basieren auf dieser einen Nacht, auch wenn sie aus irgend einem Grund diese Nacht mehr auf dem Rücken schlafen, weil sie in so einer seltsamen Umgebung sind. (***Zayna Khayat***)

Die Ergebnisse mit den neuen Schlafmasken sind deutlich besser als Messungen in Schlaflaboren. Deshalb hat Khayat in Kanada dafür gesorgt, dass die neuen Masken schneller zugelassen werden und überall zu kaufen sind.

>Wenn sie nun mit der Schlafmaske eine ganze Woche zuhause Daten sammeln, bekommen sie vielleicht eine ganz andere Diagnose als durch ein Schlaflabor. Das ist gut für die Patienten und sehr gut für das Gesundheitssystem, weil es viel billiger ist. Außerdem können damit viel mehr Patienten diagnostiziert werden. Aber natürlich ist es ganz schlecht für die Leute, die die Schlaflabore besitzen. Raten Sie mal, wer diese Schlaflabore besitzt in Kanada? Genau. Die meisten davon gehören Ärzten, die Schlafapnoe diagnostizieren. (***Zayna Khayat***)

Zu ihren wichtigsten Aufgaben gehöre es, sagt Khayat, Widerstände gegen solche  Innovationen aus dem Weg zu räumen. Den natürlich wollen die Vertreter veralteter Behandlungsmethoden Ihr Geschäft nicht so einfach aufgeben. 

##Digitalisierung forciert exponentiellen Wandel auch im Gesundheitssektor

>Krankenhäuser werden nicht mehr als Profitzentren betrachtet werden 80 Prozent der Leistungen, die heute Krankenhäuser erbringen, werden künftig von anderen Gesundheitspartner angeboten werden. Dadurch werden die Kosten sehr stark sinken. (***Zayna Khayat***)

Der Wandel sei nicht mehr aufzuhalten, ist die Gesundheitsexpertin überzeugt. Viele Patientinnen und Patienten haben schon begonnen, mit Hilfe digitaler Werkzeuge ihre Gesundheit selbst zu managen. Die Verantwortlichen seien gut beraten, sich rechtzeitig auf den Systemwandel einzustellen.

>Es wird ein neues System entstehen, dass sich am Patienten orientiert. Nicht, weil ich das gerne hätte, sondern weil es schon entsteht. Der Zug hat die Station verlassen. Die Frage ist, werden die Verantwortlichen zu spät auf den Zug aufspringen oder werden sie den Zug gleich zu Beginn selbst fahren. Ich hoffe, die politischen Führer werden etwas Neues aufbauen und nicht nur zusehen, während die bisherigen Geschäftsmodelle zerstören werden. (***Zayna Khayat***)

Künstliche Intelligenz, die Digitalisierung und Virtualisierung vieler Prozesse im Gesundheitsbereich, die Orientierung an Patienten statt an Systemstrukturen, Entwicklungen neuer Forschungsbereiche: Viele gleichzeitige Veränderungen werden den Wandel im Gesundheitsbereich mit exponentieller Geschwindigkeit vorantreiben. Zayna Khayat und andere Innovationsexperten versuchen Europa darauf vorzubereiten. Es sei höchste Zeit, die Weichen umzustellen. Für eine neue, am Patienten orientierte Medizin.

