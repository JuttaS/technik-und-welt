---
date: 2019-06-12 16:43 CEST
permalink: autonomes-fahren-und-kuenstliche-intelligenz
blog: podcast
title: Autonomes Fahren und künstliche Intelligenz
short_title: Von der Vision bis zur Umsetzung des autonomen Fahrens wird es noch lange dauern
tags:
  - Autonomes Fahren
  - Künstliche Intelligenz
  - KI
image: car-driving-in-desert.jpg
image_alt_title: Noch fahren autonome Autos nur auf abgelegenen Strecken
soundcloud_link:
lybsin_link: 10130165
published: true
author: Jutta Schwengsbier
introartikel: "Künstliche Intelligenz kann schon einparken: Aber wann und wo können Autos alleine fahren? Hören Sie Expertengespräche zur Zukunft des autonomen Fahrens."
---

>Der Begriff autonomes Fahren ist natürlich sehr weitläufig verwendet. Wir sprechen hier von fünf Stufen, um das voll autonome Fahrzeug zu haben. In den ersten zwei Stufen ist es ein assistieren des Fahrers. Bis hin zum Level 5, wo ich im Grunde genommen den Fahrer komplett ersetze. (***Hans Adlkofer Infineon***)

>Alle Szenarien, die wir im öffentlichen Straßenverkehr testen wollen, werden vorher im Simulator getestet. Da muss alles mal funktionieren. Und wir haben auch zwei Leute ständig im Fahrzeug. Der eine ist der Fahrer. Der schaut aber auch nur auf die Verkehrslage und greift ein, falls notwendig. Und der andere schaut eben am Bildschirm was unsere Software macht und kann dort auch vorzeitig eingreifen, falls etwas nicht stimmen sollte. (***Gabor Pongartz aiMotive***)

>Die großen Autobauer, die haben Fahrzeuge in USA und Fahrzeuge in Europa, die ihren eigenen Lernprozess haben. Das heißt, gewisse Grundalgorithmen sind natürlich gleich. Aber zum Beispiel mal eine einfache Situation: Die Ampel in USA ist immer auf der gegenüberliegenden Seite der Kreuzung. Bei uns ist ja immer rechts oben. So. Das heißt, von der Seite muss das Auto schon ganz anders die Ampelsituation lernen als dann vielleicht schon in China, wo die Ampel wieder anders konfiguriert ist.  Wir werden lernen müssen die verschiedenen Verkehrssituationen in den verschiedenen Kulturen.  Es klingt manchmal lustig. Aber es gehört zum Lernen dazu. (***Hans Adlkofer Infineon***)

>Es wird sicher nicht so laufen, dass plötzlich ein Durchbruch passiert und plötzlich alle Fahrzeuge überall auf dieser Welt autonom fahren können. Das muss schrittweise ausgerollt werden. Sowohl geografisch als auch im Sinne von der Komplexität womit das Fahrzeug umgehen kann. Und es wird sicher noch relativ viel Arbeit bedeuten. (***Gabor Pongartz aiMotive***)

>Ich könnte mir vorstellen, dass in den USA, da gibt es ja solche Bereiche wo ältere Menschen zusammenleben, dass dort ein autonomes Fahrzeug vielleicht nur bis 50 Stundenkilometer schon die Leute herumfahren kann und dort das Risiko sehr gering ist, dass dort etwas passieren kann. (***Hans Adlkofer Infineon***)

Das Wettrennen hat schon längst begonnen: Wem es gelingt, die ersten wirklich autonomen Fahrzeuge auf den Markt zu bringen, der wird die Zukunft des Automobilmarktes, und damit unser aller Leben, entscheidend prägen. Neben den traditionellen Autobauern sind auch Daten-Unternehmen wie Google oder Facebook mit am Start. Und, - natürlich -, der Elektromobil- Hersteller Tesla. Dessen Gründer Elon Musk betreibt mit Space X auch ein Raumfahrt-Unternehmen. Einige Beobachter beschreiben die Entwicklung von selbstfahrenden Autos tatsächlich auch als genau so herausfordernd wie eine Mars-Mission. Dass sie damit Recht haben könnten, zeigt die inzwischen auch von großen Premiummarken wie BMW und Daimler vereinbarte Kooperation. Beim Bau autonomer Fahrzeuge ist Expertise in den unterschiedlichsten Bereichen gefragt: Im Fahrzeugbau. Beim Einsatz künstlicher Intelligenz zur Analyse großer Datenmengen. Beim autonomen, Zentimeter genauen navigieren über vorgegebene Routen und große Strecken. Und - nicht zu vergessen - beim absichern der per internet vernetzten Fahrzeuge vor Hackerangriffen. Die Liste der Anforderungen liesse sich noch fortsetzen. Durch die Aufzählung ist aber sicher klar geworden: Der Weg von ersten Fahrer-Assistenzsystemen bis hin zum wirklich autonomen Fahrzeug ist noch weit.

##KI muss erst lernen mit Verkehrssituationen umzugehen

Künstliche Intelligenz kann schon einparken: Aber wann und wo können Autos wirklich alleine fahren? Finden Autos überhaupt Ihren Weg, wenn Strassenmarkierungen fehlen? Oder es schneit? Oder keine Internet- Verbindung da ist? Welche Sicherheitssysteme sollen künftig verhindern, dass Hacker zum Beispiel vernetzte Fahrzeuge per Fernsteuerung stehlen? Diese und weitere spannende Fragen habe ich einigen Experten von Unternehmen gestellt, die für verschiedene Autobauer die Systeme für autonome Fahrzeuge mit entwickeln: Mit dabei unter anderem der Halbleiterhersteller Infineon, der Mikro-Prozessoren und Sicherheitssysteme für Fahrzeuge entwickelt. AiMotive, einem  Spezialisten für die Entwicklung von künstlicher Intelligenz in autonomen Fahrzeugen. Sowie U-Blox, einem Spezialisten für die zentimetergenaue Positionierung von Fahrzeugen auf  Strassen aber zum Beispiel auch von Drohnen. Lassen Sie sich überraschen. Die Antworten geben Einblicke in einige der herausforderndsten technologischen Entwicklungen unseres Jahrhunderts.

Wo sind Hindernisse auf der Strasse? Wie schnell oder langsam kann ich im Verkehrsfluss fahren? Autonome Autos müssen zunächst einmal, genauso wie Menschen, ihre Umgebung erfassen, um dann aus den gesammelten Daten Entscheidungen treffen zu können. Dazu werden in der Regel gleichzeitig Digitalkameras, sowie Radar- und Lasersysteme genutzt, um neben dem Bild auch Abstand, Richtung und Geschwindigkeit von Objekten aller Art erfassen zu können. Die Firma aiMotive, die verschiedene Systeme für künstliche Intelligenz in autonomen Fahrzeugen entwickelt, demonstrierte auf der Messe „Embedded World“ in Nürnberg, wie das Ganze funktioniert. 

##Simulatoren helfen beim KI Training

Ein Monitor zeigt eine Autobahnfahrt  aus der Perspektive eines autonomen Fahrzeugs. Eine grün gestrichelte pulsierende Linie, die die Impulse von Lasersignalen symbolisiert, zeigt dabei zum Beispiel die freie Fläche rund um's autonome Auto. Zu sehen ist also kein Video, sondern eine rot, grün und organge leuchtende Computergrafik.

>Das ist eben dieser Grafik Motor, der die dreidimensionalen Bilder bereitstellt. Also quasi die Grafik generiert. In einem Computerspiel ist halt das Hauptziel, das für das menschliche Auge flüssig und schön darstellen zu können. In unserem Anwendungsfall ist es aber eher wichtig, dass die dargestellten 3D Umgebungen und 3D Objekte physikalisch richtig generiert werden, so wie es auch ein physikalisch richtiger Sensor sehen würde. (***Gabor Pongartz aiMotive***)

Nur dass dazu tatsächlich die Daten aus verschiedenen Sensoren per Computer zu einem dreidimensionalen Umgebungsbild zusammengesetz werden, erläutert Gabor Pongartz. Der 31jährige ist Produktmanager bei aiMotive. Die Monitorbilder, sagt der Elektroingenieur, stammen von einem seiner autonomen Testfahrzeuge. aiMotive läßt sie begleitet von je zwei menschlichen Sicherheitsfahrern täglich Hunderte Kilometer in verschiedenen Ländern  Europas und Amerikas herumfahren.

>Wir haben mehrere Kameras, die verschiedene Perspektiven haben, teilweise auch sehr stark überlappen, die jeweils andere Stärken haben und andere Objekte oder Informationen besser erkennen können . Und die wirkliche Kunst liegt dann darin, diese verschiedenartigen Informationen so zusammenzuführen, dass man eine robuste und sichere Darstellung der Umgebung hat. (***Gabor Pongartz aiMotive***)

Doch auch nach dem Kunststück, aus unzähligen Sensordaten eine dreidimensionale Computergrafik zu machen, ist es noch lange nicht getan. Um Fahrentscheidungen ableiten zu können, reicht es nicht, Hindernisse zu erkennen. Das autonome Fahrzeug muss wissen: Was steht auf einem Strassenschild? Wo sind die  Strassenmarkierungen? Ist eine Ampel rot oder grün? Jedes der generierten 3D Umgebungsbilder muss deshalb einzeln per künstlicher Intelligenz nach den gesuchten Objekten, - Schild, Strassenmarkierung, Ampel und vielen anderen - durchsucht werden. 

##Vom maschinellen Lernen zur künstlichen Intelligenz

Wie künstliche Intelligenz funktioniert? Das, was heute als künstliche Intelligenz bezeichnet wird, hieß früher einfach maschinelles Lernen. Ein Prozess, der so ähnlich abläuft wie der Lernprozess bei Kindern, erläutert Professor Klaus Robert Müller. Der Informatiker ist Lehrstuhlleiter für maschinelles Lernen an der TU Berlin und gilt als einer der führenden deutschen Experten für die Entwicklung von künstlicher Intelligenz. 

>Zeigt man einem Kind, dass hier heißt Stuhl und noch einen anderen Stuhl. Fragt das Kind. Ist das auch ein Stuhl? Nein. Ein Tisch. Das ist ein Lernen aus Beispielen. Kinder sind in der Lage ihr ganzes Weltwissen so einzubringen, dass sie besonders effizient aus sehr wenigen Daten lernen. Das maschinelle Lernen ist in der Regel nicht in der Lage aus ganz wenigen Daten dazu zu lernen. Sondern man braucht eigentlich immer mehr größere Datenmengen um komplexe Zusammenhänge zu verstehen oder die Maschine verstehen zu lassen. (***Klaus Robert Müller TU Berlin***)

Die Lernfähigkeit von Computern beruht im Wesentlichen also darauf, Ähnlichkeiten oder Unterschiede in Bildern oder anderen Daten erkennen zu können. 

>Einer der interessantesten Fortschritte bei künstlicher Intelligenz in den letzten Jahren war die Weiterentwicklung des sogenannten tiefen Lernens. Eines der prominentesten Beispiele war ein Algorithmus, der Katzen in Youtube-Videos identifizieren konnte. Dazu mußten Menschen diese Katzenvideos ansehen und darin jede Katze Frame für Frame markieren. Die Augen. Die Ohren. Alles was eine Katze ausmacht, musste als Kommentar an den Algorithmus übergeben werden. (***Joel Dudley***)

Joel Dudley, Spezialist für künstliche Intelligenz, interessiert sich nicht wirklich für Katzenvideos. An dem Beispiel lasse sich aber gut erklären, wie die Objekterkennung in Videos oder in Computergrafiken funktioniert: Dazu müssen zunächst Menschen Tausende von Bildern mit Anmerkungen markieren als Beispieldaten. Erst danach können Computer in Videos automatisch nach ähnlichen Objekten suchen. Künstliche Intelligenz ist also immer nur dann gut, wenn ausreichend viele Beispieldaten vorhanden sind. 

##Zum Training von autonomen Fahrzeugen fehlen noch viele Beispieldaten
Darin liegt eine der größten Herausforderungen bei der Entwicklung autonomer Fahrzeuge: Die notwendigen Trainingsdaten müssen erst einmal für alle denkbaren Szenarien vorhanden sein. Und Menschen müssen dazu zahllose gesuchte Objekte in diesen Trainingsvideos einzeln per Hand markieren und mit Anmerkungen für den Computer versehen. Das Problem dabei: Es gibt nahezu unbegrenzte Möglichkeiten, was alles passieren könnte, worauf ein autonomes Auto reagieren müßte. aiMotive hat deshallb einen Simulator entwickelt, um verschiedene Situationen durchspielen zu können .
>Das Fahren ist eine hochkomplexe Aufgabe. Es muss ja auch für den Mensch gelernt sein. Letzten Endes muss es dann auch für das Fahrzeug gelernt werden, falls das Fahrzeug dann selbst fahren soll. Die zweite Produktlinie von uns ist ein Simulator. Wir können Tausende Szenarien und Hunderte Kilometer jede Nacht mit jeder Software-Version fahren. (***Gabor Pongartz aiMotive***)

Wie Menschen, müssen autonome Fahrzeuge nicht nur lernen, mit der Verkehrssituation zurecht zu kommen, sondern auch mit unterschiedlichen Sichtverhältnissen oder Wetterbedingungen. Die Digitalkameras kommen dabei der menschlichen Fähigkeit des Sehens am nächsten. Visuelle Informationen alleine reichen aber nicht, weil sie durch zahlreiche Störfaktoren beeinflusst werden können. Wenn es regnet oder schneit, zu dunkel ist oder die Sonne blendet, dann würden autonome Autos, genauso wie Menschen, durch Fehleinschätzungen viele Unfälle verursachen. Die  zusätzlich eingebauten Radarsysteme können unbeeinflusst vom Wetter messen, wo und in welcher Entfernung Hindernisse sind. Und die Laser-Systeme können ausserdem per Lichtstrahlen Abstände und Geschwindigkeit messen. Im Simularor können alle denkbaren Möglichkeiten oder auch Katastrophenfälle durchgespielt werden, erläutert Pongartz. Damit das autonome Auto zum Beispiel auch lernt, langsamer zu fahren, wenn es regnet oder schneit. Oder wenn sich die Lichtverhältnisse oder die Verkehrsdichte ändert.

>Wie können simulieren was passieren würde, falls gewisse Sensoren während der Fahrt ausfallen würden oder aufgrund von Schnee nicht mehr die optimale Leistung bringen. Das alles wäre im Simulator möglich. Zusätzlicher Vorteil ist natürlich, dass man auch Szenarien testen kann, die man im realen Leben natürlich schwer testen kann. Entweder, weil sie sehr sehr selten vorkommen oder weil zum Beispiel eine gewisse Situation sehr sehr gefährlich ist. (***Gabor Pongartz aiMotive***)

##Fahrmöglichkeiten von autonomen Fahrzeugen sind noch sehr begrenzt

Nach all dem Training mit Testfahrzeugen und im Simulator: Wo können autonome Autos heute denn nun tatsächlich schon selbständig fahren? Die Antwort von Gabor Pongartz: Die Möglichkeit, Autos wirklich selbständig fahren zu lassen, ist bislang sehr beschränkt.

>Was wir aktuell anbieten  ist ein Autobahnen Pilot. Soll bedeuten, dass man von der Autobahnabfahrt bis zur Autobahnabfahrt auch mit Wechsel zwischen Autobahnstrecken auf vordefinierten Autobahn Netzen selbstständig zwischen A und B fahren kann. Das heißt, die Komplexität, die in so einem Autobahn Szenario vorkommen kann, können wir mittlerweilen bewältigen.  Die zweite Säule, wo wir aktuell Lösungen bieten können, ist das automatisierte parken. Darunter kann man sich in etwa vorstellen, sie stellen das Fahrzeug beim Parkhaus direkt bei den Schranken ab. Das Fahrzeug wird sich dann selbstständig eine Parklücke im Parkhaus suchen.  Das sind dann natürlich Umgebungen, die von der Komplexität her eingeschränkt sind. Klarerweise ist das vollautomatisierte Fahren in einer unbegrenzten Umgebung deutlich komplexer. (***Gabor Pongartz aiMotive***)

Wie Gabor Pongartz  ist auch Hans Adlkofer überzeugt, dass der Weg zum autonomen Fahrzeug noch weit ist. Adlkofer ist Vizepräsident des Halbleiterherstellers Infineon und dort verantwortlich für den ganzen Automobilbereich.

>Die Autobahn ist natürlich sehr klar strukturiert. Alle fahren in derselben Richtung. Typisch nicht sehr viel Tiere oder Kinderwägen oder Fahrradfahrer auf der Autobahn. Das heißt aber, relativ überschaubar. Das Spektrum von Situationen dagegen eine Innenstadt, da habe ich so viele Szenarien, so viel Themen. Es wird noch eine gewisse Zeit dauern, bis ich dort in diesem für ein Computersystem chaotischen Verkehr, alle diese Lernphasen durch habe. (***Hans Adlkofer Infineon***)

##Fahrzeuge müssen für's autonome Fahren komplett neu konstruiert werden

Neben der Entwicklung von künstlicher Intelligenz muss zum autonomen steuern von Fahrzeugen das Auto an sich, völlig neu gedacht werden. Menschen sollen ja als Kontrollinstanz irgendwann weg fallen. Wie kann dann verhindert werden, dass Unfälle passieren, wenn Sensoren oder andere Steuerungselemente ausfallen? Wie können Hacker davon abgehalten werden, autonome Fahrzeuge per Fernsteuerung zu stehlen? 

>Wir erleben hier nichts Neues, was die technische Hackerszene angeht, sondern wir werden einfach das, was sie bei anderen Industrien schon gemacht haben, auch auf die Autoindustrie anwenden. Das heißt, ist es realistisch, dass ein Auto angegriffen werden kann? Wir haben sehr viele Schnittstellen. Die Schnittstelle ist Bluetooth haben wir einmal im Fahrzeug. Auch gewisse Stecker, wo ich mich damit verbinden kann. Das heißt da ist ein Einfallstor gegeben. Deswegen sind Systeme, die noch nicht mit einer Security Architektur behaftet sind, natürlich leichter anzugreifen als Leute, die sich jetzt intensiv damit beschäftigt haben. Die Autoindustrie beschäftigt sich sehr stark damit, um dieses Systeme sicher zu machen. Weil sie wollen auf der einen Seite die Konnektivität den Kunden bieten. Auf der anderen Seite müssen Sie diese ganzen Einfallstore schließen. (***Hans Adlkofer Infineon***)

