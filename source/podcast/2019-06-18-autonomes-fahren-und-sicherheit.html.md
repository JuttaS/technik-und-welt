---
date: 2019-06-18 10:21 CEST
permalink: autonomes-fahren-und-sicherheit
blog: podcast
title: Autonomes Fahren und Sicherheit
short_title: Autonomes Fahren und Sicherheit
tags:
  - Autonomes Fahren
  - Sicherheit
  - IT
image: lightning-on-street.jpg
image_alt_title: "Vorsicht vor Hackern ist geboten (Photo von Alex Iby auf Unsplash)"
soundcloud_link:
lybsin_link: 10191701
published: true
author: Jutta Schwengsbier
introartikel: "Autonomes Fahren erfordert ganz neue Sicherheitskonzepte: Eine Redundanz aller Systeme wie im Flugzeugbau und eine neue IT-Architektur gegen Hacker Angriffe."
---

>Das Auto in der Vergangenheit war ja eher ein geschlossenes System. Das hat auch die Autoindustrie dadurch geschützt, dass eben nur gewisse Schnittstellen vorhanden waren. Jetzt, wo wir von einem „Connected Car“ sprechen, das heißt von einem Auto, das sich im Internet befindet, das mit all diesen neuen Medien verbunden ist, habe ich natürlich auch die gleiche Herausforderung wie im Computerbereich, dass solche Systeme angegriffen werden. (***Hans Adlkofer Infineon***)

>Dann hängt es natürlich auch sehr stark davon ab, was die Intention eines Hackers ist. Will ich nur ein Auto entwenden oder stehlen? Oder will ich das Auto manipulieren? Oder will ich gar noch mehr machen, das ich einen ganzen Konzern bedrohe, indem ich halt versuche, seine Sicherheitsphilosophy zu brechen?  (***Hans Adlkofer Infineon***)

>Es dürfen nur gewisse Autoritäten im Fahrzeug Bremsmanöver ausführen. Beim selbstfahrenden Autos ist es auch so, dass nur gewisse Einheiten das machen dürfen. Warum soll ein Infotainment System, das heißt ein Radio, eine Lenkung beeinflussen können, wenn ich zum Beispiel den Trojaner ins Radio rein setze. Das heißt, es kommt von der Lenkung selbst. Ich kann relativ klare Zugriffsrechte, ich kann ganz klar System - Hierarchien aufbauen, das sehr viele Möglichkeiten ausschließt. (***Hans Adlkofer Infineon***)

##Autonome Fahrzeuge müssen ganz neu konstruiert werden

Seit immer mehr Fahrzeuge per Mobilfunk und Internet vernetzt werden, sind ganz neue Sicherheitsrisiken entstanden. Je mehr Möglichkeiten es gibt, die Steuerung von aussen zu erreichen, um so angreifbarer wird das ganze System. Unsere modernen, vernetzten Autos sind schon heute eines der beliebtesten Angriffsziele von Hackern. Wenn künftig nicht mehr Menschen sondern Computer das Auto steuern, wer hindert dann Hacker daran, ganze Autokolonnen per Fernsteuerung zu stehlen? Oder alle Fahrzeuge einer Modellreihe gleichzeitig stehen bleiben zu lassen, um die Hersteller mit Lösegeldforderungen zu erpressen? Die Autoindustrie hat die Gefahr längst erkannt. Und ist dabei, das System Auto von Grund auf neu zu konzipieren. Um Angriffe von aussen abzuwehren zu können. Aber auch, um die Fahrzeuge sicherer zu machen, wenn künftig keine Menschen mehr am Steuer sitzen. 

Wenn künftig keine Menschen sondern nur noch Computer Autos steuern, müssen die Systeme ganz anders aufgebaut werden, erläutert Christoph Unterreiner. Der Systemingenieur des Halbleiterherstellers Infineon erläutert das neue Sicherheitskonzept mit Hilfe eines Versuchsaufbaus. 

>Hier haben wir eine elektrische Steuerung. Das sind acht verschiedene Bauteile und wir haben sie zusammen in einem Board aufgebaut und in ein mechanisches System integriert. Damit steuern wir den Motor an. Wenn ein Teil dieses Board ausfällt, dann geht die andere Seite trotzdem noch und man kann gezielt sozusagen mit dem Auto weiterfahren. (***Christoph Unterreiner Infineon***)

##Redundanz aller Systeme ist Pflicht bei autonomen Fahrzeugen

Die elektronische Steuerung ist in einem durchsichtigen Plastikkubus montiert. An der Vorderseite ragt ein Lenkrad heraus. Oben ist ein großer, roter Knopf angebracht. Im Normalzustand läuft die Lenkung sehr leichtgängig. Sobald man oben den Knopf drückt, muss man mit großer Kraft kurbeln, um noch um die Kurve zu kommen. Das soll den Ausfall einer Servolenkung simulieren. Vorausgesetzt, am Steuer sitzt noch ein Mensch, der überhaupt mit großer Kraft kurbeln könnte. Bei autonomen Fahrzeugen müssen sämtliche Systeme redundant aufgebaut sein, erläutert Christoph Unterreiner. Ein Ausfall von Steuerungseinheiten muss jederzeit von den verbauten Zwillingskomponenten kompensiert werden können.

>Zukünftige Systeme sind sozusagen hoch zuverlässig. Das heißt, wenn ein Teil ausfällt, ist es eigentlich nur kurz weg. Sie sehen, es geht ein bisschen schwerer kurzzeitig, aber sie können trotzdem noch weiterfahren. (***Christoph Unterreiner Infineon***)

Dazu müssen nicht nur alle Steuerungseinheiten im Fahrzeug doppelt ausgelegt werden. Im Prinzip, sagt Hans Adlkofer, muss die Automobilindustrie die gleichen Sicherheitsmechanismen einführen wie etwa die Luftfahrtbranche. Der Sicherheitsexperte ist Vizepräsident des Halbleiterherstellers Infineon und dort verantwortlich für Systementwicklungen im Automobilbereich. 

>Im Grunde genommen geht es ja darum, beim autonomen Fahren, dass ich viele Signale habe, die vergleiche und zur selben Entscheidung komme, wie im Flugzeug. Da mache ich auch immer drei Systeme und dann mache ich eine Entscheidung. Aus drei Systemen müssen zwei das gleiche Ergebnis haben. Das heißt, ein System kann ausfallen und das Flugzeug kann immer noch genauso weiterfliegen. Das Gleiche werden wir im Auto haben. (***Hans Adlkofer Infineon***)
##Hunderte Computer sind im autonomen Auto vernetzt 

Damit das funktioniert, werden moderne Autos nicht nur von einem - sondern gleich von Hunderten von Bordcomputern - gesteuert. Ob Sensoren, Lenkung, Bremsen oder das Infotainmentsystem: Die elektronischen Steuereinheiten sind über mehrere Netzwerke verbunden und kommunizieren miteinander. Dazu kommen noch Kommunikationsmöglichkeiten der Autosysteme mit der Aussenwelt: Bluetooth für drahtlose Geräteverbindungen. 4G-Internet-Hotspots oder bald auch 5G-internet-Verbindungen. Wi-Fi. USB-Schnittstellen. Der Komfort, den Verbrauchen zuhause gewohnt sind, darf auch in ihrem liebsten Spielzeug, dem Auto, nicht fehlen. 

>Heute ist es relativ einfach. Wir als Fahrer sind die höchste Instanz. Wir können alles verändern. Wir können alles manipulieren. In Zukunft, wenn eine Schnittstelle da ist, gibt's eben noch eine dritte Person, die etwas machen kann. Und da muss ich ganz klare Zugriffsrechte inhaltlich, personenbezogen aufstellen, dass das kontrolliert werden kann. (***Hans Adlkofer Infineon***)

##Sicherheit autonomer Fahrzeuge muss neu gedacht werden

Solche Sicherheitskonzepte sind keineswegs selbstverständlich, urteilt Hans Adlkofer. Vor wenigen Jahren war die Automobilbranche realtiv blauäugig. Noch im Jahr 2015 hatten Sicherheitsforscher eine beängstigende neue Realität aufgezeigt: Es war ihnen gelungen, aus großer Entfernung die völlige Kontrolle über ein Auto per Fernsteuerung zu übernehmen. Inklusive Bremsen und Lenkung. Als Folge musste zum Beispiel Fiat Chrysler einen formellen Rückruf von rund eineinhalb Millionen Fahrzeugen durchführen. "Um die Art der Fernmanipulation zu verhindern, die von den Forschern demonstriert wurde", wie es in der offiziellen Stellungnahme des Autobauers dann hieß. 

Sicherheitsforscher selbst kamen zu dem Schluss, dass über die eingebauten Schnittstellen zur Fernwartung, über Zugangsmöglichkeiten per CD-Player, über Bluetooth oder Mobilfunk, Hacker Zugang zu den lebenswichtigen Steuersystemen erhalten könnten. Also über sämtliche Kommunikationssysteme, mit denen moderne Fahrzeuge inzwischen ausgestattet sind. Karsten Dietrich leitet das Sicherheitsforschungs- und Entwicklungslabor des Computerkonzerns IBM in Kassel. 

>Ich glaube eines der größeren Probleme in dem Bereich ist, dass im Auto Infotainment Systeme mit der Fahrzeugelektronik zumindestens am Anfang stark verknüpft waren. Solche Infotainment Systeme aber viel einfacher angreifbar sind. Das geht bis hin, ich glaube das Fraunhofer-Institut hat da entsprechende Experimente gemacht, dass man durch manipulierte MP3-Files Zugriff oder Kontrolle über so ein Infotainment System gelangen bekommen kann und daher Kontrolle über das Navigationssystem, bis hin zur Steuerung von ABS oder ähnlichen Schutzsystemen im Auto. (***Karsten Dietrich IBM***)

##Transportbranche ist eine der Hauptangriffsziele von Hackern

Der jüngste Sicherheitsbericht des Computerkonzerns IBM kommt zu dem Schluss, dass die Transportbranche im Jahr 2018 mit 13 Prozent der gesamten Angriffe und Vorfälle der am zweithäufigsten angegriffene Sektor ist,- nur noch getoppt von der Bankenbranche.

Was also tun? Die Automobilbranche muss sich künftig, genauso wie auch schon Computer- und Internet-Firmen, verstärkt gegen Angriffe wappnen, urteilt Hans Adlkofer, Vizepräsident von Infineon. 

>Wir erleben hier nichts Neues, was die technische Hackerszene angeht, sondern wir werden einfach das, was sie bei anderen Industrien schon gemacht haben, auch auf die Autoindustrie anwenden. Ist es realistisch, dass ein Auto angegriffen werden kann? Wir haben sehr viele Schnittstellen. Da ist ein Einfallstor gegeben. Deswegen sind Systeme, die noch nicht mit einer Security Architektur behaftet sind, natürlich leichter anzugreifen als Leute, die sich jetzt intensiv damit beschäftigt haben. Die Autoindustrie beschäftigt sich sehr stark damit, um diese Systeme sicher zu machen, weil sie wollen auf der einen Seite die Konnektivität den Kunden bieten. Auf der anderen Seite müssen Sie diese ganzen Einfallstore schließen. (***Hans Adlkofer Infineon***)

Hans Adlkofer hat vor seiner Mitarbeit bei Infineon unter anderem für den Münchner Konzern Gieseke und Devrient Sicherheitslösungen für den mobilen Zahlungsverkehr entwickelt. Erfahrungen aus solchen Bereichen müßten nun auch im Automobilbereich angewendet werden.

>Für uns wird es immer wichtiger, dass wir verstehen, wo unsere Halbleiter eingesetzt werden. Und im Fahrzeug, welche Funktionen, welche Herausforderungen dort sind. Das heißt, wir beschäftigen uns mit den Systemen, auch wenn wir keine Systeme verkaufen. (***Hans Adlkofer Infineon***)

##Sicherheitskonzepte aus der IT-Industrie müssen auch im Fahrzeugbau gelten

In sicherheitskritischen Bereichen ist schon lange eine sogenannte Zwei-Faktor-Authentifizierung üblich. Wenn jemand am Bankautomaten Geld abheben will, muss er zum Beispiel immer eine Bankkarte dabei haben und zudem noch seine geheime PIN Nummer eingeben. Wie wichtig solche Sicherheitsmechanismen auch im Internet der Dinge sind, wenn immer mehr Geräte, Maschinen und auch Autos mit dem Internet vernetzt werden, war zu Beginn nicht jedem klar. Einige Hersteller hatten etwa den gleichen Zugangscode für alle ihre Geräte festgeschrieben. Ein Verfahren, das ungefähr so sicher ist wie ein Bankautomat mit einer allgemeingültigen PIN Nummer, die auch noch für alle gut lesbar vorne angebracht ist. Solche Anfängerfehler werden inzwischen zwar seltener gemacht. Sicherheitsrisiken gelten aber bis heute als größtes Problem im Internet der Dinge. Die Autoindustrie ist dabei, ihre Konstruktionspläne komplett neu zu durchdenken. Um die neuen Sicherheitsrisiken von vernetzten, irgendwann autonomen Fahrzeugen so weit wie möglich zu reduzieren, sagt Hans Adlkofer.

>Man spricht jetzt nicht von einer Lösung sondern man muss entsprechende Architekturen, die leider unterschiedlich bei jedem Autohersteller sind, die richtige Lösung finden. Und das ist, wo wir beraten mit bewährten Lösungen aus der Security Industrie. Können wir das transferieren in das Auto?  (***Hans Adlkofer Infineon***)

Dazu hat Infineon zum Beispiel ein Hardware System entwickelt, in dem Sicherheitsschlüssel im Auto gespeichert werden können. So wie auch in Bankkarten, um beim schon erwähnten Beispiel zu bleiben. Florian Schreiner ist einer der Sicherheitsarchitekten bei Infineon für den Automobilbereich. Seine Aufgabe: Kommunikationssysteme in Fahrzeugen gegen unberechtigte Zugiffe absichern. 

>Wir stellen her den TPM Chip. Er ist ein Sicherheitsanker im Fahrzeug. Er ist dazu da, dass er Schlüssel schützt. Also alle Schlüssel, die genutzt werden für Verschlüsselung oder fürs signieren, werden im TPM Chip gespeichert und werden dort verarbeitet, um zum Beispiel ein Infotainment System abzusichern. Das ist aber genauso auch anwendbar auf Telemetrieeinheiten oder Gateways, die praktisch nach außen kommunizieren. (***Florian Schreiner Infineon***)

##Vertrauensmanagemant mit Software Schlüsseln

TPM heißt auf englisch Trustet Plattform Management. Oder auf deutsch: Vertrauensmanagement. Damit keine Unbefugten Schadcode ins Steuerungssystem einschleusen können, muss also jeder, der neue Software aufspielen will, den dafür im Sicherheits-Chip hinterlegten Code kennen, erläutert Florian Schreiner. Über diese per Zufallsgenerator erstellten Software-Schlüssel können Hersteller kontrollieren, dass nur Berechtigte Software aktualisieren können.

>Er führt hier dann die Operation für eine neue Firmware-Version aus und es wird jetzt hier praktisch ein kryptographisches Ticket ausgestellt, das dem Fahrzeug auch erlaubt, die neue Firmware zu installieren. Den Prozess sehen sie gerade hier. Sobald der Neustart durchgeführt ist, wird dann praktisch beim Neustart die Software verifiziert und authentisiert. Und wenn die in Ordnung ist, dann startet wieder das Infotainment System. (***Florian Schreiner Infineon***)

Es gibt also keinen Generalschlüssel. Sondern für jedes Fahrzeug, und für jeden Kommunikationskanal, werden individuelle, geheime Zugangscodes generiert.  

>Der TPM Chip, der arbeitet eigentlich immer, soweit es geht,  mit individuellen Schlüsseln, weil das auch die größte Sécurité bietet. Und dadurch, dass man individuelle Schlüssel hat, das heißt, auch wenn ein Angreifer versucht, das Auto zu manipulieren, das nur dieser eine Schlüssel betroffen ist. Dann gibt das System eine Fehlermeldung aus und sagt, irgendwas passt da nicht. Bitte in die nächste Werkstatt fahren, um festzustellen, was da der Fehler ist. (***Florian Schreiner Infineon***)
Die Verschlüsselung aller Kommunikationskanäle reiche aber nicht, urteilt Hans Adlkofer. Auch falls Angreifer die Zugangscodes knacken können, muss das Fahrzeug noch sicher sein.

>Ist ein Externer überhaupt erlaubt, zum Beispiel, in die Lenkung dort einzugreifen. Es gibt da eine gewisse Logik. Lenkung muss immer im Fahrzeug passieren. Das heißt, ich kann dann die nächste Stufe generieren, wo überprüft wird, ob der Inhalt, der da reinkommt, überhaupt logisch ist oder sinnvoll ist. (***Hans Adlkofer Infineon***)

Das bedeutet, ein Fahrzeug per Fernsteuerung zu lenken oder abzubremsen, wie es Sicherheitsforschern vor einigen Jahren noch gelungen ist, dürfe künftig einfach nicht mehr möglich sein, sagt Hans Adlkofer. Bestimmte Steuerungssysteme müssen dazu prinzipiell vor einem Zugriff von aussen geschützt sein.

>Da werden die neuen Fahrzeuge mit viel höherem Sicherheitsniveau inzwischen ausgestattet. Man muss natürlich sehen, die Hacker werden intelligenter. Die versuchen natürlich auch zu lernen, wie sie dann auch diese neuen Systeme angreifen können. Das ist einfach ein Wettrennen. Die schlechte Nachricht ist: Dieses Wettrennen wird nie aufhören. Man muss immer ein oder zwei Schritte den Hackern voraus sein. (***Hans Adlkofer Infineon***)




