---
date: 2019-06-25 19:14 CEST
permalink: autonomes-fahren-und-navigation
blog: podcast
title: Autonomes Fahren und Navigation
short_title: Autonomes Fahren und Navigation
tags:
 - Autonomes Fahren
 - Navigation
image: satellite-navigation-system.jpg
image_alt_title: Satelliten Navigation ermöglicht autonomes Fahren (Photo von der NASA auf Unsplash)
soundcloud_link:
lybsin_link: 10285556
published: true
author: Jutta Schwengsbier
introartikel: "Autonome Fahrzeuge navigieren per KI und Satellitensignalen. Anders als beim Tracking von Gütern muss die Ortsbestimmung nicht nur Meter genau, sondern Centimeter genau sein."
---

>Das werden nicht nur das GPS-Signal sein, das uns sagt, wo ich denn fahren soll. Ein GPS-Signal zeigt mir keine Baustelle an. Ein GPS-Signal zeigt mir keine Ampel an. Das heißt, es schickt mir nur eine genaue Positionierung. Ich muss immer noch die Ampel selbst erkennen. (***Hans Adlkofer Infineon***)

>Sie können sich vorstellen, dass sie Kameras oder so was dann dazu im Auto haben. Es basiert alles auf einer relativen Information, wo ich mich gerade befinde. Und jetzt kommt dieser globale Navigationssatellitensystembereich dazu. Das heisst, ich möchte natürlich auch wissen, wo befinde ich mich absolut. Damit ich das in irgendeiner Weise mit einer Karte, die ich innerhalb meines Autos gespeichert habe, auch abgleichen kann. (***Sven Etzold U-Blox***)

>Das heisst die Karten sind eigentlich das Entscheidender, dass ich sagen kann auf der Karte, ich bin genau an dem Punkt. Das heißt, ich bin in der Mitte von der Straße und nicht nur, ich bin irgendwo auf der Straße. Deswegen ist eine hohe Genauigkeit natürlich essenziell. Aber für mich fast noch wichtiger sind hoch genaue Karten. Deswegen haben ja auch die Autofirmen hier zum Beispiel investiert. (***Hans Adlkofer Infineon***)

Sie hören den dritten Teil einer Podcast Serie zum autonomen Fahren. Im ersten Teil ging es darum, wie autonome Autos durch künstliche Intelligenz inzwischen andere Fahrzeuge, Verkehrszeichen oder auch Fußgänger erkennen und entsprechend reagieren können. Teil 2 hat sich mit der Sicherheit von autonomen Fahrzeugen beschäftigt. Dazu müssen die Baupläne bis ins  Detail neu konzipiert werden, um eventuelle Systemausfälle auffangen und Angriffe durch Hacker abfangen zu können. Im dritten Teil geht es nun um die Navigation von autonomen Fahrzeugen. Ein Problem, das genauso komplex ist, wie die Navigation in der Raumfahrt. Wie können autonome Fahrzeuge Centimeter genau navigieren und ihren Weg finden, auch wenn es keine Straßenmarkierungen gibt? Sie hören Antworten von Hans Adlkofer, Vizepräsident von Infineon und dort verantwortlich für den Automobilbereich. Und von Sven Etzold von U-Blox, einem der marktführenden Unternehmen bei der Entwicklung von Modulen und Microchips für die Satellitennavigation.

##Die Neuvermessung der Welt

Inzwischen kann fast jeder mit seinem Smartphone oder seiner Smartwatch seine Positionsdaten abfragen und dann in einer fremden Stadt oder auch in ländlichen Gebieten herausfinden, wo ungefähr der eigene Standort ist und wie weit entfernt die gesuchte Sehendwürdigkeit oder einfach nur das eigene Hotel ist. Möglich macht das ein globales Navigationssatellitensystem. Ein Satelliten-Empfänger kann über die von mehreren Satelliten ausgestrahlten Signale die eigene Position und auch die Geschwindigkeit berechnen. Die GPS-Technologie, die ursprünlich vom US-Militär zur Steuerung von Waffen, Kriegsschiffen und Flugzeugen entwickelt wurde, ist inzwischen aus unserem Alltag kaum noch wegzudenken. Insbesondere seit das US-Militär im Jahr 2000 die künstliche Signalverschlechterung abgestellt hat und inzwischen relativ genaue Daten auch für die zivile Nutzung verfügbar sind. Aber reicht das schon für's autonome Fahren, wenn man relativ genaue Satellitennavigationsdaten hat?  Nicht ganz, sagt Sven Etzold, Marketingleiter bei U-Blox. Das Schweizer Unternehmen hat vor 20 Jahren angefangen, Module und Microchips für die Positionierung zu entwickeln.

>Es heißt immer so, bei den neuesten Generationen, ja, die Genauigkeit ist so Plus, Minus ein Meter. Sie können sich vorstellen, das macht schon relativ viel aus, ob sie einen Meter auf der Straße oder einen Meter neben der Straße fahren. (***Sven Etzold U-Blox***)

Anders als wir mit unserem Smartphone müssen autonome Fahrzeuge nicht Meter genau sondern Centimeter genau navigieren können. So präzise Daten liefern Satelliten alleine bislang aber nicht, erläutert  Sven Etzold.

>Es gibt, wenn sie eine Satelliten Signal bekommen und das wird vom Satelliten auf die Erde geschickt, dann haben sie verschiedene Schichten durch die das Satelliten Signal hindurch muss. Sie haben die Ionosphäre. Sie haben die Stratosphäre. Dieses Signal weicht immer ein bisschen ab. Das geht dann nicht genau gerade nach unten auf den Boden, sondern das weicht immer ein bisschen ab. (***Sven Etzold U-Blox***)

##Ohne Korrektur sind Satellitendaten unbrauchbar

Die von Satelliten ausgestrahlten Signale werden also innerhalb der Atmosphäre der Erde in verschiedenen Schichten abgelenkt. Dadurch werden sowohl die Geschwindigkeit als auch die Richtung der Signale beeinflusst. Und es werden Messungenauigkeiten von einigen Hundert Metern in den Satellitensignal-Empfängern hervorrufen. Die von U-Blox gebauten Module und Microchips werden deshalb neben diesen Signalen zusätzlich mit sogenannten Korrekturdaten gespeist, die die atmosphärische Ablenkung, je nach Satellit, wieder herausrechnen können.

>Es gibt sehr sehr gute Algorithmen. Je nachdem wenn ich weiss, mit welchem Satelliten ich mich verbunden habe, welche Korrektur-Daten denn nun hergenommen werden müssen um quasi dieses Satelliten Signal noch genauer zu machen. (***Sven Etzold U-Blox***)

Autonome Fahrzeuge müssen deshalb jederzeit mit dem Internet verbunden sein, um die Korrekturdaten für die Navigation empfangen zu können, erläutert Sven Etzold.
>Und diese Korrektur Daten ermöglichen dann eine Navigation bis auf Zentimetergenauigkeit.  Das ist eine Kombination. Eine Kombination  von GPS und Korrektur-Daten. D as Satelliten Signal braucht man immer. (***Sven Etzold U-Blox***)

##Internetzugang ist Vorraussetzung für Autonomes Fahren

Autonomes Fahren, ohne 100-prozentige Netzabdeckung für das internet, ist in vielen Regionen noch nicht möglich, weil die Korrekturdaten nicht empfangen werden können. Weder in der deutschen Provinz, wo derzeit nur sehr schlechte oder gar keine mobilen internet Verbindungen vorhanden sind, noch etwa in der Wüste Sahara. 

>In der Sahara, da ist dann die Frage, ob sie dementsprechend eine direkte Verbindung ins Internet hätten.  Sie brauchen wahrscheinlich irgendeine, 2G, 3G, 4G also irgendwo eine Mobilfunk Verbindung, mit der sie erst einmal eine Verbindung, eine Datenverbindung aufbauen können, um diese Daten auszutauschen. (***Sven Etzold U-Blox***)

Irgendwann könnten zwar geostationäre Satelliten die Aufgabe übernehmen auch   Korrekturdaten zu versenden. Und damit die derzeit notwendige Internetverbindung von autonomen Fahrzeugen überflüssig machen.  Doch so weit ist es noch nicht, sagt Sven Etzold.

>Einfach das System selber ist etwas anders dann aufgesetzt. Aber im Grunde genommen bekommen   sie dann einfach diese Korrektur Daten zugesandt und dann könnten sie sich auch in der Sahara zentimetergenau vermessen. Diese Korrekturdaten Satelliten, die sind leider noch nicht aktiv. Da müssen wir noch ein bisschen warten, bis das wirklich kommt.  (***Sven Etzold U-Blox***)

##Autonome Fahrzeuge müssen die Dritte Dimension erkennen können

Noch nicht ganz so weit ist auch die Entwicklung von Karten, die für autonomes Fahren geeignet sind. Denn im Gegensatz zu unseren bisherigen Strassenkarten müssen die Karten für autonomes Fahren den ganzen Raum abbilden, nicht nur die Fläche, erläutert Hans Adlkofer. Vizedirektor von Infineon.  

>Ich habe eine dritte Dimension. Nicht nur zwei Dimensionen. Indem ich halt weiß, wo die Straße ist, sondern auch gibt es irgendwelche Bäume, Hindernisse oder Schnee. Wie hoch ist die Genauigkeit? Habe ich zum Beispiel Waldwege drauf? Wie genau sind die Karten? Habe ich zum Beispiel auch Randsteine und sonstige genauen Details drauf? Heute geht es mir nur darum, dass ich auf der Straße fahre. Wenn ich wirklich autonom fahren will, sind solche Hindernisse, die dann vielleicht nicht so genau verzeichnet sind, kriegsentscheidend um etwas zu machen. (***Hans Adlkofer Infineon***)

Auch die Fähigkeit, in Zukunft Karten für Navigationssysteme schnell und sicher auf einen aktuellen Stand bringen zu können, sei mit entscheidend, um künftig autonom Fahren zu können, sagt Adlkofer. 

>Deswegen auch wieder Sicherheit, weil ich will diese upgedated haben, um sicherer zu fah ren. Aber dann muss natürlich auch sichergestellt sein, dass eben nur die richtige Firma Daten eins pielt und nicht irgendein Dritter. (***Hans Adlkofer Infineon***)

##Wirklich autonomes Fahren ist noch lange nicht in Sicht

Mein Resumee nach all den Experteninterview zum autonomen Fahren: Viele Entwicklungen, die autonomes Fahren ermöglichen sollen, sind noch nicht abgeschlossen. Die neu entwickelte Künstliche Intelligenz braucht sicher noch Abertausende von Trainingsstunden, um allen möglichen Verkehrssituationen gewachsen zu sein. Weder sind die neuen Fahrzeugmodelle schon so ausgereift, dass die funktionale Sicherheit ohne Menschen am Steuer gewährleistet ist, noch ist die Software für Autos bislang ausreichend abgesichert, um ernsthaften Hackerangriffen widerstehen zu können. Die Genauigkeit der Ortsbestimmung mit Hilfe von globalen Satellitennavigationssystemen  reicht zwar jetzt schon in den Centimeterbereich. Doch solange geostationären Satelliten nicht überall auch Korrekturdaten bereitstellen können, bleibt eine zuverläßige Internetverbindung für die autonomen Fahrzeuge Grundvoraussetzung. Gerade Deutschland, mit seinen Premiummarken im Automobilbereich, hat da immer noch die größten    Schwierigkeiten. Die Netzabdeckung gerade in ländlichen Regionen ist in Deutschland im internationalen Vergleich miserabel. Und die hochpräzisen Karten, die auch dreidimensionalerr Räume darstellen müßten, sind auch noch lange nicht fertig. 
Die Frage, ob es noch wenig Monate oder eher Jahre dauert, mit dem autonomen Fahren, ist damit für mich beantwortet. Es sei denn, Elon Musk kann zaubern wie Harry Potter, wird das mit der Einführung von autonomen Fahrzeugen zum Jahresende 2019 wohl nichts werden. Wetten werden angenommen. 

##Ein Ausblick für autonome Navigation der Zukunft

Welche Bereiche und Sparten von der Entwicklung für autonome Fahrzeuge profitieren werden, ist vorher immer schwer abzusehen, meint zumindest Sven Etzold von U-Blox. Immerhin: Es gibt es schon einige Beispiel, was mit der neuen Technolgien in Zukunft alles gemacht werden könnte. 
>Hochpräzises Farming in der Landwirtschaft. Dort sind wir fast schon in diesem autonomen Bereich, dass wir Landmaschinen haben, die dann wirklich auf Zentimeter Basis über ihre Felder fahren können. Wir sehen, dass zum Beispiel Drohnen immer mehr auch dazu eingesetzt werden, um zum Beispiel Windräder zu warten. Dort ist es natürlich wichtig, dass wenn so ein Windrad inspiziert wird, das ist nachvollziehbar und immer wieder genau an den gleichen Stellen, um auch Vergleichsmöglichkeiten zu haben. Und dort einfach Drohnen eingesetzt werden, die eine Kamera installiert haben, die dann wirklich gut zentimetergenau quasi, diese Windräder inspizieren, die Bilder nachher zusammengesetzt werden und dann dementsprechend die Auswertung gemacht wird. (***Sven Etzold U-Blox***)



