---
date: 2019-07-11 14:06 CEST
permalink: sprachsteuerung-geht-auch-offline
blog: podcast
title: Sprachsteuerung geht auch offline
short_title: Sprachsteuerung geht auch offline
tags:
  - Technik
  - Spracherkennung
image: voice-recognition.jpg
image_alt_title: Amazon Echo Lautsprecher hoeren oft mit
soundcloud_link:
lybsin_link: 10482401
published: true
author: Jutta Schwengsbier
introartikel: "Datenschützer warnen vor Sprachsteuerassistenten, die private Unterhaltungen aufzeichnen. Nun arbeiten Internet Unternehmen an offline Lösungen. Ist das nun besser?"
---

>Hier geht es genau um die Frage, wie funktioniert ein Sprach-Assistenzsystem und wie häufig schaltet es sich möglicherweise ein, weil das System möglicherweise die Sprache nicht richtig versteht und einen Befehl fälschlicherweise als Aktivierungsbefehl auslegt, der gar nicht gegeben wird. Und hier haben wir ja aus dieser Diskussion mit Alexa gehört, dass es sehr häufig passiert. Diese Häufigkeit von Fehler-Mitschnitten ist aber so jedenfalls nicht abgebildet in entsprechenden Vorgaben der Datenschutzbedingung. Die betroffenen Personen, die sich so etwas ins Wohnzimmer stellen, die tun das ja, indem sie in hohem Maße Vertrauen in diese Modelle hineinbringen. Und es ist insofern erforderlich, dass die Nutzer transparent über die Datenverarbeitung und deren Schwächen eben auch informiert werden.(***Johannes Caspar Datenschutzbeauftragter Hamburg***)

##Vom Smart Home zur Industrie: Sprachsteuerung ist allgegenwärtig

Vom „Licht anmachen“ im Smart Home bis zur Steuerung von Großgeräten in der Industrie:  Immer mehr Hersteller wollen ihre Geräte mit Sprachbefehl bedienen lassen. Bislang sind sie dabei nicht an den Sprachsteuer-Assistenten von großen Internet Anbietern wie Apple, Amazon, Microsoft oder Google vorbei gekommen. Was vielen nicht klar war: Die Sprachsteuerbefehle werden dabei in der Cloud, also in zentralen, webbasierten Speichern, mitgeschnitten, - und von den Mitarbeitern der Dienste abgehört. Zur Qualitätskontrolle der Assistenten, wie es heißt. Ein Alptraum für Datenschutzexperten.

„Hey Siri“ , „Hallo Alexa“, „ok google“ - „mach das Licht an“. Mit den allgegenwärtigen Sprachassistenten in Smartphones oder Haushaltsgeräten haben sich für Konstantin von Notz, Digitalpolitiker von Bündnis 90 / Die Grünen im Bundestag, die schlimmsten Vorahnungen bestätigt. Eine allgegenwärtige Überwachung selbst in privaten Räumen ist Alltag geworden.
>Alle Befürchtungen und Dystopien, die bewahrheiten sich nach und nach. Früher, wenn man gesagt hat, Mensch, da wird irgendwie vielleicht das Mikrofon einfach angeschaltet und mitgeschnitten, war das eine Verschwörungstheorie. Heute ist das bei ganz vielen Geräten einfach so und steht im Kleingedruckten der Menü-Anweisung. (***Konstantin von Notz, MdB Bündnis90/ Grüne, Datenschutzexperte***)

##Lauschen im Wohnzimmer auch durch staatliche Nachrichtendienste

Eine Praxis, die inzwischen von Politikern und Verbraucherschützern weltweit kritisiert wird, nachdem bekannt wurde, dass Mitarbeiter der Internetgiganten - oder auch staatliche Nachrichtendienste - die private Kommunikation im Wohnzimmer von Millionen von Menschen über Smartphones oder den „Echo“ genannten Lautsprecher von Amazon belauschen können. 
>Wir haben ja bei diesem System noch eine hohe Fehleranfälligkeit vor dem Hintergrund, dass die künstliche Intelligenz, die ja diese Systeme steuert, noch nicht so ausgefeilt ist, dass diese Systeme weitgehend beschwerdefrei funktionieren. Das ist offenkundig jetzt ein Problem. Darum geht es doch. Wir müssen den Kunden den Nutzern von diesen Systemen erklären und erkennbar machen wie hoch ein solches System Fehler anfällig ist und in welcher Weise man damit rechnen muss, dass sich ein solches System einschaltet, obwohl man es nicht möchte. Darüber brauchen wir mehr Informationen. Wir brauchen Transparenz. (***Johannes Caspar***)

Professor Johannes Caspar, der Hamburgische Beauftragte für Datenschutz und Informationsfreiheit, hat sich einen Namen gemacht als Kritiker der oft illegalen Praxis von Internetgrößen, ihre Nutzer bis ins kleinste Detail auszuforschen und Daten über Jeden und Jede im Internet anzulegen. Seiner Ansicht nach müssen keine neuen Gesetze erlassen werden, um das Ausforschen bis in Wohnzimmer zu beenden. Die bestehenden Gesetze müßten nur eingehalten und umgesetzt werden.
>Naja. Der Ruf nach dem Gesetzgeber ist natürlich schwierig. Hier wir haben ja erst einmal insgesamt eine europäische Regelung des Datenschutzes, die europaweit gilt und wir haben eigentlich auch eine durchaus fortschrittliche Datenschutz-Regelung und die sollte hier doch auch reichen, um dieser Technologie insoweit entgegen zu gehen, dass das Datenschutzkonform in den meisten Fällen erfolgt. Die Installation von solchen Sprach-Assistenzsystem. Und hier geht es im Endeffekt darum, die Vorgaben der Datenschutz-Grundverordnung umzusetzen. Und ich denke, wenn die dann auch entsprechend von Anbietern erkannt und umgesetzt werden, brauchen wir keine neuen Regelungen. (***Johannes Caspar***)

##“Datenschutzgesetze reichen aus.  Sie müssen nur angewendet werden.“

Dass in der Datenschutzgrundverordnung festgelegte Prinzip der Datensparsamkeit verbietet die massenweise Speicherung von Daten ohne Anlass. Das gilt auch für Sprachdaten. Dass Sprachsteuerung auch ganz anders geht, demonstriert etwa ein startup aus Paris. Die „Snips“ genannte Software hört nicht mit sondern bietet Sprachsteuerung auch ohne Internetanschluss an, erläutert Alexander Guy, Produktmanager   bei „Snips“.

>Snips läuft direkt auf ihren Geräten. Offline. Wir garantieren dadurch ihre Privatsphäre. Wir sammeln oder speichern keine Daten in der Cloud. Und das gibt  unseren Kunden das Vertrauen, dass Datenschutzvorschriften eingehalten werden. Und die Sicherheit, dass niemand zuhört. Alles geschieht direkt auf dem Gerät. (***Alexander Guy SNIPS***)

Anders als bei den großen Wettbewerbern kann niemand die Kommunikation der Nutzer mit ihren technischen Geräten mithören,- oder sie gar mitschneiden, speichern und analysieren, sagt Alexander Guy.

>Wenn sie Geld an Ihre Frau oder Ihre Mutter oder Ihr Familienmitglied überweisen möchten, dann will eine Bank sicher kein fremdes Unternehmen beauftragen, dass diese Daten bei sich in der cloud speichert. Alles soll auf dem Gerät bleiben, mit dem sie sprechen. Das unterscheidet Snips: Wir verwenden keinen zentralen Server sondern arbeiten völlig dezentral. (***Alexander Guy SNIPS***)

##“IoT Geräte sind nicht sicher – Geheimnisse sollte Ihnen niemand anvertrauen.“

Aber sollten Kreditkartendaten wirklich per Sprachbefehl auf Mobiltelefonen gespeichert werden können? Stéphan Nappo warnt ausrücklich davor, Geräten im Internet der Dinge Geheimnisse anzuvertrauen, ob mit oder ohne Sprachsteuerung.  Der Franzose gilt als einer der weltweit besten Cyber-Sicherheitsexperten. Seiner Erfahrung nach können IoT Geräte überhaupt nicht vor Hackerangriffen geschützt werden.

>Das Internet der Dinge müßte eigentlich besser Internet der Gefahren heissen. Im Schwarzmarkt des Netzes warten Kriminelle nur darauf, die verbundenen IoT Geräte im Netz mit Schadcode zu infizieren. Deshalb dürfen wir niemals das Management von Geheimnissen wie Bankdaten in IoT Geräten umsetzen. Sie wollen ja etwas für sich kaufen und nicht für den Hacker. Wir dürfen IoT Geräten niemals vertrauen. (***Stephan Nappo, CISO 2018***)

Zugangsdaten für die Bank oder andere Geheimnisse sollten nach Ansicht von Stéphan Nappo also generell nicht per Sprachbefehl eingegeben werden können. Ganz unabhängig von Datenschutzerwägungen. Weder direkt auf dem Mobiltelefon noch mit Alexa. Unkritisch wäre dagegen die reine Menüanweisung für Haushaltsgeräte. Jede Kaffeemaschine, jeder Kühlschrank, jeder Waschmaschine wird inzwischen mit Mikrocomputern gesteuert und verfügt über ausreichend Speicherkapazitäten, um die Spracherkennungssoftware „Snips“, die nur 70 Kilobytes groß ist, - darauf laufen zu lassen. Die Rechenleistung von Kleingeräten reiche inzwischen völlig aus, um damit Sprachassistenten anbieten zu können, sagt Alexander Guy. Auch ohne Cloud Anbindung.

Die automatische Spracherkennung übersetzt einen Sprachbefehl in Maschinensprache. Damit dann anschließend der Mikrocomputer die gewünschte Aktion ausführen kann. Meine Mikrowelle, mein Musikplayer, der Fernseher,  mein Auto, meine Waschmaschine bekommen alle eine Stimme. Diese Art der allgemeinen Navigation und Befehlskontrolle. Aber gesteuert durch natürliche Sprache. Mit dieser Waschmaschine hier demonstrieren wir einen einfachen Befehlsdialog. (***Alexander Guy SNIPS***)

„Hey Snips. Custom Wash. - Sorry I did not get it.“
„Hey Snips – Standard Waschgang einschalten. - Entschuldigung. Das habe ich nicht verstanden.“

Auch wenn die Sprachsteuerung noch nicht immer perfekt funktioniert: Alexander Guy ist überzeugt, dass künftig keiner mehr Dienste verwenden will, die Sprachbefehle  belauschen können.

##“Belauschen lassen will sich niemand“ 

>Jedes zentrale System hat Schwachstellen, die irgendwie ausgenutzt werden können. Bei einem Diebstahl sind dann gleich Millionen von Datensätzen weg. Stehlen wird deutlich schwieriger, wenn jemand jedes Gerät bei ihnen zuhause oder in einer Fabrik einzeln hacken müßte.  Ausserdem arbeiten wir in einer Offline-Umgebung ohne Internetanschluss. Da wird Hacking wirklich sehr knifflig. (***Alexander Guy SNIPS***)

Hey Snips. Mach das Licht aus. Nach meinem kurzen Befehl in ein Steuergerät, das wie ein Golfball aussieht, geht tatsächlich das Licht im ganzen Haus aus.  Aber was passiert, wenn ich sage, „Hey Snips, sperr die Tür auf?“ Kann dann auch ein Einbrecher von aussen mein Smart Home steuern, - ganz ohne die Systeme zu hacken? Und ohne Einbruchsspuren hereinkommen? 
>In der Theorie erkennt unser System mehrere Stimmen. Aber es steht im Moment nicht auf unserem Plan. Wenn also ein Einbrecher sagen würde, öffne das Haus, würde der Befehl ausgeführt. Also würden wir das eher nicht zu einem Befehl machen, der möglich ist (lacht). (***Alexander Guy SNIPS***)

Mit seinem System hat das kleine Startup Snips bewiesen, dass Sprachsteuerung auch ohne Internetanbindung möglich ist. Die Bedenken von vielen, ständig lauschende Geräte in ihrem Haus, in ihrem Industriebetrieb oder Ihrer Arztpraxis zu haben, wären damit deutlich reduziert. Auch die Großen im Internetgeschäft scheinen langsam die Zeichen der Zeit zu erkennen. Google entwickelt derzeit ein künstliches neuronales Netz, das in Zukunft Sprache nicht mehr auf seinen Servern, sondern direkt in den Endgeräten verarbeiten kann. Vorerst funktioniert das nur auf Pixel-Smartphones mit amerikanischem Englisch. Die Entwicklung für andere Sprachen und andere Endgeräte laufe noch. Auch die Sprachassististen von Apple und Microsoft können einige Befehlen inzwischen offline ausführen, - solange keine Interaktion mit dem Internet gewünscht wird, wie zum Beispiel eine Frage nach dem Wetter oder Informationen von Wikipedia. 

Die derzeit laufende Diskussion, ob Sprachsteuerung nun online oder offline erfolgen soll, sei aber nur ein kleiner Teil des Problems insgesamt, urteilt Konstantin von Notz. 

##“Privatsphäre darf keine Frage des Einkommens werden“

>Können sich nur die Menschen Privatsphäre leisten, die das dann auch bezahlen müssen? Menschen, die nicht so finanzstark sind, am Ende des Tages, mit ihrer Privatsphäre für Dienste bezahlen. Im Kern geht es am Ende darum, ob Menschen aufgrund ihrer eigenen Daten dann diskriminiert werden und eben einfach Nachteile erfahren. (***Konstantin von Notz***)

Es könne nicht sein, urteilt Konstantin von Notz, dass am Ende die gesammelten Daten darüber entscheiden, ob jemand eine günstige Krankenversicherung bekommt. Oder einen Job. Oder einen Studienplatz. 

>Wenn man jetzt daran denkt, wie viele Daten zukünftig erzeugt werden im Bildungsbereich, gerade in der Schulbildung, beim Einsatz von Technik, sehr individualisierten Lernprogrammen und so weiter. Um zu gucken, wen man wohin beruflich und Studiums technisch schickt, aber eben auch nicht wohin. Und am Ende des Tages muss der Gesetzgeber die klare Linie ziehen, dass nicht irgendein Algorithmus über das Lebensglück und die Wege von Menschen entscheidet, sondern das Individuum selbst. (***Konstantin von Notz***)

