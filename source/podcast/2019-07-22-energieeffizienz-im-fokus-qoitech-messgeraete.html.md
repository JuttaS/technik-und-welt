---
date: 2019-07-22 16:15 CEST
permalink: energieeffizienz-im-fokus-qoitech-messgeraete
blog: podcast
title: Energieeffizienz im Fokus Qoitech Messgeräte
short_title: "Energieeffizienz im Fokus - Qoitech Messgeräte"
tags:
 - IoT
 - Technologie
 - Batterie
 - Energieeffizienz
image: qoitech-messgeraet.jpg
image_alt_title: Analyse Software und Messgerät von Qoitech
soundcloud_link:
lybsin_link: 10610453
published: true
author: Jutta Schwengsbier
introartikel: "Bei Milliarden von Kleinstgeräten im Internet der Dinge müssen Entwickler von Anfang an auf die Batterielaufzeit achten. Sonst droht der schnelle Systemausfall."
---

> Ich bin Vanja Samuelsson, Ingenieurin, Forscherin und Gründerin von Qoitech. Die ursprüngliche Idee kommt von Sony. Wir haben viel im 5G Sektor geforscht. Jeder, der zum Beispiel im Internet der Dinge arbeitet, kennt die Herausforderung, Geräte zu entwickeln, die mit extrem niedrigem Strom laufen. Das ist eigentlich nichts Neues. Sony hat viel Wissen darüber gesammelt, wie man in so einer Entwicklungsumgebung arbeitet. In unserem Fall hatten viele Kunden und viele Partner nachgefragt, wie genau wir das machen. Dadurch haben wir bemerkt: Es gibt einen Markt für unser Wissen und unsere Werkzeuge. Wir wollen wirklich, dass die Batterielaufzeit für  Entwickler immer eine hohe Priorität hat und das Problem nicht erst angegangen wird, wenn es schon brennt. (***Vanja Samuelsson, Qoitech***) 

Das erklärte Ziel der Entwicklungswerkzeuge von Qoitech? Sicherzustellen, dass die Milliarden von Kleinstgeräten, die gerade für das Internet der Dinge und den Telekom Sektor entwickelt werden, auch energieeffizient arbeiten. Damit Geräte nicht schon nach einem Jahr ausgetauscht werden müssen, - obwohl die Batterie eigentlich 10 Jahre lang hätte halten sollen.

> Wir haben eine Software, die Sie auf ihren PC herunterladen können. Ihre Hardware schließen sie direkt an unser Gerät an, dem wir den Namen Otii ARC gegeben haben. (***Vanja Samuelsson, Qoitech***)

##Im Ruhezustand sollten Kleinstgeräte kaum Strom verbrauchen

Otii kommt aus dem Lateinischen, erläutert Vanja Samuelsson. Übersetzt bedeutet Otii so viel wie Frieden. Oder Ruhe. Darum geht es im übertragenen Sinne auch bei der Entwicklung von Geräten für das Internet der Dinge. Die meisten warten fast nur im Ruhezustand. Bis das Gerät dann irgendwann ein Signal empfängt, das kurzfristig den Schlafmodus beendet, die gewünschte Funktion ausführt, - und dann gleich wieder in Tiefschlaf verfällt. Ein paar Beispiele, die jeder kennt? Wenn über eine Funkstrecke das Licht an oder ausgemacht wird. Oder per Bluetooth-Signal Musik oder ein Film ausgewählt wird. Die eigentliche Aktion dauert nur ganz kurz. Den Rest der Zeit müssen die Geräte aber auf mögliche Steuerbefehle lauschen, damit sie bei Bedarf aktiv werden können. In diesem „Schlafmodus“ genannten Lausch-Zustand sollen die Geräte möglichst wenig Energie brauchen, damit die Batterie lange hält.  Wie das erreicht werden kann? Genau hier beginnt die eigentliche Geschichte. Diesmal mit einem tiefen Einblick in die Technikumgebung von IOT-Entwicklern. Im Fokus: Ein neues Werkzeug von Qoitech, das die Lebensdauer von Knopfzellen-Batterien in Kleinstgeräten verbessern soll.

> Traditionell würden Sie die Batterieoptimierung aus der Perspektive von vielleicht zwei Personen im Team durchführen, die in einem Labor alles machen. Normalerweise mit einem Multi-Meter. Das sind wirklich teure, aufwändige Geräte. Wenn ihr Teams vielleicht nur eines dieser Werkzeuge besitzt und überhaupt nur zwei Leute damit umgehen können, dann können sie keinen Entwicklungsprozesses steuern. Wir ergänzen also die  fortschrittlichen und sehr  umfangreichen Tools, die Sie benötigen, mit etwas preiswerterem, das sie auf den Tisch jedes Entwicklers legen können. Wenn sie ihre Software jetzt updaten, können sie sicher sein, dass sie nichts ruiniert haben. (***Vanja Samuelsson, Qoitech***)

Die Otii genannte Mess-Station hat die Form einer etwas größeren Butterd ose mit dicken Metallwänden. In edlem Design. An einem Ende ist ein Kabel samt Stromstecker. Am anderen Ende sind verschiedene Buchsen für Verbindungskabel. Die führen dann zu einem Laptop mit der Analyse Software auf der einen und zum IoT-Gerät auf der anderen Seite. 

> Angeschlossen wird ihre Hardware über die Batteriekontakte, wenn Sie ein Bluetooth-Gerät verwenden oder ein Laura-Gerät oder irgend ein anderes Gerät, das weniger als 5 Volt Eingangsspannung braucht. Über die Hauptkontakten können sie den Systempegel messen. Also den Strom, der fließt, und die Spannung. Die aufgezeichneten Fehlerprotokolle werden m it der Strom- und Spannungsmessungen synchronisiert. Dabei wird in einem Profil aufgezeichnet, was in Echtzeit passiert. Wenn ihr System hoch fährt werden Daten gesendet. Dann schaltet sich wieder der Schlafmodus an. Im Profil werden all diese Aktivitäten aufgezeichnet und sie können genau sehen, wie viel Strom jeweils fließt und wie hoch dann die Spannung ist. Das ist wichtig, um zu bestimmen, welche Batterie verwendet werden muss. Wir berechnen automatisch die tatsächliche Energie, die verwendet wurde. Für jede einzelne Aktivität. Vielleicht wird der Schlafmodus nicht schnell genug angestellt. Oder irgendwo läuft noch ein Prozessor. Das können Entwickler im Profil nun sehr leicht sehen und reparieren. (***Vanja Samuelsson, Qoitech***)

##Spannungsabfall bei Knopfzellen-Batterien führt zu Systemabsturz

Um zu erklären, wie das ganze in der Praxis funktioniert, hat Qoitech Entwickler Björn Rosqvist eine Demonstrations-Platine, ein sogenanntes Demo-Board, an das Otii Messgerät angeschlossen. Das Demo-Board ist eine einfache kleine, grüne Leiterplatte, auf der die wichtigsten elektronischen Bauteile aufgesteckt sind, die IoT-Geräte normalerweise haben. Mit Mikroprozessoren zur Steuerung der gewünschten Funktionen. Mit Speicherchips. Und mit  Schnittstellen für die Datenkommunikation mit dem internet oder anderen Geräten.

> Ich beginne mit der Aufnahme und dem Einschalten meines Gerätes.  Am Demo-Board beginnt diese LED zu blinken. Ich kann in Echtzeit die Protokollierung verfolgen, während gleichzeitig der Strom gemessen wird. Wenn die LED am Demo-Board leuchtet, fließt ein Strom von 78,2 Mikro Ampere. Dann wacht der Mikrocontroller am Demo-Board auf. Geht die LED wieder aus, wechselt das Board in den Schlafmodus. (***Björn Rosqvist, Qoitech***)

Über die Software auf seinem Laptop kann Björn Rosqvist verschiedene Profile für die gewünschten Messungen anlegen. Grafiken zeigen dann optisch mit Sinuskurven, wie sich Strom und Spannung verändern, wenn das Licht an und wieder aus geht. Wenn also die Bauteile auf dem Demoboard voll aktiv sind und wenn alles wieder in den Schlafmodus mit geringstem Stromverbrauch zurückfällt.

##Spezial-Messgerät von Qoitech hilft Kleinstströme messen

> Wenn die LED leuchtet, verbraucht das Licht 78,2 Mikro-Ampere. Wenn das LED-Licht wieder ausgeht, verbraucht der Mikrocontroller nur noch einen kleine Ruhestrom: 565 Nano Ampere. Das ist also ein wirklich niedriger Stromverbrauch im Schlafmodus. Wir können weniger als 100 Nano-Ampere messen mit einer Auflösung von 5 Nano Ampere. Wir messen also die wirklichen kleinen Ruheströme sehr genau. (***Björn Rosqvist, Qoitech***)

Bis hierhin wäre das alles auch noch mit anderen Messgeräten nachvollziehbar, erläutert Björn Rosqvist. Der eigentliche Sinn des Otii Messgerätes sind einige der eingebauten Feature, um die Software und die Hardware von IoT-Geräten auf die verwendeten Batterien abstimmen zu können, damit die Knopfzellen möglichst lange halten.

> Ich zeige ihnen gleich, wie ein IoT-Gerät über ein Netzteil mit Strom versorgt wird. Dann kann ich vom Netzteil zu einer Batterie wechseln.  Unser Otii genanntes Gerät wird tatsächlich eine Batterie nachahmen. Was Sie sehe, sind hier viele verschiedene Batterieprofile. Mit dem Profil verschiedener Alkalien Batterien können sie prüfen, wie sich die Knopfzellen jeweils in ihrer Hardware verhalten. Ich kann zum Beispiel entscheiden, wo ich auf der Entladekurve sein möchte. Will ich mein Gerät ausprobieren, wenn die Batterie voll aufgeladen ist wie eine neue Batterie? Oder möchte ich sehen, wie sich mein Gerät verhält, wenn es 50 Prozent geladen ist? Oder will ich wirklich gemein sein und sehen, wie gut sich mein Gerät verhält, wenn die Batterie fast leer ist. (***Björn Rosqvist, Qoitech ***)

##Test unter Realbedingungen erhöhen die Lebensdauer von IoT-Geräten

Durch die verschiedenen Batterieprofile des Otii können Software- und Hardware-Ingenieure ihre Geräte also während des ganzen Entwicklungsprozesses unter Realbedingungen immer wieder danach überprüfen, ob die Batterieleistung möglichst gut ausgenutzt wird. Wenn eine Batterie zum Beispiel zu schnell entladen wird oder das IoT-Gerät seinen Geist aufgibt, obwohl die Batterie noch halb voll ist, müssen die eingebauten Fehler möglichst früh behoben werden. Das angeschlossene Demo-Board kann die Batterieleistung noch nicht sehr gut verwerten, sagt Björn Rosqvist, und lacht verschmitzt. Über ein Menü der Analysesoftware wählt er ein Batterieprofil aus, das die Grenzen des Testaufbaus auslotet.

> Ich habe mich entschieden, am Ende der Lebensdauer der Batterie zu sein. Ich behandle mein Gerät gerne sehr schlecht.  Hier sehen sie was passiert, wenn ich vom Netzbetrieb auf die Knopfzellenbatterie umschalte. Das Netzteil hat 3 Volt geliefert. Die Batterie liefert dagegen nur eine durchschnittliche Spannung von 2,45 Volt. Meine 3,2 Volt Knopfzellen-Batterie liefert also gar keine 3,2 Volt mehr. Die Spannung ist stark abgefallen. Und wenn ich diesen hohen Spitzenstrom habe, sehe ich, dass meine Knopfzelle diesen Spitzenstrom wirklich nicht mehr bewältigen kann. Die Spannung fällt dabei weiter auf 1,27 Volt. Wenn wir uns das genauer ansehen bemerken wir sofort: Mein Gerät muss neu starten wegen der Probleme mit der Stromversorgung. Hier kann ich das Fehlerprotokoll lesen. Dort steht, der Spannungsabfall hat einen Neustart erzwungen. Vielleicht passiert das, wenn ich nur 120 Milli Ampere-Stunden aus meiner Batterie mit 161 Milli Ampere-Stunden verbraucht habe. Das bedeutet, dass die verbleibende Kapazität für mich eigentlich eine Totkapazität ist. Den Rest kann ich nie verbrauchen. (***Björn Rosqvist, Qoitech***)

##Spannungsabfall verkürzt Lebensdauer von IoT-Geräten oft dramatisch

Als Folge könnte eine Batterie statt der zuvor angenommenen 5 Jahre vielleicht nur 1 Jahr halten, sagt Rosqvist. Durch die Messung mit verschiedenen Batterieprofilen des Otii-Messgerätes können IoT-Geräte unter Realbedingungen auf Energieeffizienz geprüft und optimiert werden, ergänzt Vanja  Samuelsson.

> Wir geben Ihnen die Möglichkeit, Batterien realistischer zu prüfen als nur mit einem Datenblatt. Wenn ihre Anwendung nicht sehr oft Daten sendet und viele, kurze Strom-Spitzen entstehen, können Batterien nicht so gut damit umgehen. Das bedeutet, dass sie nur ein Drittel oder die Hälfte der Kapazität nutzen können, die ihnen eigentlich zur Verfügung steht. Mit unserem Werkzeug können sie vom Datenblatt wechseln zu einer Echtzeitmessung. Als Ergebnis erhalten Sie die tatsächliche Batterielebensdauer. (***Björn Rosqvist, Qoitech***)

Als Preis gibt Qoitech derzeit 550 US-Dollar für seine Standardversion des Otii an. Dadurch soll es möglich werden, sagt Vanja Samuelsson, wirklich jedem IoT-Entwickler ein Messgerät auf den Tisch zu legen. 

> Damit wir nicht Batterien anhäufen und sagen müssen, oh, wir haben sie nicht wirklich gut benutzt. (***Vanja Samuelsson, Qoitech***)
